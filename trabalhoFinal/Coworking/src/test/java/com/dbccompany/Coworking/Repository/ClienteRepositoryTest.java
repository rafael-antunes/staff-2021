package com.dbccompany.Coworking.Repository;

import com.dbccompany.Coworking.Entity.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@Profile("test")
public class ClienteRepositoryTest {

    @Autowired
    private ClienteRepository repository;

    @Test
    public void salvarCliente () {
        ClienteEntity cliente = novoCliente();
        repository.save(cliente);

        assertEquals(cliente.getId(), repository.findById(cliente.getId()).get().getId());
        assertEquals(cliente.getNome(), repository.findById(cliente.getId()).get().getNome());
        assertEquals(cliente.getCPF(), repository.findById(cliente.getId()).get().getCPF());
        assertEquals(cliente.getDataNascimento(), repository.findById(cliente.getId()).get().getDataNascimento());
    }

    @Test
    public void clienteNaoExiste () {
        assertNull(repository.findByCPF("13245678901"));
    }

    @Test
    public void buscarPorId () {
        ClienteEntity cliente = novoCliente();
        repository.save(cliente);

        assertEquals(cliente, repository.findById(cliente.getId()).get());
    }

    @Test
    public void buscarPorNome () {
        ClienteEntity cliente = novoCliente();
        repository.save(cliente);

        assertEquals(cliente, repository.findAllByNome(cliente.getNome()).get(0));
    }

    @Test
    public void buscarPorCPF () {
        ClienteEntity cliente = novoCliente();
        repository.save(cliente);

        assertEquals(cliente, repository.findByCPF(cliente.getCPF()));
    }

    @Test
    public void buscarPorDataNascimento() {
        ClienteEntity cliente = novoCliente();
        repository.save(cliente);

        assertEquals(cliente, repository.findAllByDataNascimento(cliente.getDataNascimento()).get(0));
    }

    private ClienteEntity novoCliente () {
        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome("João");
        cliente.setCPF("12345678901");
        cliente.setDataNascimento(new Date());
        return cliente;
    }
}
