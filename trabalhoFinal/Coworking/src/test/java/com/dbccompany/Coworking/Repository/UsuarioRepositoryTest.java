package com.dbccompany.Coworking.Repository;

import com.dbccompany.Coworking.Entity.UsuarioEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@Profile("test")
public class UsuarioRepositoryTest {

    @Autowired
    private UsuarioRepository repository;

    @Test
    public void salvarUsuario () {
        UsuarioEntity usuario = novoUsuario();

        repository.save(usuario);

        assertEquals(usuario.getId(), repository.findByLogin(usuario.getLogin()).getId());
        assertEquals(usuario.getNome(), repository.findByLogin(usuario.getLogin()).getNome());
        assertEquals(usuario.getEmail(), repository.findByLogin(usuario.getLogin()).getEmail());
        assertEquals(usuario.getLogin(), repository.findByLogin(usuario.getLogin()).getLogin());
        assertEquals(usuario.getSenha(), repository.findByLogin(usuario.getLogin()).getSenha());
    }

    @Test
    public void usuarioNaoExiste () {
        assertNull(repository.findByLogin("admin"));
    }

    @Test
    public void buscarTodos () {
        List<UsuarioEntity> lista = new ArrayList<>();
        StringBuilder builder = new StringBuilder();
        for(int i = 0; i < 3; i++){
            UsuarioEntity ususario = novoUsuario();
            ususario.setEmail(builder.append(ususario.getEmail() + i).toString());
            builder.delete(0, builder.length());
            ususario.setLogin(builder.append(ususario.getLogin() + i).toString());
            builder.delete(0, builder.length());
            lista.add(ususario);
            repository.save(ususario);
        }

        assertEquals(lista, repository.findAll());
    }

    @Test
    public void buscarPorId () {
        UsuarioEntity usuario = novoUsuario();
        repository.save(usuario);

        assertEquals(usuario, repository.findById(usuario.getId()).get());
    }

    @Test
    public void buscarPorLogin () {
        UsuarioEntity usuario = novoUsuario();
        repository.save(usuario);

        assertEquals(usuario, repository.findByLogin(usuario.getLogin()));
    }

    private UsuarioEntity novoUsuario () {
        UsuarioEntity usuario = new UsuarioEntity();
        usuario.setNome("Admin");
        usuario.setEmail("admin@admin.com");
        usuario.setLogin("admin");
        usuario.setSenha("senha1234");
        return usuario;
    }

}
