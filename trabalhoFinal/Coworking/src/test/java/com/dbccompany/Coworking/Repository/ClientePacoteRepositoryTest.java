package com.dbccompany.Coworking.Repository;

import com.dbccompany.Coworking.Entity.ClienteEntity;
import com.dbccompany.Coworking.Entity.ClientePacoteEntity;
import com.dbccompany.Coworking.Entity.PacoteEntity;
import com.dbccompany.Coworking.Entity.PagamentoEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;

import java.util.Date;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@Profile("test")
public class ClientePacoteRepositoryTest {

    @Autowired
    private ClientePacoteRepository repository;

    @Test
    public void salvarClientePacote () {
        ClientePacoteEntity clientePacote = new ClientePacoteEntity();
        clientePacote.setQuantidade(8);
        repository.save(clientePacote);

        assertEquals(clientePacote.getId(), repository.findById(clientePacote.getId()).get().getId());
        assertEquals(clientePacote.getQuantidade(), repository.findById(clientePacote.getId()).get().getQuantidade());
    }

    public void clientePacoteNaoExiste () {
        assertEquals(Optional.empty(), repository.findById(1));
    }

    @Test
    public void buscarPorId () {
        ClientePacoteEntity clientePacote = new ClientePacoteEntity();
        repository.save(clientePacote);

        assertEquals(clientePacote, repository.findById(clientePacote.getId()).get());
    }

    public void buscarPorPacote () {
        ClientePacoteEntity clientePacote = new ClientePacoteEntity();

        PacoteEntity pacote = new PacoteEntity();

        clientePacote.setPacote(pacote);
        repository.save(clientePacote);

        assertEquals(clientePacote, repository.findAllByPacote(pacote).get(0));
    }

    public void buscarPorCliente () {
        ClientePacoteEntity clientePacote = new ClientePacoteEntity();

        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome("João");
        cliente.setCPF("12345678901");
        cliente.setDataNascimento(new Date());

        clientePacote.setCliente(cliente);
        repository.save(clientePacote);

        assertEquals(clientePacote, repository.findAllByCliente(cliente).get(0));
    }

    public void buscarPorPagamento () {
        ClientePacoteEntity clientePacote = new ClientePacoteEntity();

        PagamentoEntity pagamento = new PagamentoEntity();

        clientePacote.setPagamento(pagamento);
        repository.save(clientePacote);

        assertEquals(clientePacote, repository.findByPagamento(pagamento));
    }
}
