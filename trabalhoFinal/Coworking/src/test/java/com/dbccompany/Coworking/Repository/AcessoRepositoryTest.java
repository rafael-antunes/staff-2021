package com.dbccompany.Coworking.Repository;

import com.dbccompany.Coworking.Entity.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@Profile("test")
public class AcessoRepositoryTest {

    @Autowired
    private AcessoRepository repository;

    @Test
    public void salvarAcesso() {
        AcessoEntity acesso = novoAcesso();
        repository.save(acesso);

        assertEquals(acesso.getId(), repository.findById(acesso.getId()).get().getId());
        assertEquals(acesso.getData(), repository.findById(acesso.getId()).get().getData());
        assertEquals(acesso.isEntrada(), repository.findById(acesso.getId()).get().isEntrada());
        assertEquals(acesso.isExcecao(), repository.findById(acesso.getId()).get().isExcecao());
    }

    @Test
    public void acessoNãoExiste () {
        assertEquals(Optional.empty(), repository.findById(1));
    }

    @Test
    public void buscarPorId () {
        AcessoEntity acesso = novoAcesso();
        repository.save(acesso);

        assertEquals(acesso, repository.findById(acesso.getId()).get());
    }

    @Test
    public void buscarPorEntrada () {
        AcessoEntity acesso = novoAcesso();
        repository.save(acesso);

        assertEquals(acesso, repository.findAllByIsEntrada(acesso.isEntrada()).get(0));
    }

    @Test
    public void buscarPorExcecao () {
        AcessoEntity acesso = novoAcesso();
        repository.save(acesso);

        assertEquals(acesso, repository.findAllByIsExcecao(acesso.isExcecao()).get(0));
    }

    @Test
    public void buscarPorData () {
        AcessoEntity acesso = novoAcesso();
        repository.save(acesso);

        assertEquals(acesso, repository.findByData(acesso.getData()));
    }

    @Test
    public void buscarPorSaldoCliente () {
        AcessoEntity acesso = novoAcesso();

        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome("João");
        cliente.setCPF("12345678901");
        cliente.setDataNascimento(new Date());

        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("Sala");
        espaco.setValor(10.0);
        espaco.setQtdPessoas(1);

        SaldoClienteEntity saldoCliente = new SaldoClienteEntity();
        saldoCliente.setCliente(cliente);
        saldoCliente.setEspaco(espaco);
        saldoCliente.setId(new SaldoClienteId(cliente.getId(), espaco.getId()));

        acesso.setSaldoCliente(saldoCliente);

        repository.save(acesso);

        assertEquals(acesso, repository.findAllBySaldoCliente(acesso.getSaldoCliente()).get(0));
    }

    private AcessoEntity novoAcesso () {
        AcessoEntity acesso = new AcessoEntity();
        acesso.setExcecao(Boolean.FALSE);
        acesso.setEntrada(Boolean.TRUE);
        acesso.setData(LocalDateTime.now());
        return acesso;
    }
}
