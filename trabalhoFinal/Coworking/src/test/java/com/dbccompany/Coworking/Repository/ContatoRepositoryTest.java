package com.dbccompany.Coworking.Repository;

import com.dbccompany.Coworking.Entity.ClienteEntity;
import com.dbccompany.Coworking.Entity.ContatoEntity;
import com.dbccompany.Coworking.Entity.TipoContatoEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;

import java.awt.*;
import java.util.Date;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@Profile("test")
public class ContatoRepositoryTest {

    @Autowired
    private ContatoRepository repository;

    @Test
    public void salvarContato () {
        ContatoEntity contato = novoContato();

        repository.save(contato);

        assertEquals(contato.getValor(), repository.findById(1).get().getValor());
    }

    @Test
    public void contatoNaoExiste () {
        assertEquals(Optional.empty(), repository.findById(1));
    }

    @Test
    public void buscarPorId () {
        ContatoEntity contato = novoContato();
        repository.save(contato);

        assertEquals(contato, repository.findById(contato.getId()).get());
    }

    @Test
    public void buscarPorTipoContato () {
        ContatoEntity contato = novoContato();

        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setNome("Celular");

        contato.setTipoContato(tipoContato);
        repository.save(contato);

        assertEquals(contato, repository.findAllByTipoContato(contato.getTipoContato()).get(0));
    }

    @Test
    public void buscarPorCliente () {
        ContatoEntity contato = novoContato();

        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome("João");
        cliente.setCPF("12345678901");
        cliente.setDataNascimento(new Date());

        contato.setCliente(cliente);
        repository.save(contato);
    }

    @Test
    public void buscarPorValor () {}

    private ContatoEntity novoContato() {
        ContatoEntity contato = new ContatoEntity();
        contato.setValor("Celular");
        return contato;
    }

}
