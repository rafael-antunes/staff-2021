package com.dbccompany.Coworking.Exception.Espaco;

public class EspacoNaoEncontrado extends EspacoException {
    public EspacoNaoEncontrado() {
        super("Espaco não encontrado");
    }
}
