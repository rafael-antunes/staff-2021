package com.dbccompany.Coworking.Exception.Pacote;

public class PacoteNaoEncontrado extends PacoteException{
    public PacoteNaoEncontrado() {
        super("Pacote não encontrado");
    }

}
