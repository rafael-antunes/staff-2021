package com.dbccompany.Coworking.DTO;

import com.dbccompany.Coworking.Entity.SaldoClienteId;

public class SaldoClienteIdDTO {

    private Integer cliente_id;
    private Integer espaco_id;

    public SaldoClienteIdDTO(SaldoClienteId saldoClienteId) {
        this.cliente_id = saldoClienteId.getId_cliente();
        this.espaco_id = saldoClienteId.getId_espaco();
    }

    public SaldoClienteIdDTO() {
    }

    public SaldoClienteId converter() {
        SaldoClienteId saldoClienteId = new SaldoClienteId();
        saldoClienteId.setId_cliente(this.cliente_id);
        saldoClienteId.setId_espaco(this.espaco_id);
        return saldoClienteId;
    }

    public Integer getCliente_id() {
        return cliente_id;
    }

    public void setCliente_id(Integer cliente_id) {
        this.cliente_id = cliente_id;
    }

    public Integer getEspaco_id() {
        return espaco_id;
    }

    public void setEspaco_id(Integer espaco_id) {
        this.espaco_id = espaco_id;
    }
}
