package com.dbccompany.Coworking.DTO;

import com.dbccompany.Coworking.Entity.SaldoClienteEntity;
import com.dbccompany.Coworking.Entity.SaldoClienteId;
import com.dbccompany.Coworking.Entity.TipoContratacaoEnum;

import java.util.Date;

public class SaldoClienteDTO {

    private Integer cliente_id;
    private Integer espaco_id;
    private TipoContratacaoEnum tipoContratacao;
    private Integer quantidade;
    private Date vencimento;

    public SaldoClienteDTO(SaldoClienteEntity saldoCliente) {
        this.cliente_id = saldoCliente.getCliente().getId();
        this.espaco_id = saldoCliente.getEspaco().getId();
        this.tipoContratacao = saldoCliente.getTipoContratacao();
        this.quantidade = saldoCliente.getQuantidade();
        this.vencimento = saldoCliente.getVencimento();
    }

    public SaldoClienteDTO() {
    }
    
    public SaldoClienteEntity converter () {
        SaldoClienteEntity saldoCliente = new SaldoClienteEntity();
        saldoCliente.setId(new SaldoClienteId(this.cliente_id, this.espaco_id));
        saldoCliente.setTipoContratacao(this.tipoContratacao);
        saldoCliente.setQuantidade(this.quantidade);
        saldoCliente.setVencimento(this.vencimento);
        return saldoCliente;
    }

    public Integer getCliente_id() {
        return cliente_id;
    }

    public void setCliente_id(Integer cliente_id) {
        this.cliente_id = cliente_id;
    }

    public Integer getEspaco_id() {
        return espaco_id;
    }

    public void setEspaco_id(Integer espaco_id) {
        this.espaco_id = espaco_id;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Date getVencimento() {
        return vencimento;
    }

    public void setVencimento(Date vencimento) {
        this.vencimento = vencimento;
    }
}
