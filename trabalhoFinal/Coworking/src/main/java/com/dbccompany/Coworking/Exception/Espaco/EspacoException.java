package com.dbccompany.Coworking.Exception.Espaco;

public class EspacoException extends Exception{
    private String mensagem;

    public EspacoException(String mensagem) {
        this.mensagem = mensagem;
    }

    public String getMensagem() {
        return mensagem;
    }
}
