package com.dbccompany.Coworking.Exception.Contratacao;

public class ArgumentosInvalidosContratacao extends ContratacaoException {
    public ArgumentosInvalidosContratacao() {
        super("Faltou campos");
    }
}
