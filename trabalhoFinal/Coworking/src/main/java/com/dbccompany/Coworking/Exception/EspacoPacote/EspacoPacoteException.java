package com.dbccompany.Coworking.Exception.EspacoPacote;

public class EspacoPacoteException extends Exception {
    String mensagem;

    public EspacoPacoteException(String mensagem) {
        this.mensagem = mensagem;
    }

    public String getMensagem() {
        return mensagem;
    }
}
