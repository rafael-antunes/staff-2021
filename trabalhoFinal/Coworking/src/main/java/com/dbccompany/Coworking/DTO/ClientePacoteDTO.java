package com.dbccompany.Coworking.DTO;

import com.dbccompany.Coworking.Entity.ClientePacoteEntity;

public class ClientePacoteDTO {

    private Integer quantidade;
    private ClienteDTO cliente;
    private PacoteDTO pacote;

    public ClientePacoteDTO(ClientePacoteEntity clientePacote) {
        this.quantidade = clientePacote.getQuantidade();
        this.cliente = new ClienteDTO(clientePacote.getCliente());
        this.pacote = new PacoteDTO(clientePacote.getPacote());
    }

    public ClientePacoteDTO() {
    }

    public ClientePacoteEntity converter() {
        ClientePacoteEntity clientePacote = new ClientePacoteEntity();
        clientePacote.setQuantidade(this.quantidade);
        clientePacote.setCliente(this.cliente.converter());
        clientePacote.setPacote(this.pacote.converter());
        return clientePacote;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public ClienteDTO getCliente() {
        return cliente;
    }

    public void setCliente(ClienteDTO cliente) {
        this.cliente = cliente;
    }

    public PacoteDTO getPacote() {
        return pacote;
    }

    public void setPacote(PacoteDTO pacote) {
        this.pacote = pacote;
    }
}
