package com.dbccompany.Coworking.Repository;

import com.dbccompany.Coworking.Entity.ClientePacoteEntity;
import com.dbccompany.Coworking.Entity.EspacoPacoteEntity;
import com.dbccompany.Coworking.Entity.PacoteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PacoteRepository extends CrudRepository<PacoteEntity, Integer> {
    PacoteEntity save (PacoteEntity pacote);
}
