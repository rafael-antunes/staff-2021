package com.dbccompany.Coworking.Service;

import com.dbccompany.Coworking.DTO.SaldoClienteDTO;
import com.dbccompany.Coworking.Entity.ClienteEntity;
import com.dbccompany.Coworking.Entity.EspacoEntity;
import com.dbccompany.Coworking.Entity.SaldoClienteEntity;
import com.dbccompany.Coworking.Entity.SaldoClienteId;
import com.dbccompany.Coworking.Exception.SaldoCliente.ArgumentosInvalidosSaldoCliente;
import com.dbccompany.Coworking.Exception.SaldoCliente.NaoExistemSaldosCliente;
import com.dbccompany.Coworking.Exception.SaldoCliente.SaldoClienteNaoEncontrado;
import com.dbccompany.Coworking.Repository.SaldoClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class SaldoClienteService {

    @Autowired
    private SaldoClienteRepository repository;

    @Transactional
    public SaldoClienteDTO salvar (SaldoClienteEntity saldoCliente) throws ArgumentosInvalidosSaldoCliente {
        try {
            return new SaldoClienteDTO(this.salvarEEditar(saldoCliente));
        } catch (Exception e) {
            throw new ArgumentosInvalidosSaldoCliente();
        }
    }

    @Transactional
    public SaldoClienteDTO editar (SaldoClienteEntity saldoCliente) throws ArgumentosInvalidosSaldoCliente {
        try {
            return new SaldoClienteDTO(this.salvarEEditar(saldoCliente));
        } catch (Exception e) {
            throw new ArgumentosInvalidosSaldoCliente();
        }
    }

    public List<SaldoClienteDTO> buscarTodosSaldosCliente () throws NaoExistemSaldosCliente {
        try {
            return this.transformarList((List<SaldoClienteEntity>) repository.findAll());
        } catch (Exception e) {
            throw new NaoExistemSaldosCliente();
        }
    }

    public SaldoClienteDTO buscarPorId (SaldoClienteId id) throws SaldoClienteNaoEncontrado {
        try {
            return new SaldoClienteDTO(repository.findById(id).get());
        } catch (Exception e) {
            throw new SaldoClienteNaoEncontrado();
        }
    }

    public List<SaldoClienteDTO> buscarPorEspaco (EspacoEntity espaco) throws SaldoClienteNaoEncontrado {
        try {
            return this.transformarList(repository.findAllByEspaco(espaco));
        } catch (Exception e) {
            throw new SaldoClienteNaoEncontrado();
        }
    }

    public List<SaldoClienteDTO> buscarPorCliente (ClienteEntity cliente) throws SaldoClienteNaoEncontrado {
        try {
            return this.transformarList(repository.findAllByCliente(cliente));
        } catch (Exception e) {
            throw new SaldoClienteNaoEncontrado();
        }
    }

    private SaldoClienteEntity salvarEEditar (SaldoClienteEntity saldoCliente) {
        return repository.save(saldoCliente);
    }

    private List<SaldoClienteDTO> transformarList (List<SaldoClienteEntity> lista) {
        List<SaldoClienteDTO> retorno = new ArrayList<>();
        for ( SaldoClienteEntity presente : lista ) {
            retorno.add(new SaldoClienteDTO(presente));
        }
        return retorno;
    }
}
