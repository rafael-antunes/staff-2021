package com.dbccompany.Coworking.DTO;

import com.dbccompany.Coworking.Entity.ClienteEntity;
import com.dbccompany.Coworking.Repository.ContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;

public class ClienteDTO {

    private String nome;
    private String CPF;
    private Date dataNascimento;
    private List<ContatoDTO> contato;

    public ClienteDTO(ClienteEntity cliente) {
        this.nome = cliente.getNome();
        this.CPF = cliente.getCPF();
        this.dataNascimento = cliente.getDataNascimento();
    }

    public ClienteDTO() {
    }

    public ClienteEntity converter() {
        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome(this.nome);
        cliente.setCPF(this.CPF);
        cliente.setDataNascimento(this.dataNascimento);
        return cliente;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCPF() {
        return CPF;
    }

    public void setCPF(String CPF) {
        this.CPF = CPF;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public List<ContatoDTO> getContato() {
        return contato;
    }

    public void setContato(List<ContatoDTO> contato) {
        this.contato = contato;
    }
}
