package com.dbccompany.Coworking.DTO;

import com.dbccompany.Coworking.Entity.EspacoPacoteEntity;
import com.dbccompany.Coworking.Entity.TipoContratacaoEnum;
import com.dbccompany.Coworking.Repository.EspacoRepository;
import com.dbccompany.Coworking.Repository.PacoteRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class EspacoPacoteDTO {

    private TipoContratacaoEnum tipoContratacao;
    private Integer quantidade;
    private Integer prazo;
    private EspacoDTO espaco;
    private PacoteDTO pacote;

    public EspacoPacoteDTO(EspacoPacoteEntity espacoPacote) {
        this.tipoContratacao = espacoPacote.getTipoContratacao();
        this.quantidade = espacoPacote.getQuantidade();
        this.prazo = espacoPacote.getPrazo();
        this.espaco = new EspacoDTO(espacoPacote.getEspaco());
        this.pacote = new PacoteDTO(espacoPacote.getPacote());
    }

    public EspacoPacoteDTO() {
    }

    public EspacoPacoteEntity converter() {
        EspacoPacoteEntity espacoPacote =  new EspacoPacoteEntity();
        espacoPacote.setTipoContratacao(this.tipoContratacao);
        espacoPacote.setQuantidade(this.quantidade);
        espacoPacote.setPrazo(this.prazo);
        espacoPacote.setPacote(pacote.converter());
        espacoPacote.setEspaco(espaco.converter());
        return espacoPacote;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }

    public EspacoDTO getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoDTO espaco) {
        this.espaco = espaco;
    }

    public PacoteDTO getPacote() {
        return pacote;
    }

    public void setPacote(PacoteDTO pacote) {
        this.pacote = pacote;
    }
}
