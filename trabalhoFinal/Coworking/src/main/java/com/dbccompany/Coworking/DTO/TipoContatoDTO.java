package com.dbccompany.Coworking.DTO;

import com.dbccompany.Coworking.Entity.TipoContatoEntity;

public class TipoContatoDTO {

    private String nome;

    public TipoContatoDTO(TipoContatoEntity tipoContato) {
        this.nome = tipoContato.getNome();
    }

    public TipoContatoDTO() {
    }

    public TipoContatoEntity converter() {
        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setNome(this.nome);
        return tipoContato;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
