package com.dbccompany.Coworking.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class SaldoClienteEntity {

    @EmbeddedId
    private SaldoClienteId id;

    @JsonIgnore
    @ManyToOne ( cascade = CascadeType.ALL )
    @MapsId("id_cliente")
    private ClienteEntity cliente;

    @JsonIgnore
    @ManyToOne ( cascade = CascadeType.ALL )
    @MapsId("id_espaco")
    private EspacoEntity espaco;

    @OneToMany ( fetch = FetchType.LAZY, mappedBy = "saldoCliente")
    private List<AcessoEntity> acesso;

    @Enumerated ( EnumType.STRING )
    private TipoContratacaoEnum tipoContratacao;

    @Column
    private Integer quantidade;

    @Column
    private Date vencimento;

    public SaldoClienteId getId() {
        return id;
    }

    public void setId(SaldoClienteId id) {
        this.id = id;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }

    public EspacoEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoEntity espaco) {
        this.espaco = espaco;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Date getVencimento() {
        return vencimento;
    }

    public void setVencimento(Date vencimento) {
        this.vencimento = vencimento;
    }

    public List<AcessoEntity> getAcesso() {
        return acesso;
    }

    public void setAcesso(List<AcessoEntity> acesso) {
        this.acesso = acesso;
    }
}
