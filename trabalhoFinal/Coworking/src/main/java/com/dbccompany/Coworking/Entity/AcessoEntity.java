package com.dbccompany.Coworking.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class AcessoEntity {

    @Id
    @SequenceGenerator( name = "ACESSO_SEQ", sequenceName = "ACESSO_SEQ")
    @GeneratedValue( generator = "ACESSO_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @JsonIgnore
    @ManyToOne( cascade = CascadeType.ALL)
    private SaldoClienteEntity saldoCliente;

    @Column ( nullable = false )
    private boolean isEntrada;

    @Column ( nullable = false )
    private LocalDateTime data;

    @Column ( nullable = false )
    private boolean isExcecao;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public SaldoClienteEntity getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(SaldoClienteEntity saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public boolean isEntrada() {
        return isEntrada;
    }

    public void setEntrada(boolean entrada) {
        isEntrada = entrada;
    }

    public LocalDateTime getData() {
        return data;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }

    public boolean isExcecao() {
        return isExcecao;
    }

    public void setExcecao(boolean excecao) {
        isExcecao = excecao;
    }
}
