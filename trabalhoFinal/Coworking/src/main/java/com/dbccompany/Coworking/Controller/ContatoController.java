package com.dbccompany.Coworking.Controller;

import com.dbccompany.Coworking.DTO.ClienteDTO;
import com.dbccompany.Coworking.DTO.ContatoDTO;
import com.dbccompany.Coworking.Exception.Cliente.ClienteNaoEncontrado;
import com.dbccompany.Coworking.Exception.Contato.ArgumentosInvalidosContato;
import com.dbccompany.Coworking.Exception.Contato.ContatoNaoEncontrado;
import com.dbccompany.Coworking.Exception.Contato.NaoExistemContatosCadastrados;
import com.dbccompany.Coworking.Service.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/api/contato")
public class ContatoController {

    @Autowired
    private ContatoService service;

    @GetMapping(value = "/")
    @ResponseBody
    public ResponseEntity<List<ContatoDTO>> buscarTodosContatos () {
        try {
            return new ResponseEntity<>(service.buscarTodosContatos(), HttpStatus.OK);
        } catch (NaoExistemContatosCadastrados e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<ContatoDTO> buscarPorId (@PathVariable Integer id) {
        try {
            return new ResponseEntity<>(service.buscarPorId(id), HttpStatus.OK);
        } catch (ContatoNaoEncontrado e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(value = "/salvar")
    @ResponseBody
    public ResponseEntity<ContatoDTO> salvarContato (@RequestBody ContatoDTO contato) {
        try {
            return new ResponseEntity<>(service.salvar(contato.converter()), HttpStatus.CREATED);
        } catch (ArgumentosInvalidosContato e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<ContatoDTO> editarContato (@RequestBody ContatoDTO contato, @PathVariable Integer id) {
        try {
            return new ResponseEntity<>(service.editar(contato.converter(), id), HttpStatus.ACCEPTED);
        } catch (ArgumentosInvalidosContato e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/busca/porCliente")
    @ResponseBody
    public ResponseEntity<List<ContatoDTO>> buscarTodosContatosPorCliente (@RequestBody ClienteDTO cliente) {
        try {
            return new ResponseEntity<>(service.buscarTodosPorCliente(cliente.converter()), HttpStatus.OK);
        } catch (ClienteNaoEncontrado e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
