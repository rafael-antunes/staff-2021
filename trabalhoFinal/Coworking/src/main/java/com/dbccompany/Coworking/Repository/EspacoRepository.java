package com.dbccompany.Coworking.Repository;

import com.dbccompany.Coworking.Entity.EspacoEntity;
import com.dbccompany.Coworking.Entity.EspacoPacoteEntity;
import com.dbccompany.Coworking.Entity.SaldoClienteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;

@Repository
public interface EspacoRepository extends CrudRepository<EspacoEntity, Integer> {
    EspacoEntity save (EspacoEntity espaco);
    EspacoEntity findByNome (String nome);
}
