package com.dbccompany.Coworking.Repository;

import com.dbccompany.Coworking.Entity.*;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SaldoClienteRepository extends CrudRepository<SaldoClienteEntity, SaldoClienteId> {
    SaldoClienteEntity save (SaldoClienteEntity saldo);
    List<SaldoClienteEntity> findAllByCliente (ClienteEntity cliente);
    List<SaldoClienteEntity> findAllByEspaco (EspacoEntity espaco);
    Optional<SaldoClienteEntity> findById (SaldoClienteId id);
}
