package com.dbccompany.Coworking.Exception.SaldoCliente;

public class NaoExistemSaldosCliente extends SaldoClienteException {
    public NaoExistemSaldosCliente() {
        super("Não existe nenhum saldo!");
    }
}
