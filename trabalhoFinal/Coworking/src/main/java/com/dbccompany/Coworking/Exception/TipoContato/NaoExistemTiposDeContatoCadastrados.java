package com.dbccompany.Coworking.Exception.TipoContato;

public class NaoExistemTiposDeContatoCadastrados extends TipoContatoException {
    public NaoExistemTiposDeContatoCadastrados() {
        super("Não existe nenhum tipo de contato cadastrado!");
    }
}
