package com.dbccompany.Coworking.DTO;

import com.dbccompany.Coworking.Entity.UsuarioEntity;

public class UsuarioDTO {

    private String nome;
    private String email;
    private String login;
    private String senha;

    public UsuarioDTO(UsuarioEntity usuario) {
        this.nome = usuario.getNome();
        this.email = usuario.getEmail();
        this.login = usuario.getLogin();
        this.senha = usuario.getSenha();
    }

    public UsuarioDTO() {
    }

    public UsuarioEntity converter () {
        UsuarioEntity usuario = new UsuarioEntity();
        usuario.setNome(this.nome);
        usuario.setEmail(this.email);
        usuario.setLogin(this.login);
        usuario.setSenha(this.senha);
        return usuario;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}
