package com.dbccompany.Coworking.Service;

import com.dbccompany.Coworking.DTO.TipoContatoDTO;
import com.dbccompany.Coworking.Entity.TipoContatoEntity;
import com.dbccompany.Coworking.Exception.TipoContato.ArgumentosInvalidosTipoContato;
import com.dbccompany.Coworking.Exception.TipoContato.NaoExistemTiposDeContatoCadastrados;
import com.dbccompany.Coworking.Exception.TipoContato.TipoContatoNaoEncontrado;
import com.dbccompany.Coworking.Repository.TipoContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class TipoContatoService {

    @Autowired
    private TipoContatoRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public TipoContatoDTO salvar (TipoContatoEntity tipoContato) throws ArgumentosInvalidosTipoContato {
        try {
            return new TipoContatoDTO(this.salvarEEditar(tipoContato));
        } catch (Exception e) {
            throw new ArgumentosInvalidosTipoContato();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public TipoContatoDTO editar (TipoContatoEntity tipoContato, Integer id) throws ArgumentosInvalidosTipoContato {
        try {
            tipoContato.setId(id);
            this.salvar(tipoContato);
            return new TipoContatoDTO(tipoContato);
        } catch (Exception e) {
            throw new ArgumentosInvalidosTipoContato();
        }
    }

    private TipoContatoEntity salvarEEditar (TipoContatoEntity tipoContato) {
        return repository.save(tipoContato);
    }

    public List<TipoContatoDTO> buscarTodosTiposDeContato () throws NaoExistemTiposDeContatoCadastrados {
        try {
            return this.transformarList((List<TipoContatoEntity>) repository.findAll());
        } catch (Exception e) {
            throw new NaoExistemTiposDeContatoCadastrados();
        }
    }

    public TipoContatoDTO buscarPorId (Integer id) throws TipoContatoNaoEncontrado {
        try {
            return new TipoContatoDTO(repository.findById(id).get());
        } catch (Exception e) {
            throw new TipoContatoNaoEncontrado();
        }
    }

    public TipoContatoDTO buscarPorNome (String nome) throws TipoContatoNaoEncontrado {
        try {
            return new TipoContatoDTO(repository.findByNome(nome));
        } catch (Exception e) {
            throw new TipoContatoNaoEncontrado();
        }
    }

    private List<TipoContatoDTO> transformarList (List<TipoContatoEntity> lista) {
        List<TipoContatoDTO> retorno = new ArrayList<>();
        for ( TipoContatoEntity presente : lista ) {
            retorno.add(new TipoContatoDTO(presente));
        }
        return retorno;
    }
}
