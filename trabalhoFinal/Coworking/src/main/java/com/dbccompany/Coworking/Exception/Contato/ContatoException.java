package com.dbccompany.Coworking.Exception.Contato;

public class ContatoException extends Exception{
    String mensagem;

    public ContatoException(String mensagem) {
        this.mensagem = mensagem;
    }

    public String getMensagem() {
        return mensagem;
    }
}
