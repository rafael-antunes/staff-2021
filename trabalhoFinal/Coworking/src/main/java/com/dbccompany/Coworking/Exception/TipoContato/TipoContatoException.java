package com.dbccompany.Coworking.Exception.TipoContato;

public class TipoContatoException extends Exception{
    String mensagem;

    public TipoContatoException(String mensagem) {
        this.mensagem = mensagem;
    }

    public String getMensagem() {
        return mensagem;
    }
}
