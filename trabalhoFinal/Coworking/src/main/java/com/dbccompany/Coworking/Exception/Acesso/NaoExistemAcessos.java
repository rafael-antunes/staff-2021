package com.dbccompany.Coworking.Exception.Acesso;

public class NaoExistemAcessos extends AcessoException {
    public NaoExistemAcessos() {
        super("Não existe nenhum acesso!");
    }
}
