package com.dbccompany.Coworking.Repository;

import com.dbccompany.Coworking.Entity.ClienteEntity;
import com.dbccompany.Coworking.Entity.ClientePacoteEntity;
import com.dbccompany.Coworking.Entity.PacoteEntity;
import com.dbccompany.Coworking.Entity.PagamentoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClientePacoteRepository extends CrudRepository<ClientePacoteEntity, Integer> {
    ClientePacoteEntity save (ClientePacoteEntity clientePacote);

    List<ClientePacoteEntity> findAllByPacote (PacoteEntity pacote);

    List<ClientePacoteEntity> findAllByCliente (ClienteEntity cliente);
}
