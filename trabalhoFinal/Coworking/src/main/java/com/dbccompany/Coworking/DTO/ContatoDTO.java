package com.dbccompany.Coworking.DTO;

import com.dbccompany.Coworking.Entity.ContatoEntity;

public class ContatoDTO {

    private String valor;
    private TipoContatoDTO tipoContato;

    public ContatoDTO(ContatoEntity contato) {
        this.valor = contato.getValor();
        this.tipoContato = new TipoContatoDTO(contato.getTipoContato());
    }

    public ContatoDTO() {
    }

    public ContatoEntity converter() {
        ContatoEntity contato = new ContatoEntity();
        contato.setValor(this.valor);
        contato.setTipoContato(tipoContato.converter());
        return contato;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public TipoContatoDTO getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(TipoContatoDTO tipoContato) {
        this.tipoContato = tipoContato;
    }
}
