package com.dbccompany.Coworking.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class PacoteEntity {

    @Id
    @SequenceGenerator( name = "PACOTE_SEQ", sequenceName = "PACOTE_SEQ")
    @GeneratedValue( generator = "PACOTE_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @OneToMany (fetch = FetchType.LAZY, mappedBy = "pacote" )
    private List<EspacoPacoteEntity> espacoPacote;

    @OneToMany ( fetch = FetchType.LAZY, mappedBy = "pacote" )
    private List<ClientePacoteEntity> clientePacote;

    @Column
    private double valor;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<EspacoPacoteEntity> getEspacoPacote() {
        return espacoPacote;
    }

    public void setEspacoPacote(List<EspacoPacoteEntity> espacoPacote) {
        this.espacoPacote = espacoPacote;
    }

    public List<ClientePacoteEntity> getClientePacote() {
        return clientePacote;
    }

    public void setClientePacote(List<ClientePacoteEntity> clientePacote) {
        this.clientePacote = clientePacote;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
