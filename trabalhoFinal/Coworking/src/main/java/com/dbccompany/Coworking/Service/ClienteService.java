package com.dbccompany.Coworking.Service;

import com.dbccompany.Coworking.DTO.ClienteDTO;
import com.dbccompany.Coworking.DTO.ContatoDTO;
import com.dbccompany.Coworking.DTO.TipoContatoDTO;
import com.dbccompany.Coworking.Entity.ClienteEntity;
import com.dbccompany.Coworking.Entity.ContatoEntity;
import com.dbccompany.Coworking.Exception.Cliente.ArgumentosInvalidosCliente;
import com.dbccompany.Coworking.Exception.Cliente.ClienteNaoEncontrado;
import com.dbccompany.Coworking.Exception.Cliente.NaoExistemClientesCadastrados;
import com.dbccompany.Coworking.Repository.ClienteRepository;
import com.dbccompany.Coworking.Repository.ContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository repository;

    @Autowired
    private ContatoRepository contatoRepository;

    @Transactional(rollbackFor = Exception.class)
    public ClienteDTO salvar (ClienteEntity cliente, List<ContatoEntity> contatos) throws ArgumentosInvalidosCliente {
        try {
            if ( verificarSeTemTelefoneEEmail(contatos) ) {
                ClienteEntity clienteSalvo = this.salvarEEditar(cliente);
                for (ContatoEntity contato : contatos) {
                    contato.setCliente(clienteSalvo);
                    contatoRepository.save(contato);
                }
            }
            return null;
        } catch (Exception e) {
            throw new ArgumentosInvalidosCliente();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public ClienteDTO editar (ClienteEntity cliente, Integer id) throws ArgumentosInvalidosCliente {
        try {
            return new ClienteDTO(this.salvarEEditar(cliente));
        } catch (Exception e) {
            throw new ArgumentosInvalidosCliente();
        }
    }

    private boolean verificarSeTemTelefoneEEmail (List<ContatoEntity> contatos) {
        boolean temEmail = Boolean.FALSE;
        boolean temTelefone = Boolean.FALSE;
        for ( ContatoEntity contato : contatos ) {
            if ( contato.getTipoContato().getNome() == "Email"){temEmail = true;}
            if ( contato.getTipoContato().getNome() == "Telefone"){temTelefone = true;}
        }
        if (temEmail == true && temTelefone == true) { return true; }
        return false;
    }

    private ClienteEntity salvarEEditar (ClienteEntity cliente) {
        return repository.save(cliente);
    }

    public List<ClienteDTO> buscarTodosClientes () throws NaoExistemClientesCadastrados {
        try {
            return this.transformarList((List<ClienteEntity>) repository.findAll());
        } catch (Exception e) {
            throw new NaoExistemClientesCadastrados();
        }
    }

    public ClienteDTO buscarPorId (Integer id) throws ClienteNaoEncontrado {
        try {
            return new ClienteDTO(repository.findById(id).get());
        } catch (Exception e) {
            throw new ClienteNaoEncontrado();
        }
    }

    public ClienteDTO buscarPorCPF (String CPF) throws ClienteNaoEncontrado {
        try {
            return new ClienteDTO(repository.findByCPF(CPF));
        } catch (Exception e) {
            throw new ClienteNaoEncontrado();
        }
    }

    private List<ClienteDTO> transformarList (List<ClienteEntity> lista) {
        List<ClienteDTO> retorno = new ArrayList<>();
        for ( ClienteEntity presente : lista ) {
            retorno.add(new ClienteDTO(presente));
        }
        return retorno;
    }
}
