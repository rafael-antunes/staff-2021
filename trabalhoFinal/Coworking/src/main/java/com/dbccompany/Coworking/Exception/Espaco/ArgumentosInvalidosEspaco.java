package com.dbccompany.Coworking.Exception.Espaco;

public class ArgumentosInvalidosEspaco extends EspacoException {
    public ArgumentosInvalidosEspaco() {
        super("Faltou campos");
    }
}
