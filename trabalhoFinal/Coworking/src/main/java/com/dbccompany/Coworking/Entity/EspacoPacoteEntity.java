package com.dbccompany.Coworking.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;

@Entity
public class EspacoPacoteEntity {

    @Id
    @SequenceGenerator( name = "ESPACO_PACOTE_SEQ", sequenceName = "ESPACO_PACOTE_SEQ")
    @GeneratedValue( generator = "ESPACO_PACOTE_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @JsonIgnore
    @ManyToOne ( cascade = CascadeType.ALL)
    private PacoteEntity pacote;

    @JsonIgnore
    @ManyToOne ( cascade = CascadeType.ALL )
    private EspacoEntity espaco;

    @Enumerated ( EnumType.STRING )
    private TipoContratacaoEnum tipoContratacao;

    @Column
    private Integer quantidade;

    @Column
    private Integer prazo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public PacoteEntity getPacote() {
        return pacote;
    }

    public void setPacote(PacoteEntity pacote) {
        this.pacote = pacote;
    }

    public EspacoEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoEntity espaco) {
        this.espaco = espaco;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }
}
