package com.dbccompany.Coworking.Exception.Contratacao;

public class ContratacaoException extends Exception {
    String mensagem;

    public ContratacaoException(String mensagem) {
        this.mensagem = mensagem;
    }

    public String getMensagem() {
        return mensagem;
    }
}
