package com.dbccompany.Coworking.Repository;

import com.dbccompany.Coworking.Entity.AcessoEntity;
import com.dbccompany.Coworking.Entity.SaldoClienteId;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface AcessoRepository extends CrudRepository<AcessoEntity, Integer> {
    AcessoEntity save (AcessoEntity acesso);

    List<AcessoEntity> findAllBySaldoClienteId (SaldoClienteId saldoClienteId);

    @Query(value = "select * from acesso_entity a where a.data = ?1", nativeQuery = true)
    AcessoEntity findByData (LocalDateTime data);
}
