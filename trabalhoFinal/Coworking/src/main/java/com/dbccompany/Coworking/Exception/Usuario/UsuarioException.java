package com.dbccompany.Coworking.Exception.Usuario;

public class UsuarioException extends Exception{
    private String mensagem;

    public UsuarioException ( String mensagem ) {
        this.mensagem = mensagem;
    }

    public String getMensagem() {
        return mensagem;
    }
}
