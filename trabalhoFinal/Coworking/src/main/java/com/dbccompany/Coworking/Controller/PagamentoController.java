package com.dbccompany.Coworking.Controller;

import com.dbccompany.Coworking.DTO.*;
import com.dbccompany.Coworking.Exception.Pagamento.ArgumentosInvalidosPagamento;
import com.dbccompany.Coworking.Exception.Pagamento.NaoExistemPagamentos;
import com.dbccompany.Coworking.Exception.Pagamento.PagamentoNaoEncontrado;
import com.dbccompany.Coworking.Service.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class PagamentoController {

    @Autowired
    private PagamentoService service;

    @GetMapping(value = "/")
    @ResponseBody
    public ResponseEntity<List<PagamentoDTO>> buscarTodosPagamentos () {
        try {
            return new ResponseEntity<>(service.buscarTodosPagamentos(), HttpStatus.OK);
        } catch (NaoExistemPagamentos e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<PagamentoDTO> buscarPorId (@PathVariable Integer id) {
        try {
            return new ResponseEntity<>(service.buscarPorId(id), HttpStatus.OK);
        } catch (PagamentoNaoEncontrado e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(value = "/pagarContratacao")
    @ResponseBody
    public ResponseEntity<PagamentoDTO> pagarContratacao (@RequestBody PagamentoDTO pagamento) {
        try {
            return new ResponseEntity<>(service.pagarContratacao(pagamento.converter()), HttpStatus.CREATED);
        } catch (ArgumentosInvalidosPagamento e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/pagarPacote")
    @ResponseBody
    public ResponseEntity<PagamentoDTO> pagarPacote (@RequestBody PagamentoDTO pagamento) {
        try {
            return new ResponseEntity<>(service.pagarPacote(pagamento.converter()), HttpStatus.CREATED);
        } catch (ArgumentosInvalidosPagamento e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<PagamentoDTO> editarPagamento (@RequestBody PagamentoDTO pagamento, @PathVariable Integer id) {
        try {
            return new ResponseEntity<>(service.editar(pagamento.converter(), id), HttpStatus.ACCEPTED);
        } catch (ArgumentosInvalidosPagamento e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/busca/porCliente")
    @ResponseBody
    public ResponseEntity<PagamentoDTO> buscarTodosPagamentosPorClientePacote (@RequestBody ClientePacoteDTO clientePacote) {
        try {
            return new ResponseEntity<>(service.buscarPorClientePacote(clientePacote.converter()), HttpStatus.OK);
        } catch (PagamentoNaoEncontrado e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/busca/porEspaco")
    @ResponseBody
    public ResponseEntity<PagamentoDTO> buscarTodosPagamentosPorContratacao (@RequestBody ContratacaoDTO contratacao) {
        try {
            return new ResponseEntity<>(service.buscarPorContratacao(contratacao.converter()), HttpStatus.OK);
        } catch (PagamentoNaoEncontrado e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
