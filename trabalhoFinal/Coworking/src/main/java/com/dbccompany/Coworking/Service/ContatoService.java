package com.dbccompany.Coworking.Service;

import com.dbccompany.Coworking.DTO.ContatoDTO;
import com.dbccompany.Coworking.Entity.ClienteEntity;
import com.dbccompany.Coworking.Entity.ContatoEntity;
import com.dbccompany.Coworking.Exception.Cliente.ClienteNaoEncontrado;
import com.dbccompany.Coworking.Exception.Contato.ArgumentosInvalidosContato;
import com.dbccompany.Coworking.Exception.Contato.ContatoNaoEncontrado;
import com.dbccompany.Coworking.Exception.Contato.NaoExistemContatosCadastrados;
import com.dbccompany.Coworking.Exception.Usuario.UsuarioNaoEncontrado;
import com.dbccompany.Coworking.Repository.ContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class ContatoService {

    @Autowired
    private ContatoRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public ContatoDTO salvar (ContatoEntity contato) throws ArgumentosInvalidosContato {
        try {
            return new ContatoDTO(this.salvarEEditar(contato));
        } catch (Exception e) {
            throw new ArgumentosInvalidosContato();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public ContatoDTO editar (ContatoEntity contato, Integer id) throws ArgumentosInvalidosContato {
        try {
            contato.setId(id);
            this.salvarEEditar(contato);
            return new ContatoDTO(contato);
        } catch (Exception e) {
            throw new ArgumentosInvalidosContato();
        }
    }

    private ContatoEntity salvarEEditar (ContatoEntity contato) {
        return repository.save(contato);
    }

    public List<ContatoDTO> buscarTodosContatos () throws NaoExistemContatosCadastrados {
        try {
            return this.transformarList((List<ContatoEntity>) repository.findAll());
        } catch (Exception e) {
            throw new NaoExistemContatosCadastrados();
        }
    }

    public ContatoDTO buscarPorId (Integer id) throws ContatoNaoEncontrado {
        try {
            return new ContatoDTO(repository.findById(id).get());
        } catch (Exception e) {
            throw new ContatoNaoEncontrado();
        }
    }

    public List<ContatoDTO> buscarTodosPorCliente (ClienteEntity cliente) throws ClienteNaoEncontrado {
        try {
            return this.transformarList(repository.findAllByCliente(cliente));
        } catch (Exception e) {
            throw new ClienteNaoEncontrado();
        }
    }

    private List<ContatoDTO> transformarList (List<ContatoEntity> lista) {
        List<ContatoDTO> retorno = new ArrayList<>();
        for ( ContatoEntity presente : lista ) {
            retorno.add(new ContatoDTO(presente));
        }
        return retorno;
    }
}
