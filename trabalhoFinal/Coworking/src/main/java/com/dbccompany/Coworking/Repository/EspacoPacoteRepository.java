package com.dbccompany.Coworking.Repository;

import com.dbccompany.Coworking.Entity.EspacoEntity;
import com.dbccompany.Coworking.Entity.EspacoPacoteEntity;
import com.dbccompany.Coworking.Entity.PacoteEntity;
import com.dbccompany.Coworking.Entity.TipoContratacaoEnum;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EspacoPacoteRepository extends CrudRepository<EspacoPacoteEntity, Integer> {
    EspacoPacoteEntity save (EspacoPacoteEntity espacoPacote);

    List<EspacoPacoteEntity> findAllByPacote (PacoteEntity pacote);

    List<EspacoPacoteEntity> findAllByEspaco (EspacoEntity espaco);
}
