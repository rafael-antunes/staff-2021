package com.dbccompany.Coworking.Exception.Contato;

public class NaoExistemContatosCadastrados extends ContatoException {
    public NaoExistemContatosCadastrados() {
        super("Não existe nenhum contato cadastrado!");
    }
}
