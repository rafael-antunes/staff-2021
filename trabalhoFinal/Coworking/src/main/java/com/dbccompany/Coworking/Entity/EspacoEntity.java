package com.dbccompany.Coworking.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class EspacoEntity {

    @Id
    @SequenceGenerator( name = "ESPACO_SEQ", sequenceName = "ESPACO_SEQ" )
    @GeneratedValue( generator = "ESPACO_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @OneToMany ( fetch = FetchType.LAZY, mappedBy = "espaco")
    private List<SaldoClienteEntity> saldoCliente;

    @OneToMany ( fetch = FetchType.LAZY, mappedBy = "espaco")
    private List<EspacoPacoteEntity> espacoPacote;

    @Column ( nullable = false, unique = true )
    private String nome;

    @Column ( nullable = false )
    private Integer qtdPessoas;

    @Column ( nullable = false )
    private double valor;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<SaldoClienteEntity> getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(List<SaldoClienteEntity> saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public List<EspacoPacoteEntity> getEspacoPacote() {
        return espacoPacote;
    }

    public void setEspacoPacote(List<EspacoPacoteEntity> espacoPacote) {
        this.espacoPacote = espacoPacote;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas(Integer qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
