package com.dbccompany.Coworking.Exception.Pacote;

public class NaoExistemPacotes extends PacoteException{
    public NaoExistemPacotes() {
        super("Não existe nenhum pacote!");
    }
}
