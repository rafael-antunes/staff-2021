package com.dbccompany.Coworking.Exception.ClientePacote;

public class ClientePacoteException extends Exception {
    String mensagem;

    public ClientePacoteException(String mensagem) {
        this.mensagem = mensagem;
    }

    public String getMensagem() {
        return mensagem;
    }
}
