package com.dbccompany.Coworking.Exception.EspacoPacote;

public class EspacoPacoteNaoEncontrado extends EspacoPacoteException {
    public EspacoPacoteNaoEncontrado() {
        super("Espaço em pacote não encontrado");
    }

}
