package com.dbccompany.Coworking.Controller;

import com.dbccompany.Coworking.DTO.TipoContatoDTO;
import com.dbccompany.Coworking.Entity.TipoContatoEntity;
import com.dbccompany.Coworking.Exception.TipoContato.ArgumentosInvalidosTipoContato;
import com.dbccompany.Coworking.Exception.TipoContato.NaoExistemTiposDeContatoCadastrados;
import com.dbccompany.Coworking.Exception.TipoContato.TipoContatoNaoEncontrado;
import com.dbccompany.Coworking.Service.TipoContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Service
@RequestMapping("api/tipo_contato/")
public class TipoContatoController {

    @Autowired
    private TipoContatoService service;

    @GetMapping(value = "/")
    @ResponseBody
    public ResponseEntity<List<TipoContatoDTO>> trazerTodosTiposDeContato () {
        try {
            return new ResponseEntity<>(service.buscarTodosTiposDeContato(), HttpStatus.OK);
        } catch (NaoExistemTiposDeContatoCadastrados e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<TipoContatoDTO> buscarPorId (@PathVariable Integer id) {
        try {
            return new ResponseEntity<>(service.buscarPorId(id), HttpStatus.OK);
        } catch (TipoContatoNaoEncontrado e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/{nome}")
    @ResponseBody
    public ResponseEntity<TipoContatoDTO> buscarPorNome (@PathVariable String nome) {
        try {
            return new ResponseEntity<>(service.buscarPorNome(nome), HttpStatus.OK);
        } catch (TipoContatoNaoEncontrado e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(value = "/salvar")
    @ResponseBody
    public ResponseEntity<TipoContatoDTO> salvarTipoContato (@RequestBody TipoContatoDTO tipoContato) {
        try {
            return new ResponseEntity<>(service.salvar(tipoContato.converter()), HttpStatus.CREATED);
        } catch (ArgumentosInvalidosTipoContato e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<TipoContatoDTO> editarTipoContato (@RequestBody TipoContatoDTO tipoContato, @PathVariable Integer id) {
        try {
            return new ResponseEntity<>(service.editar(tipoContato.converter(), id),HttpStatus.ACCEPTED);
        } catch (ArgumentosInvalidosTipoContato e){
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
