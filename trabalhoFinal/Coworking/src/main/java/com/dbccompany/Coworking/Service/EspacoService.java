package com.dbccompany.Coworking.Service;

import com.dbccompany.Coworking.DTO.EspacoDTO;
import com.dbccompany.Coworking.Entity.EspacoEntity;
import com.dbccompany.Coworking.Exception.Espaco.ArgumentosInvalidosEspaco;
import com.dbccompany.Coworking.Exception.Espaco.EspacoNaoEncontrado;
import com.dbccompany.Coworking.Exception.Espaco.NaoExistemEspacosCadastrados;
import com.dbccompany.Coworking.Repository.EspacoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class EspacoService {

    @Autowired
    private EspacoRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public EspacoDTO salvar (EspacoEntity espaco) throws ArgumentosInvalidosEspaco {
        try {
            return new EspacoDTO(this.salvarEEditar(espaco));
        } catch (Exception e) {
            throw new ArgumentosInvalidosEspaco();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public EspacoDTO editar (EspacoEntity espaco, Integer id) throws ArgumentosInvalidosEspaco {
        try {
            espaco.setId(id);
            return new EspacoDTO(this.salvarEEditar(espaco));
        } catch (Exception e) {
            throw new ArgumentosInvalidosEspaco();
        }
    }

    private EspacoEntity salvarEEditar (EspacoEntity espaco) {
        return repository.save(espaco);
    }

    public List<EspacoDTO> buscarTodosEspacos () throws NaoExistemEspacosCadastrados {
        try {
            return this.transformarList((List<EspacoEntity>) repository.findAll());
        } catch (Exception e) {
            throw new NaoExistemEspacosCadastrados();
        }
    }

    public EspacoDTO buscarPorId (Integer id) throws EspacoNaoEncontrado {
        try {
            return new EspacoDTO(repository.findById(id).get());
        } catch (Exception e) {
            throw new EspacoNaoEncontrado();
        }
    }

    public EspacoDTO buscarPorNome (String nome) throws EspacoNaoEncontrado {
        try {
            return new EspacoDTO(repository.findByNome(nome));
        } catch (Exception e) {
            throw new EspacoNaoEncontrado();
        }
    }

    private List<EspacoDTO> transformarList (List<EspacoEntity> lista) {
        List<EspacoDTO> retorno = new ArrayList<>();
        for ( EspacoEntity presente : lista ) {
            retorno.add((new EspacoDTO(presente)));
        }
        return retorno;
    }
}
