package com.dbccompany.Coworking.Repository;

import com.dbccompany.Coworking.Entity.*;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContratacaoRepository extends CrudRepository<ContratacaoEntity, Integer> {
    ContratacaoEntity save (ContratacaoEntity contratacao);

    List<ContratacaoEntity> findAllByEspaco (EspacoEntity espaco);

    List<ContratacaoEntity> findAllByCliente (ClienteEntity cliente);
}
