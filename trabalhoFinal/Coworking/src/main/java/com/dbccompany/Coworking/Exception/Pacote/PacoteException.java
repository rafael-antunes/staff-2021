package com.dbccompany.Coworking.Exception.Pacote;

public class PacoteException extends Exception{
    String mensagem;

    public PacoteException(String mensagem) {
        this.mensagem = mensagem;
    }

    public String getMensagem() {
        return mensagem;
    }
}
