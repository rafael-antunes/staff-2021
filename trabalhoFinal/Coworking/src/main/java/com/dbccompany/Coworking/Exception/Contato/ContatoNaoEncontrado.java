package com.dbccompany.Coworking.Exception.Contato;

public class ContatoNaoEncontrado extends ContatoException {
    public ContatoNaoEncontrado() {
        super("Contato não encontrado");
    }
}
