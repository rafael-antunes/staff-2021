package com.dbccompany.Coworking.Exception.ClientePacote;

public class NaoExistemClientesPacote extends ClientePacoteException{
    public NaoExistemClientesPacote() {
        super("Não existe nenhum cliente pacote!");
    }
}
