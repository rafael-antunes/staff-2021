package com.dbccompany.Coworking.Repository;

import com.dbccompany.Coworking.Entity.*;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClienteRepository extends CrudRepository<ClienteEntity, Integer> {
    ClienteEntity save(ClienteEntity cliente);

    ClienteEntity findByCPF(String CPF);
}
