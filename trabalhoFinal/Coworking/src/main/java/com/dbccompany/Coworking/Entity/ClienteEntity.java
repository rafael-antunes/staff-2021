package com.dbccompany.Coworking.Entity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class ClienteEntity {

    @Id
    @SequenceGenerator( name = "CLIENTE_SEQ", sequenceName = "CLIENTE_SEQ" )
    @GeneratedValue( generator = "CLIENTE_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @Column ( nullable = false )
    private String nome;

    @Column(length = 11 , columnDefinition = "CHAR", nullable = false, unique = true)
    private String CPF;

    @Column ( nullable = false )
    private Date dataNascimento;

    @OneToMany( fetch = FetchType.LAZY, mappedBy = "cliente", cascade = CascadeType.ALL )
    private List<ContatoEntity> contato;

    @OneToMany ( fetch = FetchType.LAZY, mappedBy = "cliente")
    private List<SaldoClienteEntity> saldoCliente;

    @OneToMany ( fetch = FetchType.LAZY, mappedBy = "cliente", cascade = CascadeType.ALL)
    private List<ClientePacoteEntity> clientePacote;

    @OneToMany ( fetch = FetchType.LAZY, mappedBy = "cliente", cascade = CascadeType.ALL)
    private List<ContratacaoEntity> contratacao;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCPF() {
        return CPF;
    }

    public void setCPF(String CPF) {
        this.CPF = CPF;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public List<ContatoEntity> getContato() {
        return contato;
    }

    public void setContato(List<ContatoEntity> contato) {
        this.contato = contato;
    }

    public List<SaldoClienteEntity> getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(List<SaldoClienteEntity> saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public List<ClientePacoteEntity> getClientePacote() {
        return clientePacote;
    }

    public void setClientePacote(List<ClientePacoteEntity> clientePacote) {
        this.clientePacote = clientePacote;
    }

    public List<ContratacaoEntity> getContratacao() {
        return contratacao;
    }

    public void setContratacao(List<ContratacaoEntity> contratacao) {
        this.contratacao = contratacao;
    }
}
