package com.dbccompany.Coworking.Exception.Pagamento;

public class ArgumentosInvalidosPagamento extends PagamentoException {
    public ArgumentosInvalidosPagamento() {
        super("Faltou campos");
    }
}
