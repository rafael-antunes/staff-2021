package com.dbccompany.Coworking.Entity;

import javax.persistence.*;

@Entity
public class PagamentoEntity {

    @Id
    @SequenceGenerator( name = "ESPACO_PACOTE_SEQ", sequenceName = "ESPACO_PACOTE_SEQ")
    @GeneratedValue( generator = "ESPACO_PACOTE_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @OneToOne ( cascade = CascadeType.ALL )
    private ClientePacoteEntity clientePacote;

    @OneToOne ( cascade = CascadeType.ALL )
    private ContratacaoEntity contratacao;

    @Enumerated ( EnumType.STRING )
    private TipoPagamentoEnum tipoPagamento;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ClientePacoteEntity getClientePacote() {
        return clientePacote;
    }

    public void setClientePacote(ClientePacoteEntity clientePacote) {
        this.clientePacote = clientePacote;
    }

    public ContratacaoEntity getContratacao() {
        return contratacao;
    }

    public void setContratacao(ContratacaoEntity contratacao) {
        this.contratacao = contratacao;
    }

    public TipoPagamentoEnum getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamentoEnum tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }
}
