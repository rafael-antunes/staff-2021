package com.dbccompany.Coworking.Service;

import com.dbccompany.Coworking.DTO.AcessoDTO;
import com.dbccompany.Coworking.Entity.*;
import com.dbccompany.Coworking.Exception.Acesso.AcessoNaoEncontrado;
import com.dbccompany.Coworking.Exception.Acesso.ArgumentosInvalidosAcesso;
import com.dbccompany.Coworking.Exception.Acesso.NaoExistemAcessos;
import com.dbccompany.Coworking.Exception.ClientePacote.ArgumentosInvalidosClientePacote;
import com.dbccompany.Coworking.Exception.ClientePacote.ClientePacoteNaoEncontrado;
import com.dbccompany.Coworking.Exception.ClientePacote.NaoExistemClientesPacote;
import com.dbccompany.Coworking.Repository.AcessoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

@Service
public class AcessoService {

    @Autowired
    private AcessoRepository repository;

    @Transactional
    public AcessoDTO acesso (AcessoEntity acesso) throws ArgumentosInvalidosAcesso {
        try {
            AcessoEntity ultimoAcesso = repository.findAllBySaldoClienteId(acesso.getSaldoCliente().getId()).get(repository.findAllBySaldoClienteId(acesso.getSaldoCliente().getId()).size() - 1);
            AcessoDTO acessoNovo;
            if ( ultimoAcesso.isEntrada() ) {
                acesso.setEntrada(Boolean.FALSE);
                acesso.setData(LocalDateTime.now());
                acesso.getSaldoCliente().setQuantidade(acesso.getSaldoCliente().getQuantidade() - (int)ChronoUnit.MINUTES.between(ultimoAcesso.getData(), acesso.getData()));
                acessoNovo = new AcessoDTO(this.salvarEEditar(acesso));
            } else {
                acesso.setData(LocalDateTime.now());
                acesso.setEntrada(Boolean.TRUE);
                acessoNovo = new AcessoDTO(this.salvarEEditar(acesso));
            }
            return acessoNovo;
       } catch (Exception e) {
            throw new ArgumentosInvalidosAcesso();
        }
    }

    @Transactional
    public AcessoDTO editar (AcessoEntity acesso, Integer id) throws ArgumentosInvalidosAcesso {
        try {
            acesso.setId(id);
            return new AcessoDTO(this.salvarEEditar(acesso));
        } catch (Exception e) {
            throw new ArgumentosInvalidosAcesso();
        }
    }

    public List<AcessoDTO> buscarTodosAcessos () throws NaoExistemAcessos {
        try {
            return this.transformarList((List<AcessoEntity>) repository.findAll());
        } catch (Exception e) {
            throw new NaoExistemAcessos();
        }
    }

    public AcessoDTO buscarPorId (Integer id) throws AcessoNaoEncontrado {
        try {
            return new AcessoDTO(repository.findById(id).get());
        } catch (Exception e) {
            throw new AcessoNaoEncontrado();
        }
    }

    public List<AcessoDTO> buscarPorSaldoClienteId (SaldoClienteId saldoClienteId) throws AcessoNaoEncontrado {
        try {
            return this.transformarList(repository.findAllBySaldoClienteId(saldoClienteId));
        } catch (Exception e) {
            throw new AcessoNaoEncontrado();
        }
    }

    public AcessoDTO buscarPorData (LocalDateTime data) throws AcessoNaoEncontrado {
        try {
            return new AcessoDTO(repository.findByData(data));
        } catch (Exception e) {
            throw new AcessoNaoEncontrado();
        }
    }

    private AcessoEntity salvarEEditar (AcessoEntity acesso) {
        return repository.save(acesso);
    }

    private List<AcessoDTO> transformarList (List<AcessoEntity> lista) {
        List<AcessoDTO> retorno = new ArrayList<>();
        for ( AcessoEntity presente : lista ) {
            retorno.add(new AcessoDTO(presente));
        }
        return retorno;
    }
}
