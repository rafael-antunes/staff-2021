package com.dbccompany.Coworking.Controller;

import com.dbccompany.Coworking.DTO.ClienteDTO;
import com.dbccompany.Coworking.DTO.ContratacaoDTO;
import com.dbccompany.Coworking.DTO.EspacoDTO;
import com.dbccompany.Coworking.Exception.Contratacao.ArgumentosInvalidosContratacao;
import com.dbccompany.Coworking.Exception.Contratacao.ContratacaoNaoEncontrada;
import com.dbccompany.Coworking.Exception.Contratacao.NaoExistemContratacoes;
import com.dbccompany.Coworking.Service.ContratacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Service
@RequestMapping(value = "/api/contratacao")
public class ContratacaoController {

    @Autowired
    private ContratacaoService service;

    @GetMapping(value = "/")
    @ResponseBody
    public ResponseEntity<List<ContratacaoDTO>> buscarTodasContratacoes () {
        try {
            return new ResponseEntity<>(service.buscarTodasContratacoes(), HttpStatus.OK);
        } catch (NaoExistemContratacoes e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<ContratacaoDTO> buscarPorId (@PathVariable Integer id) {
        try {
            return new ResponseEntity<>(service.buscarPorId(id), HttpStatus.OK);
        } catch (ContratacaoNaoEncontrada e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(value = "/salvar")
    @ResponseBody
    public ResponseEntity<ContratacaoDTO> salvarContratacao (@RequestBody ContratacaoDTO contratacao) {
        try {
            return new ResponseEntity<>(service.salvar(contratacao.converter()), HttpStatus.CREATED);
        } catch (ArgumentosInvalidosContratacao e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<ContratacaoDTO> editarContratacao (@RequestBody ContratacaoDTO contratacao, @PathVariable Integer id) {
        try {
            return new ResponseEntity<>(service.editar(contratacao.converter(), id), HttpStatus.ACCEPTED);
        } catch (ArgumentosInvalidosContratacao e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/busca/porCliente")
    @ResponseBody
    public ResponseEntity<List<ContratacaoDTO>> buscarTodasContratacoesPorCliente (@RequestBody ClienteDTO cliente) {
        try {
            return new ResponseEntity<>(service.buscarPorCliente(cliente.converter()), HttpStatus.OK);
        } catch (ContratacaoNaoEncontrada e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/busca/porEspaco")
    @ResponseBody
    public ResponseEntity<List<ContratacaoDTO>> buscarTodasContratacoesPorEspaco (@RequestBody EspacoDTO espaco) {
        try {
            return new ResponseEntity<>(service.buscarPorEspaco(espaco.converter()), HttpStatus.OK);
        } catch (ContratacaoNaoEncontrada e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
