package com.dbccompany.Coworking.Exception.Acesso;

public class AcessoNaoEncontrado extends AcessoException{
    public AcessoNaoEncontrado() {
        super("Acesso não encontrado");
    }

}
