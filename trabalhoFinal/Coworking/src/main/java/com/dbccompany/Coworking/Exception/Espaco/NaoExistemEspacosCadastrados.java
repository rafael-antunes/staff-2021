package com.dbccompany.Coworking.Exception.Espaco;

public class NaoExistemEspacosCadastrados extends EspacoException {
    public NaoExistemEspacosCadastrados() {
        super("Não existe nenhum usuário cadastrado!");
    }
}
