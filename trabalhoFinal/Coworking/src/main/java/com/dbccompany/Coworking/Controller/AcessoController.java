package com.dbccompany.Coworking.Controller;

import com.dbccompany.Coworking.DTO.*;
import com.dbccompany.Coworking.Exception.Acesso.AcessoNaoEncontrado;
import com.dbccompany.Coworking.Exception.Acesso.ArgumentosInvalidosAcesso;
import com.dbccompany.Coworking.Exception.Acesso.NaoExistemAcessos;
import com.dbccompany.Coworking.Service.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@Controller
@RequestMapping(value = "/api/acesso")
public class AcessoController {

    @Autowired
    private AcessoService service;

    @GetMapping(value = "/")
    @ResponseBody
    public ResponseEntity<List<AcessoDTO>> buscarTodosAcessos () {
        try {
            return new ResponseEntity<>(service.buscarTodosAcessos(), HttpStatus.OK);
        } catch (NaoExistemAcessos e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<AcessoDTO> buscarPorId (@PathVariable Integer id) {
        try {
            return new ResponseEntity<>(service.buscarPorId(id), HttpStatus.OK);
        } catch (AcessoNaoEncontrado e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(value = "/acessar")
    @ResponseBody
    public ResponseEntity<AcessoDTO> acessar (@RequestBody AcessoDTO acesso) {
        try {
            return new ResponseEntity<>(service.acesso(acesso.converter()), HttpStatus.CREATED);
        } catch (ArgumentosInvalidosAcesso e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<AcessoDTO> editarClientePacote (@RequestBody AcessoDTO acesso, @PathVariable Integer id) {
        try {
            return new ResponseEntity<>(service.editar(acesso.converter(), id), HttpStatus.ACCEPTED);
        } catch (ArgumentosInvalidosAcesso e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/busca/porSaldo")
    @ResponseBody
    public ResponseEntity<List<AcessoDTO>> buscarTodosPorSaldoClienteId (@RequestBody SaldoClienteIdDTO saldoClienteId) {
        try {
            return new ResponseEntity<>(service.buscarPorSaldoClienteId(saldoClienteId.converter()), HttpStatus.OK);
        } catch (AcessoNaoEncontrado e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/busca/porEspaco")
    @ResponseBody
    public ResponseEntity<AcessoDTO> buscarPorData (@RequestBody LocalDateTime data) {
        try {
            return new ResponseEntity<>(service.buscarPorData(data), HttpStatus.OK);
        } catch (AcessoNaoEncontrado e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
