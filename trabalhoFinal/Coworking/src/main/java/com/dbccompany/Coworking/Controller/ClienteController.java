package com.dbccompany.Coworking.Controller;

import com.dbccompany.Coworking.DTO.ClienteDTO;
import com.dbccompany.Coworking.DTO.ContatoDTO;
import com.dbccompany.Coworking.Entity.ClienteEntity;
import com.dbccompany.Coworking.Entity.ContatoEntity;
import com.dbccompany.Coworking.Exception.Cliente.ArgumentosInvalidosCliente;
import com.dbccompany.Coworking.Exception.Cliente.ClienteNaoEncontrado;
import com.dbccompany.Coworking.Exception.Cliente.NaoExistemClientesCadastrados;
import com.dbccompany.Coworking.Service.ClienteService;
import com.dbccompany.Coworking.Service.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = "/api/cliente")
public class ClienteController {

    @Autowired
    private ClienteService service;

    @Autowired
    private ContatoService contatoService;

    @GetMapping(value = "/")
    @ResponseBody
    public ResponseEntity<List<ClienteDTO>> trazerTodosClientes() {
        try {
            return new ResponseEntity<>(service.buscarTodosClientes(), HttpStatus.OK);
        } catch (NaoExistemClientesCadastrados e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<ClienteDTO> buscarPorId(@PathVariable Integer id) {
        try{
            return new ResponseEntity<>(service.buscarPorId(id), HttpStatus.OK);
        } catch (ClienteNaoEncontrado e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/{CPF}")
    @ResponseBody
    public ResponseEntity<ClienteDTO> buscarPorCPF(@PathVariable String CPF) {
        try{
            return new ResponseEntity<>(service.buscarPorCPF(CPF), HttpStatus.OK);
        } catch (ClienteNaoEncontrado e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(value = "/salvar")
    @ResponseBody
    public ResponseEntity<ClienteDTO> salvarCliente (@RequestBody ClienteDTO cliente) {
        try {
            List<ContatoEntity> contatos = new ArrayList<>();
            for ( ContatoDTO contato : cliente.getContato() ) {
                contatos.add(contato.converter());
            }
            ClienteDTO clienteDTO = service.salvar(cliente.converter(), contatos);
            ResponseEntity<ClienteDTO> resposta =  new ResponseEntity<>(clienteDTO, HttpStatus.CREATED);
            if ( resposta.getBody() != null ) {
                return resposta;
            } else {
                System.err.println(new ArgumentosInvalidosCliente().getMensagem());
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } catch (ArgumentosInvalidosCliente e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<ClienteDTO> editarCliente (@RequestBody ClienteDTO cliente, @PathVariable Integer id) {
        try {
            return new ResponseEntity<>(service.editar(cliente.converter(), id), HttpStatus.ACCEPTED);
        } catch (ArgumentosInvalidosCliente e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
