package com.dbccompany.Coworking.Exception.Cliente;

public class NaoExistemClientesCadastrados extends ClienteException {
    public NaoExistemClientesCadastrados() {
        super("Não existe nenhum cliente cadastrado!");
    }
}
