package com.dbccompany.Coworking.Controller;

import com.dbccompany.Coworking.DTO.UsuarioDTO;
import com.dbccompany.Coworking.Entity.UsuarioEntity;
import com.dbccompany.Coworking.Exception.Usuario.ArgumentosInvalidosUsuario;
import com.dbccompany.Coworking.Exception.Usuario.NaoExistemUsuariosCadastrados;
import com.dbccompany.Coworking.Exception.Usuario.UsuarioNaoEncontrado;
import com.dbccompany.Coworking.Service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/api/usuario")
public class UsuarioController {

    @Autowired
    private UsuarioService service;

    @GetMapping(value = "/")
    @ResponseBody
    public ResponseEntity<List<UsuarioDTO>> trazerTodosUsuarios() {
        try {
            return new ResponseEntity<>(service.buscarTodosUsuarios(), HttpStatus.OK);
        } catch (NaoExistemUsuariosCadastrados e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<UsuarioDTO> buscarPorId(@PathVariable Integer id) {
        try{
            return new ResponseEntity<>(service.buscarPorId(id), HttpStatus.OK);
        } catch (UsuarioNaoEncontrado e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/{login}")
    @ResponseBody
    public ResponseEntity<UsuarioDTO> buscarPorLogin(@PathVariable String login) {
        try {
            return new ResponseEntity<>(service.buscarPorLogin(login), HttpStatus.OK);
        } catch (UsuarioNaoEncontrado e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(value = "/salvar")
    @ResponseBody
    public ResponseEntity<UsuarioDTO> salvarUsuario(@RequestBody UsuarioDTO usuario) {
        try {
            return new ResponseEntity<>(service.salvar(usuario.converter()), HttpStatus.CREATED);
        } catch (ArgumentosInvalidosUsuario e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<UsuarioDTO> editarUsuario(@RequestBody UsuarioDTO usuario, @PathVariable Integer id) {
        try {
            return new ResponseEntity<>(service.editar(usuario.converter(), id), HttpStatus.ACCEPTED);
        } catch (ArgumentosInvalidosUsuario e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
