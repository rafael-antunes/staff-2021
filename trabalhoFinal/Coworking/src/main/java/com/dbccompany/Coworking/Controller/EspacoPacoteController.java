package com.dbccompany.Coworking.Controller;

import com.dbccompany.Coworking.DTO.EspacoDTO;
import com.dbccompany.Coworking.DTO.EspacoPacoteDTO;
import com.dbccompany.Coworking.DTO.PacoteDTO;
import com.dbccompany.Coworking.Exception.EspacoPacote.ArgumentosInvalidosEspacoPacote;
import com.dbccompany.Coworking.Exception.EspacoPacote.EspacoPacoteNaoEncontrado;
import com.dbccompany.Coworking.Exception.EspacoPacote.NaoExistemEspacosPacote;
import com.dbccompany.Coworking.Service.EspacoPacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/api/espacoPacote")
public class EspacoPacoteController {

    @Autowired
    private EspacoPacoteService service;

    @GetMapping(value = "/")
    @ResponseBody
    public ResponseEntity<List<EspacoPacoteDTO>> buscarTodosEspacosPacote () {
        try {
            return new ResponseEntity<>(service.buscarTodosEspacosPacote(), HttpStatus.OK);
        } catch (NaoExistemEspacosPacote e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<EspacoPacoteDTO> buscarPorId (@PathVariable Integer id) {
        try {
            return new ResponseEntity<>(service.buscarPorId(id), HttpStatus.OK);
        } catch (EspacoPacoteNaoEncontrado e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(value = "/salvar")
    @ResponseBody
    public ResponseEntity<EspacoPacoteDTO> salvarEspacoPacote (@RequestBody EspacoPacoteDTO espacoPacote) {
        try {
            return new ResponseEntity<>(service.salvar(espacoPacote.converter()), HttpStatus.CREATED);
        } catch (ArgumentosInvalidosEspacoPacote e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<EspacoPacoteDTO> editarEspacoPacote (@RequestBody EspacoPacoteDTO espacoPacote, @PathVariable Integer id) {
        try {
            return new ResponseEntity<>(service.editar(espacoPacote.converter(), id), HttpStatus.ACCEPTED);
        } catch (ArgumentosInvalidosEspacoPacote e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/busca/porPacote")
    @ResponseBody
    public ResponseEntity<List<EspacoPacoteDTO>> buscarTodasEspacosPacotePorPacote (@RequestBody PacoteDTO pacote) {
        try {
            return new ResponseEntity<>(service.buscarPorPacote(pacote.converter()), HttpStatus.OK);
        } catch (EspacoPacoteNaoEncontrado e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/busca/porEspaco")
    @ResponseBody
    public ResponseEntity<List<EspacoPacoteDTO>> buscarTodasEspacosPacotePorEspaco (@RequestBody EspacoDTO espaco) {
        try {
            return new ResponseEntity<>(service.buscarPorEspaco(espaco.converter()), HttpStatus.OK);
        } catch (EspacoPacoteNaoEncontrado e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
