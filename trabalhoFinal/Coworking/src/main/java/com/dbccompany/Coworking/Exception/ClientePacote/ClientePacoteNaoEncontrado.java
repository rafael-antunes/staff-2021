package com.dbccompany.Coworking.Exception.ClientePacote;

public class ClientePacoteNaoEncontrado extends ClientePacoteException {
    public ClientePacoteNaoEncontrado() {
        super("Cliente pacote não encontrado");
    }

}
