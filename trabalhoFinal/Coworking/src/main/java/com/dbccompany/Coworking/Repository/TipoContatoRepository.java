package com.dbccompany.Coworking.Repository;

import com.dbccompany.Coworking.Entity.TipoContatoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TipoContatoRepository extends CrudRepository<TipoContatoEntity, Integer> {
    TipoContatoEntity save (TipoContatoEntity tipoContato);

    TipoContatoEntity findByNome (String nome);
}
