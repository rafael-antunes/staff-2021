package com.dbccompany.Coworking.Exception.SaldoCliente;

public class SaldoClienteException extends Exception {
    String mensagem;

    public SaldoClienteException(String mensagem) {
        this.mensagem = mensagem;
    }

    public String getMensagem() {
        return mensagem;
    }
}
