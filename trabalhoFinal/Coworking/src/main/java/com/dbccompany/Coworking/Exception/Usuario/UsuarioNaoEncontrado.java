package com.dbccompany.Coworking.Exception.Usuario;

import com.dbccompany.Coworking.Exception.Usuario.UsuarioException;

public class UsuarioNaoEncontrado extends UsuarioException {
    public UsuarioNaoEncontrado() {
        super("Este usuário não existe");
    }
}
