package com.dbccompany.Coworking.Repository;

import com.dbccompany.Coworking.Entity.UsuarioEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuarioRepository extends CrudRepository<UsuarioEntity, Integer> {
    UsuarioEntity save (UsuarioEntity usuario);

    UsuarioEntity findByLogin (String login);
}
