package com.dbccompany.Coworking.Controller;

import com.dbccompany.Coworking.DTO.PacoteDTO;
import com.dbccompany.Coworking.Exception.Pacote.ArgumentosInvalidosPacote;
import com.dbccompany.Coworking.Exception.Pacote.NaoExistemPacotes;
import com.dbccompany.Coworking.Exception.Pacote.PacoteNaoEncontrado;
import com.dbccompany.Coworking.Service.PacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/api/pacote")
public class PacoteController {

    @Autowired
    private PacoteService service;

    @GetMapping(value = "/")
    @ResponseBody
    public ResponseEntity<List<PacoteDTO>> buscarTodosPacotes () {
        try {
            return new ResponseEntity<>(service.buscarTodosPacotes(), HttpStatus.OK);
        } catch (NaoExistemPacotes e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<PacoteDTO> buscarPorId (@PathVariable Integer id) {
        try {
            return new ResponseEntity<>(service.buscarPorId(id), HttpStatus.OK);
        } catch (PacoteNaoEncontrado e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(value = "/salvar")
    @ResponseBody
    public ResponseEntity<PacoteDTO> salvarPacote (@RequestBody PacoteDTO pacote) {
        try {
            return new ResponseEntity<>(service.salvar(pacote.converter()), HttpStatus.CREATED);
        } catch (ArgumentosInvalidosPacote e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<PacoteDTO> editarPacote (@RequestBody PacoteDTO pacote, @PathVariable Integer id) {
        try {
            return new ResponseEntity<>(service.editar(pacote.converter(), id), HttpStatus.ACCEPTED);
        } catch (ArgumentosInvalidosPacote e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
