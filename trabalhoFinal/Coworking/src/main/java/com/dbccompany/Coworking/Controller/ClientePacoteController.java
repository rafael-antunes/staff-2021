package com.dbccompany.Coworking.Controller;

import com.dbccompany.Coworking.DTO.*;
import com.dbccompany.Coworking.Exception.ClientePacote.ArgumentosInvalidosClientePacote;
import com.dbccompany.Coworking.Exception.ClientePacote.ClientePacoteNaoEncontrado;
import com.dbccompany.Coworking.Exception.ClientePacote.NaoExistemClientesPacote;
import com.dbccompany.Coworking.Service.ClientePacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/api/clientePacote")
public class ClientePacoteController {

    @Autowired
    private ClientePacoteService service;

    @GetMapping(value = "/")
    @ResponseBody
    public ResponseEntity<List<ClientePacoteDTO>> buscarTodosClientePacote () {
        try {
            return new ResponseEntity<>(service.buscarTodasContratacoes(), HttpStatus.OK);
        } catch (NaoExistemClientesPacote e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<ClientePacoteDTO> buscarPorId (@PathVariable Integer id) {
        try {
            return new ResponseEntity<>(service.buscarPorId(id), HttpStatus.OK);
        } catch (ClientePacoteNaoEncontrado e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(value = "/salvar")
    @ResponseBody
    public ResponseEntity<ClientePacoteDTO> salvarClientePacote (@RequestBody ClientePacoteDTO clientePacote) {
        try {
            return new ResponseEntity<>(service.salvar(clientePacote.converter()), HttpStatus.CREATED);
        } catch (ArgumentosInvalidosClientePacote e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<ClientePacoteDTO> editarClientePacote (@RequestBody ClientePacoteDTO clientePacote, @PathVariable Integer id) {
        try {
            return new ResponseEntity<>(service.editar(clientePacote.converter(), id), HttpStatus.ACCEPTED);
        } catch (ArgumentosInvalidosClientePacote e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/busca/porCliente")
    @ResponseBody
    public ResponseEntity<List<ClientePacoteDTO>> buscarTodosClientePacotePorCliente (@RequestBody ClienteDTO cliente) {
        try {
            return new ResponseEntity<>(service.buscarPorCliente(cliente.converter()), HttpStatus.OK);
        } catch (ClientePacoteNaoEncontrado e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/busca/porEspaco")
    @ResponseBody
    public ResponseEntity<List<ClientePacoteDTO>> buscarTodosClientePacotePorPacote (@RequestBody PacoteDTO pacote) {
        try {
            return new ResponseEntity<>(service.buscarPorPacote(pacote.converter()), HttpStatus.OK);
        } catch (ClientePacoteNaoEncontrado e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
