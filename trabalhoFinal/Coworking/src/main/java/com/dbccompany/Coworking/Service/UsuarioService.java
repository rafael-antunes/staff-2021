package com.dbccompany.Coworking.Service;

import com.dbccompany.Coworking.DTO.UsuarioDTO;
import com.dbccompany.Coworking.Entity.UsuarioEntity;
import com.dbccompany.Coworking.Exception.Usuario.ArgumentosInvalidosUsuario;
import com.dbccompany.Coworking.Exception.Usuario.NaoExistemUsuariosCadastrados;
import com.dbccompany.Coworking.Exception.Usuario.UsuarioNaoEncontrado;
import com.dbccompany.Coworking.Repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public UsuarioDTO salvar (UsuarioEntity usuario) throws ArgumentosInvalidosUsuario {
        try {
            return new UsuarioDTO(this.salvarEEditar(usuario));
        } catch (Exception e) {
            throw new ArgumentosInvalidosUsuario();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public UsuarioDTO editar (UsuarioEntity usuario, Integer id) throws ArgumentosInvalidosUsuario {
        try {
            usuario.setId(id);
            this.salvarEEditar(usuario);
            return new UsuarioDTO(usuario);
        } catch (Exception e) {
            throw new ArgumentosInvalidosUsuario();
        }

    }

    private UsuarioEntity salvarEEditar (UsuarioEntity usuario) {
        return repository.save(usuario);
    }

    public List<UsuarioDTO> buscarTodosUsuarios () throws NaoExistemUsuariosCadastrados {
        try {
            return this.transformarList((List<UsuarioEntity>) repository.findAll());
        } catch ( Exception e ) {
            throw new NaoExistemUsuariosCadastrados();
        }
    }

    public UsuarioDTO buscarPorId (Integer id) throws UsuarioNaoEncontrado {
        try {
            return new UsuarioDTO(repository.findById(id).get());
        } catch (Exception e) {
            throw new UsuarioNaoEncontrado();
        }
    }

    public UsuarioDTO buscarPorLogin (String login) throws UsuarioNaoEncontrado {
        try {
            return new UsuarioDTO(repository.findByLogin(login));
        } catch ( Exception e ) {
            throw new UsuarioNaoEncontrado();
        }
    }

    private List<UsuarioDTO> transformarList (List<UsuarioEntity> lista) {
        List<UsuarioDTO> retorno = new ArrayList<>();
        for ( UsuarioEntity presente : lista ) {
            retorno.add(new UsuarioDTO(presente));
        }
        return retorno;
    }
}
