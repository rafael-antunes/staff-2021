package com.dbccompany.Coworking.Repository;

import com.dbccompany.Coworking.Entity.ClientePacoteEntity;
import com.dbccompany.Coworking.Entity.ContratacaoEntity;
import com.dbccompany.Coworking.Entity.PagamentoEntity;
import com.dbccompany.Coworking.Entity.TipoPagamentoEnum;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PagamentoRepository extends CrudRepository<PagamentoEntity, Integer> {
    PagamentoEntity save (PagamentoEntity pagamento);

    PagamentoEntity findByClientePacote (ClientePacoteEntity Clientepacote);

    PagamentoEntity findByContratacao (ContratacaoEntity contratacao);


}
