package com.dbccompany.Coworking.DTO;

import com.dbccompany.Coworking.Entity.ContratacaoEntity;
import com.dbccompany.Coworking.Entity.TipoContratacaoEnum;

public class ContratacaoDTO {

    private TipoContratacaoEnum tipoContratacao;
    private Integer quantidade;
    private double desconto;
    private Integer prazo;
    private String valor;
    private EspacoDTO espaco;
    private ClienteDTO cliente;

    public ContratacaoDTO(ContratacaoEntity contratacao) {
        this.tipoContratacao = contratacao.getTipoContratacao();
        this.quantidade = contratacao.getQuantidade();
        this.desconto = contratacao.getDesconto();
        this.prazo = contratacao.getPrazo();
        this.espaco = new EspacoDTO(contratacao.getEspaco());
        this.cliente = new ClienteDTO(contratacao.getCliente());
    }

    public ContratacaoDTO() {
    }

    public ContratacaoEntity converter() {
        ContratacaoEntity contratacao = new ContratacaoEntity();
        contratacao.setTipoContratacao(this.tipoContratacao);
        contratacao.setQuantidade(this.quantidade);
        contratacao.setDesconto(this.desconto);
        contratacao.setPrazo(this.prazo);
        contratacao.setEspaco(this.espaco.converter());
        contratacao.setCliente(this.cliente.converter());
        return contratacao;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public double getDesconto() {
        return desconto;
    }

    public void setDesconto(double desconto) {
        this.desconto = desconto;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public EspacoDTO getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoDTO espaco) {
        this.espaco = espaco;
    }

    public ClienteDTO getCliente() {
        return cliente;
    }

    public void setCliente(ClienteDTO cliente) {
        this.cliente = cliente;
    }
}
