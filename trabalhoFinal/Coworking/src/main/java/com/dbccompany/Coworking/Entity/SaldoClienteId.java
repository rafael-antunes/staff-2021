package com.dbccompany.Coworking.Entity;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class SaldoClienteId implements Serializable {
    private Integer id_cliente;
    private Integer id_espaco;

    public SaldoClienteId(Integer id_cliente, Integer id_espaco) {
        this.id_cliente = id_cliente;
        this.id_espaco = id_espaco;
    }

    public SaldoClienteId() {
    }

    public Integer getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(Integer id_cliente) {
        this.id_cliente = id_cliente;
    }

    public Integer getId_espaco() {
        return id_espaco;
    }

    public void setId_espaco(Integer id_espaco) {
        this.id_espaco = id_espaco;
    }
}
