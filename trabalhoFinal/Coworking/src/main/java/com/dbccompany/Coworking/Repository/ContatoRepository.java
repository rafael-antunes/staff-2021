package com.dbccompany.Coworking.Repository;

import com.dbccompany.Coworking.Entity.ClienteEntity;
import com.dbccompany.Coworking.Entity.ContatoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContatoRepository extends CrudRepository<ContatoEntity, Integer> {
    ContatoEntity save (ContatoEntity contato);

    List<ContatoEntity> findAllByCliente (ClienteEntity cliente);
}
