package com.dbccompany.Coworking.Exception.EspacoPacote;

public class NaoExistemEspacosPacote extends EspacoPacoteException{
    public NaoExistemEspacosPacote() {
        super("Não existe nenhum espaço em pacote!");
    }
}
