package com.dbccompany.Coworking.DTO;

import com.dbccompany.Coworking.Entity.PagamentoEntity;
import com.dbccompany.Coworking.Entity.TipoPagamentoEnum;

public class PagamentoDTO {

    private TipoPagamentoEnum tipoPagamento;
    ContratacaoDTO contratacao;
    ClientePacoteDTO clientePacote;

    public PagamentoDTO(PagamentoEntity pagamento) {
        this.tipoPagamento = pagamento.getTipoPagamento();
        this.clientePacote = new ClientePacoteDTO(pagamento.getClientePacote());
        this.contratacao = new ContratacaoDTO(pagamento.getContratacao());
    }

    public PagamentoDTO() {
    }

    public PagamentoEntity converter() {
        PagamentoEntity pagamento = new PagamentoEntity();
        pagamento.setTipoPagamento(this.tipoPagamento);
        pagamento.setClientePacote(this.clientePacote.converter());
        pagamento.setContratacao(this.contratacao.converter());
        return pagamento;
    }

    public TipoPagamentoEnum getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamentoEnum tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }

    public ContratacaoDTO getContratacao() {
        return contratacao;
    }

    public void setContratacao(ContratacaoDTO contratacao) {
        this.contratacao = contratacao;
    }

    public ClientePacoteDTO getClientePacote() {
        return clientePacote;
    }

    public void setClientePacote(ClientePacoteDTO clientePacote) {
        this.clientePacote = clientePacote;
    }
}
