package com.dbccompany.Coworking.Service;

import com.dbccompany.Coworking.Conversor;
import com.dbccompany.Coworking.DTO.PagamentoDTO;
import com.dbccompany.Coworking.Entity.*;
import com.dbccompany.Coworking.Exception.Pagamento.ArgumentosInvalidosPagamento;
import com.dbccompany.Coworking.Exception.Pagamento.NaoExistemPagamentos;
import com.dbccompany.Coworking.Exception.Pagamento.PagamentoNaoEncontrado;
import com.dbccompany.Coworking.Exception.SaldoCliente.ArgumentosInvalidosSaldoCliente;
import com.dbccompany.Coworking.Exception.SaldoCliente.NaoExistemSaldosCliente;
import com.dbccompany.Coworking.Exception.SaldoCliente.SaldoClienteNaoEncontrado;
import com.dbccompany.Coworking.Repository.EspacoPacoteRepository;
import com.dbccompany.Coworking.Repository.PagamentoRepository;
import com.dbccompany.Coworking.Repository.SaldoClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository repository;

    @Autowired
    private SaldoClienteRepository saldoClienteRepository;

    @Autowired
    private EspacoPacoteRepository espacoPacoteRepository;

    @Transactional
    public PagamentoDTO pagarContratacao (PagamentoEntity pagamento) throws ArgumentosInvalidosPagamento {
        try {
            PagamentoDTO pagamentoNovo = new PagamentoDTO(this.salvarEEditar(pagamento));
            if ( !saldoClienteRepository.findById(new SaldoClienteId(pagamento.getContratacao().getCliente().getId(), pagamento.getContratacao().getEspaco().getId())).isPresent()) {
                SaldoClienteEntity saldoCliente = new SaldoClienteEntity();
                saldoCliente.setId(new SaldoClienteId(pagamento.getContratacao().getCliente().getId(), pagamento.getContratacao().getEspaco().getId()));
                saldoCliente.setCliente(pagamento.getContratacao().getCliente());
                saldoCliente.setEspaco(pagamento.getContratacao().getEspaco());
                saldoCliente.setQuantidade(pagamento.getContratacao().getQuantidade());
                saldoCliente.setTipoContratacao(pagamento.getContratacao().getTipoContratacao());
                saldoCliente.setVencimento(Date.from(Instant.now().plus(pagamento.getContratacao().getPrazo(), ChronoUnit.DAYS)));
            } else {
                SaldoClienteEntity saldoCliente = saldoClienteRepository.findById(new SaldoClienteId(pagamento.getContratacao().getCliente().getId(), pagamento.getContratacao().getEspaco().getId())).get();
                saldoCliente.getVencimento().setTime(saldoCliente.getVencimento().getTime() + (pagamento.getContratacao().getPrazo() * new Conversor().paraMilisegundos(pagamento.getContratacao().getTipoContratacao())));
                saldoCliente.setQuantidade(saldoCliente.getQuantidade() + (pagamento.getContratacao().getQuantidade() * new Conversor().converterTempo(pagamento.getContratacao().getTipoContratacao())));
            }
            return pagamentoNovo;
        } catch (Exception e) {
            throw new ArgumentosInvalidosPagamento();
        }
    }

    @Transactional
    public PagamentoDTO pagarPacote (PagamentoEntity pagamento) throws ArgumentosInvalidosPagamento {
        try {
            PagamentoDTO pagamentoNovo = new PagamentoDTO(this.salvarEEditar(pagamento));
            List<EspacoPacoteEntity> lista = espacoPacoteRepository.findAllByPacote(pagamento.getClientePacote().getPacote());
            for (EspacoPacoteEntity espacoPacote : lista ) {
                if ( !saldoClienteRepository.findById(new SaldoClienteId(pagamento.getClientePacote().getCliente().getId(), espacoPacote.getPacote().getId())).isPresent()) {
                    SaldoClienteEntity saldoCliente = new SaldoClienteEntity();
                    saldoCliente.setId(new SaldoClienteId(pagamento.getClientePacote().getCliente().getId(), espacoPacote.getPacote().getId()));
                    saldoCliente.setCliente(pagamento.getClientePacote().getCliente());
                    saldoCliente.setEspaco(espacoPacote.getEspaco());
                    saldoCliente.setQuantidade((espacoPacote.getQuantidade()*pagamento.getClientePacote().getQuantidade()));
                    saldoCliente.setTipoContratacao(espacoPacote.getTipoContratacao());
                    saldoCliente.setVencimento(Date.from(Instant.now().plus((espacoPacote.getQuantidade()*pagamento.getClientePacote().getQuantidade()), ChronoUnit.DAYS)));
                } else {
                    SaldoClienteEntity saldoCliente = saldoClienteRepository.findById(new SaldoClienteId(pagamento.getClientePacote().getCliente().getId(), espacoPacote.getPacote().getId())).get();
                    saldoCliente.getVencimento().setTime(saldoCliente.getVencimento().getTime() + (espacoPacote.getPrazo() * new Conversor().paraMilisegundos(espacoPacote.getTipoContratacao())));
                    saldoCliente.setQuantidade(saldoCliente.getQuantidade() + ((pagamento.getClientePacote().getQuantidade() * espacoPacote.getQuantidade() * new Conversor().converterTempo(espacoPacote.getTipoContratacao()))));
                }
            }
            return pagamentoNovo;
        } catch (Exception e) {
            throw new ArgumentosInvalidosPagamento();
        }
    }

    @Transactional
    public PagamentoDTO editar (PagamentoEntity pagamento, Integer id) throws ArgumentosInvalidosPagamento {
        try {
            pagamento.setId(id);
            return new PagamentoDTO(this.salvarEEditar(pagamento));
        } catch (Exception e) {
            throw new ArgumentosInvalidosPagamento();
        }
    }

    public List<PagamentoDTO> buscarTodosPagamentos () throws NaoExistemPagamentos {
        try {
            return this.transformarList((List<PagamentoEntity>) repository.findAll());
        } catch (Exception e) {
            throw new NaoExistemPagamentos();
        }
    }

    public PagamentoDTO buscarPorId (Integer id) throws PagamentoNaoEncontrado {
        try {
            return new PagamentoDTO(repository.findById(id).get());
        } catch (Exception e) {
            throw new PagamentoNaoEncontrado();
        }
    }

    public PagamentoDTO buscarPorClientePacote (ClientePacoteEntity clientePacote) throws PagamentoNaoEncontrado {
        try {
            return new PagamentoDTO(repository.findByClientePacote(clientePacote));
        } catch (Exception e) {
            throw new PagamentoNaoEncontrado();
        }
    }

    public PagamentoDTO buscarPorContratacao (ContratacaoEntity contratacao) throws PagamentoNaoEncontrado {
        try {
            return new PagamentoDTO(repository.findByContratacao(contratacao));
        } catch (Exception e) {
            throw new PagamentoNaoEncontrado();
        }
    }

    private PagamentoEntity salvarEEditar (PagamentoEntity pagamento) {
        return repository.save(pagamento);
    }

    private List<PagamentoDTO> transformarList (List<PagamentoEntity> lista) {
        List<PagamentoDTO> retorno = new ArrayList<>();
        for ( PagamentoEntity presente : lista ) {
            retorno.add(new PagamentoDTO(presente));
        }
        return retorno;
    }
}
