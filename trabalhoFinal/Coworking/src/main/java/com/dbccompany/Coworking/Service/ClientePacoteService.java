package com.dbccompany.Coworking.Service;

import com.dbccompany.Coworking.DTO.ClientePacoteDTO;
import com.dbccompany.Coworking.Entity.*;
import com.dbccompany.Coworking.Exception.ClientePacote.ArgumentosInvalidosClientePacote;
import com.dbccompany.Coworking.Exception.ClientePacote.ClientePacoteNaoEncontrado;
import com.dbccompany.Coworking.Exception.ClientePacote.NaoExistemClientesPacote;
import com.dbccompany.Coworking.Repository.ClientePacoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

public class ClientePacoteService {

    @Autowired
    private ClientePacoteRepository repository;

    @Transactional
    public ClientePacoteDTO salvar (ClientePacoteEntity clientePacote) throws ArgumentosInvalidosClientePacote {
        try {
            return new ClientePacoteDTO(this.salvarEEditar(clientePacote));
        } catch (Exception e) {
            throw new ArgumentosInvalidosClientePacote();
        }
    }

    @Transactional
    public ClientePacoteDTO editar (ClientePacoteEntity clientePacote, Integer id) throws ArgumentosInvalidosClientePacote {
        try {
            clientePacote.setId(id);
            return new ClientePacoteDTO(this.salvarEEditar(clientePacote));
        } catch (Exception e) {
            throw new ArgumentosInvalidosClientePacote();
        }
    }

    public List<ClientePacoteDTO> buscarTodasContratacoes () throws NaoExistemClientesPacote {
        try {
            return this.transformarList((List<ClientePacoteEntity>) repository.findAll());
        } catch (Exception e) {
            throw new NaoExistemClientesPacote();
        }
    }

    public ClientePacoteDTO buscarPorId (Integer id) throws ClientePacoteNaoEncontrado {
        try {
            return new ClientePacoteDTO(repository.findById(id).get());
        } catch (Exception e) {
            throw new ClientePacoteNaoEncontrado();
        }
    }

    public List<ClientePacoteDTO> buscarPorPacote (PacoteEntity pacote) throws ClientePacoteNaoEncontrado {
        try {
            return this.transformarList(repository.findAllByPacote(pacote));
        } catch (Exception e) {
            throw new ClientePacoteNaoEncontrado();
        }
    }

    public List<ClientePacoteDTO> buscarPorCliente (ClienteEntity cliente) throws ClientePacoteNaoEncontrado {
        try {
            return this.transformarList(repository.findAllByCliente(cliente));
        } catch (Exception e) {
            throw new ClientePacoteNaoEncontrado();
        }
    }

    private ClientePacoteEntity salvarEEditar (ClientePacoteEntity clientePacote) {
        return repository.save(clientePacote);
    }

    private List<ClientePacoteDTO> transformarList (List<ClientePacoteEntity> lista) {
        List<ClientePacoteDTO> retorno = new ArrayList<>();
        for ( ClientePacoteEntity presente : lista ) {
            retorno.add(new ClientePacoteDTO(presente));
        }
        return retorno;
    }
}
