package com.dbccompany.Coworking.DTO;

import com.dbccompany.Coworking.Entity.EspacoEntity;

public class EspacoDTO {

    private String nome;
    private Integer qtdPessoas;
    private double valor;

    public EspacoDTO(EspacoEntity espaco) {
        this.nome = espaco.getNome();
        this.qtdPessoas = espaco.getQtdPessoas();
        this.valor = espaco.getValor();
    }

    public EspacoDTO() {
    }

    public EspacoEntity converter() {
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome(this.nome);
        espaco.setQtdPessoas(this.qtdPessoas);
        espaco.setValor(this.valor);
        return espaco;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas(Integer qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
