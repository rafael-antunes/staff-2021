package com.dbccompany.Coworking.Exception.Usuario;

public class ArgumentosInvalidosUsuario extends UsuarioException {
    public ArgumentosInvalidosUsuario() {
        super("Faltou campos");
    }
}
