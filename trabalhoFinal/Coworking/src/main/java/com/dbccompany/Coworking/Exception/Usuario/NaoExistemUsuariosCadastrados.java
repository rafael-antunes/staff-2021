package com.dbccompany.Coworking.Exception.Usuario;

public class NaoExistemUsuariosCadastrados extends UsuarioException {
    public NaoExistemUsuariosCadastrados() {
        super("Não existe nenhum usuário cadastrado!");
    }
}
