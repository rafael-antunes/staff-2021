package com.dbccompany.Coworking.Service;

import com.dbccompany.Coworking.DTO.PacoteDTO;
import com.dbccompany.Coworking.Entity.PacoteEntity;
import com.dbccompany.Coworking.Exception.Pacote.ArgumentosInvalidosPacote;
import com.dbccompany.Coworking.Exception.Pacote.NaoExistemPacotes;
import com.dbccompany.Coworking.Exception.Pacote.PacoteNaoEncontrado;
import com.dbccompany.Coworking.Repository.PacoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class PacoteService {

    @Autowired
    private PacoteRepository repository;

    @Transactional
    public PacoteDTO salvar (PacoteEntity pacote) throws ArgumentosInvalidosPacote {
        try {
            return new PacoteDTO(this.salvarEEditar(pacote));
        } catch (Exception e) {
            throw new ArgumentosInvalidosPacote();
        }
    }

    @Transactional
    public PacoteDTO editar (PacoteEntity pacote, Integer id) throws ArgumentosInvalidosPacote {
        try {
            pacote.setId(id);
            return new PacoteDTO(this.salvarEEditar(pacote));
        } catch (Exception e) {
            throw new ArgumentosInvalidosPacote();
        }
    }

    public List<PacoteDTO> buscarTodosPacotes () throws NaoExistemPacotes {
        try {
            return this.transformarList((List<PacoteEntity>) repository.findAll());
        } catch (Exception e) {
            throw new NaoExistemPacotes();
        }
    }

    public PacoteDTO buscarPorId (Integer id) throws PacoteNaoEncontrado {
        try {
            return new PacoteDTO(repository.findById(id).get());
        } catch (Exception e) {
            throw new PacoteNaoEncontrado();
        }
    }

    private PacoteEntity salvarEEditar (PacoteEntity pacote) {
        return repository.save(pacote);
    }

    private List<PacoteDTO> transformarList (List<PacoteEntity> lista) {
        List<PacoteDTO> retorno = new ArrayList<>();
        for ( PacoteEntity presente : lista ) {
            retorno.add(new PacoteDTO(presente));
        }
        return retorno;
    }
}
