package com.dbccompany.Coworking.Controller;

import com.dbccompany.Coworking.DTO.EspacoDTO;
import com.dbccompany.Coworking.Entity.EspacoEntity;
import com.dbccompany.Coworking.Exception.Espaco.ArgumentosInvalidosEspaco;
import com.dbccompany.Coworking.Exception.Espaco.EspacoNaoEncontrado;
import com.dbccompany.Coworking.Exception.Espaco.NaoExistemEspacosCadastrados;
import com.dbccompany.Coworking.Service.EspacoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/api/espaco")
public class EspacoController {

    @Autowired
    private EspacoService service;

    @GetMapping(value = "/")
    @ResponseBody
    public ResponseEntity<List<EspacoDTO>> trazerTodosEspacos () {
        try {
            return new ResponseEntity<>(service.buscarTodosEspacos(), HttpStatus.OK);
        } catch (NaoExistemEspacosCadastrados e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<EspacoDTO> buscarPorId (@PathVariable Integer id) {
        try {
            return new ResponseEntity<>(service.buscarPorId(id), HttpStatus.OK);
        } catch (EspacoNaoEncontrado e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/{nome}")
    @ResponseBody
    public ResponseEntity<EspacoDTO> buscarPorNome (@PathVariable String nome) {
        try {
            return new ResponseEntity<>(service.buscarPorNome(nome), HttpStatus.OK);
        } catch (EspacoNaoEncontrado e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(value = "/salvar")
    @ResponseBody
    public ResponseEntity<EspacoDTO> salvarTipoContato (@RequestBody EspacoDTO espaco) {
        try {
            return new ResponseEntity<>(service.salvar(espaco.converter()), HttpStatus.CREATED);
        } catch (ArgumentosInvalidosEspaco e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<EspacoDTO> editarTipoContato (@RequestBody EspacoDTO espaco, @PathVariable Integer id) {
        try {
            return new ResponseEntity<>(service.editar(espaco.converter(), id),HttpStatus.ACCEPTED);
        } catch (ArgumentosInvalidosEspaco e){
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
