package com.dbccompany.Coworking.Exception.Cliente;

public class ArgumentosInvalidosCliente extends ClienteException {
    public ArgumentosInvalidosCliente() {
        super("Faltou campos");
    }
}
