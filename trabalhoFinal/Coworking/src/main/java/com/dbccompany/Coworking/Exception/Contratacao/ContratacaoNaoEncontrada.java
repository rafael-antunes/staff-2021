package com.dbccompany.Coworking.Exception.Contratacao;

public class ContratacaoNaoEncontrada extends ContratacaoException{
    public ContratacaoNaoEncontrada() {
        super("Contratação não encontrada");
    }
}
