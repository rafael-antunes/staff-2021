package com.dbccompany.Coworking.Exception.Pagamento;

public class PagamentoNaoEncontrado extends PagamentoException{
    public PagamentoNaoEncontrado() {
        super("Pagamento não encontrado");
    }

}
