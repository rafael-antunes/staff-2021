package com.dbccompany.Coworking.Exception.Pagamento;

public class NaoExistemPagamentos extends PagamentoException {
    public NaoExistemPagamentos() {
        super("Não existe nenhum pagamento!");
    }
}
