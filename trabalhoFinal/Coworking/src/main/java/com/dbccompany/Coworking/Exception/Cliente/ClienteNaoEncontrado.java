package com.dbccompany.Coworking.Exception.Cliente;

public class ClienteNaoEncontrado extends  ClienteException{
    public ClienteNaoEncontrado() {
        super("Cliente não encontrado");
    }
}
