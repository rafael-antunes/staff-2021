package com.dbccompany.Coworking.DTO;

import com.dbccompany.Coworking.Entity.AcessoEntity;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

public class AcessoDTO {

    private boolean isEntrada;
    private LocalDateTime data;
    private boolean isExcecao;

    public AcessoDTO(AcessoEntity acesso) {
        this.isEntrada = acesso.isEntrada();
        this.data = acesso.getData();
        this.isExcecao = acesso.isExcecao();
    }

    public AcessoDTO() {
    }

    public AcessoEntity converter() {
        AcessoEntity acesso = new AcessoEntity();
        acesso.setEntrada(this.isEntrada);
        acesso.setData(this.data);
        acesso.setExcecao(this.isExcecao);
        return acesso;
    }

    public boolean isEntrada() {
        return isEntrada;
    }

    public void setEntrada(boolean entrada) {
        isEntrada = entrada;
    }

    public LocalDateTime getData() {
        return data;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }

    public boolean isExcecao() {
        return isExcecao;
    }

    public void setExcecao(boolean excecao) {
        isExcecao = excecao;
    }
}
