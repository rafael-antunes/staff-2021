package com.dbccompany.Coworking.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class TipoContatoEntity {

    @Id
    @SequenceGenerator( name = "TIPO_USUARIO_SEQ", sequenceName = "TIPO_USUARIO_SEQ" )
    @GeneratedValue( generator = "TIPO_USUARIO_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @OneToMany( fetch = FetchType.LAZY, mappedBy = "tipoContato")
    private List<ContatoEntity> contato;

    @Column ( nullable = false, unique = true )
    private String nome;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<ContatoEntity> getContato() {
        return contato;
    }

    public void setContato(List<ContatoEntity> contato) {
        this.contato = contato;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
