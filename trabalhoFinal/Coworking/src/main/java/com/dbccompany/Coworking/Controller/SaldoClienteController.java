package com.dbccompany.Coworking.Controller;

import com.dbccompany.Coworking.DTO.*;
import com.dbccompany.Coworking.Entity.SaldoClienteId;
import com.dbccompany.Coworking.Exception.SaldoCliente.ArgumentosInvalidosSaldoCliente;
import com.dbccompany.Coworking.Exception.SaldoCliente.NaoExistemSaldosCliente;
import com.dbccompany.Coworking.Exception.SaldoCliente.SaldoClienteNaoEncontrado;
import com.dbccompany.Coworking.Service.SaldoClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/api/SaldoCliente")
public class SaldoClienteController {

    @Autowired
    private SaldoClienteService service;

    @GetMapping(value = "/")
    @ResponseBody
    public ResponseEntity<List<SaldoClienteDTO>> buscarTodosClientePacote () {
        try {
            return new ResponseEntity<>(service.buscarTodosSaldosCliente(), HttpStatus.OK);
        } catch (NaoExistemSaldosCliente e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<SaldoClienteDTO> buscarPorId (@PathVariable SaldoClienteId id) {
        try {
            return new ResponseEntity<>(service.buscarPorId(id), HttpStatus.OK);
        } catch (SaldoClienteNaoEncontrado e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(value = "/salvar")
    @ResponseBody
    public ResponseEntity<SaldoClienteDTO> salvarClientePacote (@RequestBody SaldoClienteDTO saldoCliente) {
        try {
            return new ResponseEntity<>(service.salvar(saldoCliente.converter()), HttpStatus.CREATED);
        } catch (ArgumentosInvalidosSaldoCliente e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(value = "/editar/")
    @ResponseBody
    public ResponseEntity<SaldoClienteDTO> editarClientePacote (@RequestBody SaldoClienteDTO saldoCliente) {
        try {
            return new ResponseEntity<>(service.editar(saldoCliente.converter()), HttpStatus.ACCEPTED);
        } catch (ArgumentosInvalidosSaldoCliente e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/busca/porCliente")
    @ResponseBody
    public ResponseEntity<List<SaldoClienteDTO>> buscarTodosClientePacotePorCliente (@RequestBody ClienteDTO cliente) {
        try {
            return new ResponseEntity<>(service.buscarPorCliente(cliente.converter()), HttpStatus.OK);
        } catch (SaldoClienteNaoEncontrado e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/busca/porEspaco")
    @ResponseBody
    public ResponseEntity<List<SaldoClienteDTO>> buscarTodosClientePacotePorPacote (@RequestBody EspacoDTO espaco) {
        try {
            return new ResponseEntity<>(service.buscarPorEspaco(espaco.converter()), HttpStatus.OK);
        } catch (SaldoClienteNaoEncontrado e) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
