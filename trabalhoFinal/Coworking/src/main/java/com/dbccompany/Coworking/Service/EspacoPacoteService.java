package com.dbccompany.Coworking.Service;

import com.dbccompany.Coworking.DTO.EspacoPacoteDTO;
import com.dbccompany.Coworking.Entity.*;
import com.dbccompany.Coworking.Exception.EspacoPacote.ArgumentosInvalidosEspacoPacote;
import com.dbccompany.Coworking.Exception.EspacoPacote.EspacoPacoteNaoEncontrado;
import com.dbccompany.Coworking.Exception.EspacoPacote.NaoExistemEspacosPacote;
import com.dbccompany.Coworking.Exception.Pacote.ArgumentosInvalidosPacote;
import com.dbccompany.Coworking.Repository.EspacoPacoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class EspacoPacoteService {

    @Autowired
    private EspacoPacoteRepository repository;

    @Transactional
    public EspacoPacoteDTO salvar (EspacoPacoteEntity espacoPacote) throws ArgumentosInvalidosEspacoPacote {
        try {
            return new EspacoPacoteDTO(this.salvarEEditar(espacoPacote));
        } catch (Exception e) {
            throw new ArgumentosInvalidosEspacoPacote();
        }
    }

    @Transactional
    public EspacoPacoteDTO editar (EspacoPacoteEntity espacoPacote, Integer id) throws ArgumentosInvalidosEspacoPacote {
        try {
            espacoPacote.setId(id);
            return new EspacoPacoteDTO(this.salvarEEditar(espacoPacote));
        } catch (Exception e) {
            throw new ArgumentosInvalidosEspacoPacote();
        }
    }

    public List<EspacoPacoteDTO> buscarTodosEspacosPacote () throws NaoExistemEspacosPacote {
        try {
            return this.transformarList((List<EspacoPacoteEntity>) repository.findAll());
        } catch (Exception e) {
            throw new NaoExistemEspacosPacote();
        }
    }

    public EspacoPacoteDTO buscarPorId (Integer id) throws EspacoPacoteNaoEncontrado {
        try {
            return new EspacoPacoteDTO(repository.findById(id).get());
        } catch (Exception e) {
            throw new EspacoPacoteNaoEncontrado();
        }
    }

    public List<EspacoPacoteDTO> buscarPorEspaco (EspacoEntity espaco) throws EspacoPacoteNaoEncontrado {
        try {
            return this.transformarList(repository.findAllByEspaco(espaco));
        } catch (Exception e) {
            throw new EspacoPacoteNaoEncontrado();
        }
    }

    public List<EspacoPacoteDTO> buscarPorPacote (PacoteEntity pacote) throws EspacoPacoteNaoEncontrado {
        try {
            return this.transformarList(repository.findAllByPacote(pacote));
        } catch (Exception e) {
            throw new EspacoPacoteNaoEncontrado();
        }
    }

    private EspacoPacoteEntity salvarEEditar (EspacoPacoteEntity contratacao) {
        return repository.save(contratacao);
    }

    private List<EspacoPacoteDTO> transformarList (List<EspacoPacoteEntity> lista) {
        List<EspacoPacoteDTO> retorno = new ArrayList<>();
        for ( EspacoPacoteEntity presente : lista ) {
            retorno.add(new EspacoPacoteDTO(presente));
        }
        return retorno;
    }
}
