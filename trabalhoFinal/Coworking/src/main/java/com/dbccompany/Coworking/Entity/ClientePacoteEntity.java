package com.dbccompany.Coworking.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
public class ClientePacoteEntity {

    @Id
    @SequenceGenerator( name = "CLIENTE_PACOTE_SEQ", sequenceName = "CLIENTE_PACOTE_SEQ")
    @GeneratedValue( generator = "CLIENTE_PACOTE_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @JsonIgnore
    @ManyToOne( cascade = CascadeType.ALL)
    private PacoteEntity pacote;

    @JsonIgnore
    @ManyToOne
    private ClienteEntity cliente;

    @Column
    private Integer quantidade;

    @OneToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "id_pagamento")
    private PagamentoEntity pagamento;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public PacoteEntity getPacote() {
        return pacote;
    }

    public void setPacote(PacoteEntity pacote) {
        this.pacote = pacote;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public PagamentoEntity getPagamento() {
        return pagamento;
    }

    public void setPagamento(PagamentoEntity pagamento) {
        this.pagamento = pagamento;
    }
}
