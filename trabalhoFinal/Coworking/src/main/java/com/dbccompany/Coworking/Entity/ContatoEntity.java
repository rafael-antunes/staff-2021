package com.dbccompany.Coworking.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Optional;

@Entity
public class ContatoEntity {

    @Id
    @SequenceGenerator( name = "CONTATO_SEQ", sequenceName = "CONTATO_SEQ" )
    @GeneratedValue( generator = "CONTATO_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @JsonIgnore
    @ManyToOne ( cascade = CascadeType.ALL )
    private TipoContatoEntity tipoContato;

    @JsonIgnore
    @ManyToOne
    private ClienteEntity cliente;

    @Column (nullable = false)
    private String valor;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoContatoEntity getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(TipoContatoEntity tipoContato) {
        this.tipoContato = tipoContato;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
}
