package com.dbccompany.Coworking.Exception.Contratacao;

public class NaoExistemContratacoes extends ContratacaoException{
    public NaoExistemContratacoes() {
        super("Não existe nenhuma contratação no sistema!");
    }
}
