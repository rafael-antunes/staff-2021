package com.dbccompany.Coworking.Exception.Acesso;

public class AcessoException extends Exception{
    String mensagem;

    public AcessoException(String mensagem) {
        this.mensagem = mensagem;
    }

    public String getMensagem() {
        return mensagem;
    }
}
