package com.dbccompany.Coworking.Exception.Pagamento;

public class PagamentoException extends Exception{
    String mensagem;

    public PagamentoException(String mensagem) {
        this.mensagem = mensagem;
    }

    public String getMensagem() {
        return mensagem;
    }
}
