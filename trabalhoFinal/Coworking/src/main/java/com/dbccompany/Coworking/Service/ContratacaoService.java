package com.dbccompany.Coworking.Service;

import com.dbccompany.Coworking.Conversor;
import com.dbccompany.Coworking.DTO.ContratacaoDTO;
import com.dbccompany.Coworking.Entity.ClienteEntity;
import com.dbccompany.Coworking.Entity.ContratacaoEntity;
import com.dbccompany.Coworking.Entity.EspacoEntity;
import com.dbccompany.Coworking.Exception.Contratacao.ArgumentosInvalidosContratacao;
import com.dbccompany.Coworking.Exception.Contratacao.ContratacaoNaoEncontrada;
import com.dbccompany.Coworking.Exception.Contratacao.NaoExistemContratacoes;
import com.dbccompany.Coworking.Repository.ContratacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class ContratacaoService {

    @Autowired
    private ContratacaoRepository repository;

    @Transactional
    public ContratacaoDTO salvar (ContratacaoEntity contratacao) throws ArgumentosInvalidosContratacao {
        try {
            return new ContratacaoDTO(this.salvarEEditar(contratacao));
        } catch (Exception e) {
            throw new ArgumentosInvalidosContratacao();
        }
    }

    @Transactional
    public ContratacaoDTO editar (ContratacaoEntity contratacao, Integer id) throws ArgumentosInvalidosContratacao {
        try {
            contratacao.setId(id);
            return new ContratacaoDTO(this.salvarEEditar(contratacao));
        } catch (Exception e) {
            throw new ArgumentosInvalidosContratacao();
        }
    }

    public List<ContratacaoDTO> buscarTodasContratacoes () throws NaoExistemContratacoes {
        try {
            return this.transformarList((List<ContratacaoEntity>) repository.findAll());
        } catch (Exception e) {
            throw new NaoExistemContratacoes();
        }
    }

    public ContratacaoDTO buscarPorId (Integer id) throws ContratacaoNaoEncontrada {
        try {
            return calcularValor(new ContratacaoDTO(repository.findById(id).get()));
        } catch (Exception e) {
            throw new ContratacaoNaoEncontrada();
        }
    }

    public List<ContratacaoDTO> buscarPorEspaco (EspacoEntity espaco) throws ContratacaoNaoEncontrada {
        try {
            return this.transformarList(repository.findAllByEspaco(espaco));
        } catch (Exception e) {
            throw new ContratacaoNaoEncontrada();
        }
    }

    public List<ContratacaoDTO> buscarPorCliente (ClienteEntity cliente) throws ContratacaoNaoEncontrada {
        try {
            return this.transformarList(repository.findAllByCliente(cliente));
        } catch (Exception e) {
            throw new ContratacaoNaoEncontrada();
        }
    }

    private ContratacaoEntity salvarEEditar (ContratacaoEntity contratacao) {
        return repository.save(contratacao);
    }

    private List<ContratacaoDTO> transformarList (List<ContratacaoEntity> lista) {
        List<ContratacaoDTO> retorno = new ArrayList<>();
        for ( ContratacaoEntity presente : lista ) {
            ContratacaoDTO contratacao = new ContratacaoDTO(presente);
            calcularValor(contratacao);
            retorno.add(contratacao);
        }
        return retorno;
    }

    private ContratacaoDTO calcularValor (ContratacaoDTO contratacao) {
        StringBuilder stringBuilder = new StringBuilder();
        Conversor conversor = new Conversor();
        stringBuilder.append("R$" + String.format("%.2f", (contratacao.getQuantidade()*conversor.converterTempo(contratacao.getTipoContratacao()))*contratacao.getEspaco().getValor() ) );
        contratacao.setValor(stringBuilder.toString());
        return contratacao;
    }
}
