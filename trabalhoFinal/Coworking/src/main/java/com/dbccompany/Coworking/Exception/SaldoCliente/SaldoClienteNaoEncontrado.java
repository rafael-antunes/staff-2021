package com.dbccompany.Coworking.Exception.SaldoCliente;

public class SaldoClienteNaoEncontrado extends SaldoClienteException{
    public SaldoClienteNaoEncontrado() {
        super("Saldo não encontrado");
    }
}
