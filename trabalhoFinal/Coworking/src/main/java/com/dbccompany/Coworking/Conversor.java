package com.dbccompany.Coworking;

import com.dbccompany.Coworking.Entity.TipoContratacaoEnum;

public class Conversor {

    public Conversor() {
    }

    public Integer converterTempo (TipoContratacaoEnum tipoContratacao) {
        Integer retorno = 1;
        switch (tipoContratacao) {
            case MINUTOS:
                break;
            case HORAS:
                retorno = 60;
                break;
            case TURNOS:
                retorno = 300;
                break;
            case DIARIAS:
                retorno = 600;
                break;
            case SEMANAS:
                retorno = 4200;
                break;
            case MESES:
                retorno = 18000;
        }
        return retorno;
    }

    public Long paraMilisegundos (TipoContratacaoEnum tipoContratacao) {
        Long retorno = 60000L;
        switch (tipoContratacao) {
            case MINUTOS:
                break;
            case HORAS:
                retorno = 3600000L;
                break;
            case TURNOS:
                retorno = 18000000L;
                break;
            case DIARIAS:
                retorno = 36000000L;
                break;
            case SEMANAS:
                retorno = 252000000L;
                break;
            case MESES:
                retorno = 1080000000L;
        }
        return retorno;
    }
}
