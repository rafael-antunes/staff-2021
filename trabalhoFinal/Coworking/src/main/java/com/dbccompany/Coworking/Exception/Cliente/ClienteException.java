package com.dbccompany.Coworking.Exception.Cliente;

public class ClienteException extends Exception{
    String mensagem;

    public ClienteException(String mensagem) {
        this.mensagem = mensagem;
    }

    public String getMensagem() {
        return mensagem;
    }
}
