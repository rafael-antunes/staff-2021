package com.dbccompany.Coworking.Entity;

public enum TipoContratacaoEnum {
    MINUTOS, HORAS, TURNOS, DIARIAS, SEMANAS, MESES
}
