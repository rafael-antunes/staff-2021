package com.dbccompany.Coworking.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
public class ContratacaoEntity {

    @Id
    @SequenceGenerator( name = "CONTRATACAO_SEQ", sequenceName = "CONTRATACAO_SEQ" )
    @GeneratedValue( generator = "CONTRATACAO_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @JsonIgnore
    @ManyToOne ( cascade = CascadeType.ALL)
    private EspacoEntity espaco;

    @JsonIgnore
    @ManyToOne
    private ClienteEntity cliente;

    @Enumerated ( EnumType.STRING )
    private TipoContratacaoEnum tipoContratacao;

    @OneToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "id_pagamento")
    private PagamentoEntity pagamento;

    @Column (nullable = false)
    private Integer quantidade;

    @Column
    private double desconto;

    @Column (nullable = false)
    private Integer prazo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public EspacoEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoEntity espaco) {
        this.espaco = espaco;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public PagamentoEntity getPagamento() {
        return pagamento;
    }

    public void setPagamento(PagamentoEntity pagamento) {
        this.pagamento = pagamento;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public double getDesconto() {
        return desconto;
    }

    public void setDesconto(double desconto) {
        this.desconto = desconto;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }
}
