package com.dbccompany.Coworking.Exception.TipoContato;

public class TipoContatoNaoEncontrado extends TipoContatoException {
    public TipoContatoNaoEncontrado() {
        super("Tipo de contato não encontrado");
    }
}
