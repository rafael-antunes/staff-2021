package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.ElfoEntity;

public class ElfoDTO extends PersonagemDTO {

    public ElfoDTO( ElfoEntity elfo ) {
        super(elfo);
    }

    public ElfoDTO() {
        super(new ElfoEntity(""));
    }
}
