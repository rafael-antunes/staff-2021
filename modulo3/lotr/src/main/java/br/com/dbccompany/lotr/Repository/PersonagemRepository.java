package br.com.dbccompany.lotr.Repository;

import br.com.dbccompany.lotr.Entity.*;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PersonagemRepository<T extends PersonagemEntity> extends CrudRepository< T, Integer > {
    //T save (T personagem );
    Optional<T> findById(Integer id );
    List<T> findAllById (Integer id);
    List<T> findAllByIdIn(List<Integer> id);
    T findByInventario(InventarioEntity inventario );
    List<T> findAllByInventarioIn(List<InventarioEntity> inventario);
    T findByNome (String nome );
    List<T> findAllByNome (String nome );
    List<T> findAllByNomeIn (List<String> nome );
    T findByVida ( Double vida );
    List<T> findAllByVida ( Double vida );
    List<T> findAllByVidaIn ( List<Double> vida );
    T findByStatus ( StatusEnum status );
    List<T> findAllByStatus ( StatusEnum status );
    List<T> findAllByStatusIn ( List<StatusEnum> status );
    T findByQtdDano ( Double qtdDano );
    List<T> findAllByQtdDano ( Double qtdDano );
    List<T> findAllByQtdDanoIn ( List<Double> qtdDano );
    T findByExperiencia ( Integer experiencia );
    List<T> findAllByExperiencia ( Integer experiencia );
    List<T> findAllByExperienciaIn ( List<Integer> experiencia );
    T findByQtdExperienciaPorAtaque ( Integer qtdExperienciaPorAtaque );
    List<T> findAllByQtdExperienciaPorAtaque ( Integer qtdExperienciaPorAtaque );
    List<T> findAllByQtdExperienciaPorAtaqueIn ( List<Integer> qtdExperienciaPorAtaque );
}
