package br.com.dbccompany.lotr.Exception;

public class AnaoNaoEncontrado extends AnaoException {
    public AnaoNaoEncontrado() {
        super("Este anão não existe!");
    }
}
