package br.com.dbccompany.lotr.Security;

import br.com.dbccompany.lotr.Entity.UsuarioEntity;
import br.com.dbccompany.lotr.Repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AutenticacaoService implements UserDetailsService {

    @Autowired
    private UsuarioRepository repository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<UsuarioEntity> usuario = repository.findByLogin(username);

        if( usuario.isPresent() ) { return usuario.get(); }

        throw new UsernameNotFoundException("Usuario não existe!");
    }
}
