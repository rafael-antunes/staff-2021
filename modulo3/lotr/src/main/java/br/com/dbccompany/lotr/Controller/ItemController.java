package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.ItemDTO;
import br.com.dbccompany.lotr.Entity.Inventario_X_Item;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import br.com.dbccompany.lotr.Exception.ArgumentosInvalidosItem;
import br.com.dbccompany.lotr.Exception.ItemNaoEncontrado;
import br.com.dbccompany.lotr.LotrApplication;
import br.com.dbccompany.lotr.Service.ItemService;
import org.hibernate.cache.spi.support.AbstractReadWriteAccess;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/item" )
public class ItemController {

    @Autowired
    private ItemService service;

    private Logger logger = LoggerFactory.getLogger(LotrApplication.class);

    @GetMapping( value = "/" )
    @ResponseBody
    public List<ItemDTO> trazerTodosItens(){
        return service.trazerTodosOsItens();
    }

    @GetMapping( value = "/{id}")
    @ResponseBody
    public ResponseEntity<Object> trazerItemEspecifico (@PathVariable Integer id ) throws ItemNaoEncontrado {
        try {
            return new ResponseEntity<>(service.buscarPorId(id), HttpStatus.ACCEPTED);
        } catch ( ItemNaoEncontrado e ) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(e.getMensagem(), HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping( value = "/salvar")
    @ResponseBody
    public ResponseEntity<ItemDTO> salvarItem(@RequestBody ItemDTO item ) throws ArgumentosInvalidosItem {
        try {
            return new ResponseEntity<>(service.salvar( item.converter()), HttpStatus.ACCEPTED);
        } catch ( ArgumentosInvalidosItem e ) {
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public ItemDTO editarItem(@RequestBody ItemEntity item, @PathVariable Integer id ) {
        return service.editar(item, id);
    }

    @GetMapping( value = "/buscar/todos/porId/{id}" )
    @ResponseBody
    public List<ItemDTO> trazerTodosPorId( @PathVariable Integer id ) {
        return service.buscarTodosPorId(id);
    }

    @GetMapping( value = "/buscar/todosIn/porId/" )
    @ResponseBody
    public List<ItemDTO> trazerTodosPorIdIn( @RequestBody List<Integer> id ) {
        return service.buscarTodosPorIdIn(id);
    }

    @GetMapping( value = "/buscar/porDescricao/{descricao}" )
    @ResponseBody
    public ItemDTO trazerPorDescricao( @PathVariable String descricao ) {
        return service.buscarPorDescricao(descricao);
    }

    @GetMapping( value = "/buscar/todos/porDescricao/{descricao}" )
    @ResponseBody
    public List<ItemDTO> trazerTodosPorDescricao( @PathVariable String descricao ) {
        return service.buscarTodosPorDescricao(descricao);
    }

    @GetMapping( value = "/buscar/todosIn/porDescricao/" )
    @ResponseBody
    public List<ItemDTO> trazerTodosPorDescricaoIn( @RequestBody List<String> descricao ) {
        return service.buscarTodosPorDescricaoIn(descricao);
    }

    @GetMapping( value = "/buscar/porInventario/" )
    @ResponseBody
    public ItemDTO trazerPorInventario( @RequestBody Inventario_X_Item inventario ) {
        return service.buscarPorInventarioItem(inventario);
    }

    @GetMapping( value = "/buscar/todos/porInventario/" )
    @ResponseBody
    public List<ItemDTO> trazerTodosPorInventario( @RequestBody Inventario_X_Item inventario ) {
        return service.buscarTodosInventarioItem(inventario);
    }

    @GetMapping( value = "/buscar/todosIn/porInventario/" )
    @ResponseBody
    public List<ItemDTO> trazerTodosPorInventario( @RequestBody List<Inventario_X_Item> inventario ) {
        return service.buscarTodosPorInventarioItemIn(inventario);
    }
}
