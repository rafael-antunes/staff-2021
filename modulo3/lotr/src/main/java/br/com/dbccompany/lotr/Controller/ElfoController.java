package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.ElfoDTO;
import br.com.dbccompany.lotr.Entity.ElfoEntity;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.StatusEnum;
import br.com.dbccompany.lotr.Exception.ArgumentosInvalidosElfo;
import br.com.dbccompany.lotr.Exception.ElfoNaoEncontrado;
import br.com.dbccompany.lotr.Service.ElfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/elfo " )
public class ElfoController {

    @Autowired
    private ElfoService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<ElfoDTO> trazerTodosElfos(){
        return service.trazerTodosOsElfos();
    }

    @GetMapping( value = "/{id}")
    @ResponseBody
    public ElfoDTO trazerElfoEspecifico(@PathVariable Integer id ) throws ElfoNaoEncontrado {
        try {
            return service.buscarPorId(id);
        } catch ( Exception e ) {
           throw new ElfoNaoEncontrado();
        }
    }

    @PostMapping( value = "/salvar")
    @ResponseBody
    public ElfoDTO salvarElfo(@RequestBody ElfoDTO elfo ) throws ArgumentosInvalidosElfo {
        try {
            return service.salvar((ElfoEntity) elfo.converter(new ElfoEntity(elfo.getNome())));
        } catch ( Exception e ) {
            throw new ArgumentosInvalidosElfo();
        }
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public ElfoDTO editarElfo(@RequestBody ElfoEntity elfo, @PathVariable Integer id ) {
        return service.editar(elfo, id);
    }

    @GetMapping( value = "/buscar/todos/porId/{id}" )
    @ResponseBody
    public List<ElfoDTO> trazerTodosPorId( @PathVariable Integer id ) {
        return service.buscarTodosPorId(id);
    }

    @GetMapping( value = "/buscar/todosIn/porId/{id}" )
    @ResponseBody
    public List<ElfoDTO> trazerTodosPorIdIn( @RequestBody List<Integer> id ) {
        return service.buscarTodosPorIdIn(id);
    }

    @GetMapping( value = "/buscar/porInventario/{inventario}" )
    @ResponseBody
    public ElfoDTO trazerPorInventario( @PathVariable InventarioEntity inventario ) {
        return service.buscarPorInventario(inventario);
    }

    @GetMapping( value = "/buscar/todosIn/porInventario/" )
    @ResponseBody
    public List<ElfoDTO> trazerTodosPorInventario( @RequestBody List<InventarioEntity> inventario ) {
        return service.buscarTodosPorInventarioIn(inventario);
    }

    @GetMapping( value = "/buscar/porNome/{nome}" )
    @ResponseBody
    public ElfoDTO trazerPorNome( @PathVariable String nome ) {
        return service.buscarPorNome(nome);
    }

    @GetMapping( value = "/buscar/todos/porNome/{nome}" )
    @ResponseBody
    public List<ElfoDTO> trazerTodosPorNome( @PathVariable String nome ) {
        return service.buscarTodosPorNome(nome);
    }

    @GetMapping( value = "/buscar/todosIn/porNome/" )
    @ResponseBody
    public List<ElfoDTO> trazerTodosPorNomeIn ( @RequestBody List<String> nome ) {
        return service.buscarTodosPorNomeIn(nome);
    }

    @GetMapping( value = "/buscar/porVida/{vida}" )
    @ResponseBody
    public ElfoDTO trazerPorVida( @PathVariable Double vida ) {
        return service.buscarPorVida(vida);
    }

    @GetMapping( value = "/buscar/todos/porVida/{vida}" )
    @ResponseBody
    public List<ElfoDTO> trazerTodosPorVida( @PathVariable Double vida ) {
        return service.buscarTodosPorVida(vida);
    }

    @GetMapping( value = "/buscar/todosIn/porVida/" )
    @ResponseBody
    public List<ElfoDTO> trazerTodosPorVidaIn( @RequestBody List<Double> vida ) {
        return service.buscarTodosPorVidaIn(vida);
    }

    @GetMapping( value = "/buscar/porStatus/{status}" )
    @ResponseBody
    public ElfoDTO trazerPorStatus( @PathVariable StatusEnum status ) {
        return service.buscarPorStatus(status);
    }

    @GetMapping( value = "/buscar/todos/porStatus/{status}" )
    @ResponseBody
    public List<ElfoDTO> trazerTodosPorStatus( @PathVariable StatusEnum status ) {
        return service.buscarTodosPorStatus(status);
    }

    @GetMapping( value = "/buscar/todosIn/porStatus/" )
    @ResponseBody
    public List<ElfoDTO> trazerTodosPorStatusIn( @RequestBody List<StatusEnum> status ) {
        return service.buscarTodosPorStatusIn(status);
    }

    @GetMapping( value = "/buscar/porQtdDano/{qtdDano}" )
    @ResponseBody
    public ElfoDTO trazerPorQtdDano( @PathVariable Double qtdDano ) {
        return service.buscarPorQtdDano(qtdDano);
    }

    @GetMapping( value = "/buscar/todos/porQtdDano/{qtdDano}" )
    @ResponseBody
    public List<ElfoDTO> trazerTodosPorQtdDano( @PathVariable Double qtdDano ) {
        return service.buscarTodosPorQtdDano(qtdDano);
    }

    @GetMapping( value = "/buscar/todosIn/porQtdDano/" )
    @ResponseBody
    public List<ElfoDTO> trazerTodosPorQtdDanoIn( @RequestBody List<Double> qtdDano ) {
        return service.buscarTodosPorQtdDanoIn(qtdDano);
    }

    @GetMapping( value = "/buscar/porExperiencia/{experiencia}" )
    @ResponseBody
    public ElfoDTO trazerPorExperiencia( @PathVariable Integer experiencia ) {
        return service.buscarPorExperiencia(experiencia);
    }

    @GetMapping( value = "/buscar/todos/porExperiencia/{experiencia}" )
    @ResponseBody
    public List<ElfoDTO> trazerTodosPorExperiencia( @PathVariable Integer experiencia ) {
        return service.buscarTodosPorExperiencia(experiencia);
    }

    @GetMapping( value = "/buscar/todosIn/porExperiencia/" )
    @ResponseBody
    public List<ElfoDTO> trazerTodosPorExperienciaIn( @RequestBody List<Integer> experiencia ) {
        return service.buscarTodosPorExperienciaIn(experiencia);
    }

    @GetMapping( value = "/buscar/porQtdExperienciaPorAtaque/{qtdExperienciaPorAtaque}" )
    @ResponseBody
    public ElfoDTO trazerPorQtdExperienciaPorAtaque( @PathVariable Integer qtdExperienciaPorAtaque ) {
        return service.buscarPorQtdExperienciaPorAtaque(qtdExperienciaPorAtaque);
    }

    @GetMapping( value = "/buscar/todos/porQtdExperienciaPorAtaque/{qtdExperienciaPorAtaque}" )
    @ResponseBody
    public List<ElfoDTO> trazerTodosPorQtdExperienciaPorAtaque( @PathVariable Integer qtdExperienciaPorAtaque ) {
        return service.buscarTodosPorQtdExperienciaPorAtaque(qtdExperienciaPorAtaque);
    }

    @GetMapping( value = "/buscar/todosIn/porQtdExperienciaPorAtaque/" )
    @ResponseBody
    public List<ElfoDTO> trazerTodosPorQtdExperienciaPorAtaque( @RequestBody List<Integer> qtdExperienciaPorAtaque ) {
        return service.buscarTodosPorQtdExperienciaPorAtaqueIn(qtdExperienciaPorAtaque);
    }

}
