package br.com.dbccompany.lotr.Repository;

import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.Inventario_X_Item;
import br.com.dbccompany.lotr.Entity.Inventario_X_ItemId;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface Inventario_X_ItemRepository extends CrudRepository<Inventario_X_Item, Inventario_X_ItemId> {
    Inventario_X_Item save (Inventario_X_Item inventario_x_item );
    Optional<Inventario_X_Item> findById (Inventario_X_ItemId id);
    //<Inventario_X_ItemEntity> Inventario_X_ItemEntity findById(Inventario_X_ItemId id );
    List<Inventario_X_Item> findAllById (Inventario_X_ItemId id);
    List<Inventario_X_Item> findAllByIdIn (List<Inventario_X_ItemId> ids);
    Inventario_X_Item findByQuantidade(Integer quantidade );
    List<Inventario_X_Item> findAllByQuantidade(Integer quantidade );
    List<Inventario_X_Item> findAllByQuantidadeIn(Iterable<Integer> quantidade );
    Inventario_X_Item findByInventario(InventarioEntity inventario);
    List<Inventario_X_Item> findAllByInventario(InventarioEntity inventario );
    List<Inventario_X_Item> findAllByInventarioIn(List<InventarioEntity> inventario);
    Inventario_X_Item findByItem(ItemEntity item);
    List<Inventario_X_Item> findAllByItem(ItemEntity item);
    List<Inventario_X_Item> findAllByItemIn(List<ItemEntity> item);
}
