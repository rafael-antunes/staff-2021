package br.com.dbccompany.lotr.Exception;

public class AnaoException extends Exception {
    private String mensagem;

    public AnaoException ( String mensagem ) {
        this.mensagem = mensagem;
    }

    public String getMensagem() {
        return mensagem;
    }
}
