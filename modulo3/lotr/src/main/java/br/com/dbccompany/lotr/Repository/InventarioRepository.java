package br.com.dbccompany.lotr.Repository;

import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.Inventario_X_Item;
import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InventarioRepository extends CrudRepository<InventarioEntity, Integer> {
    InventarioEntity save (InventarioEntity inventario );
    //<InventarioEntity> InventarioEntity findById(Integer id );
    List<InventarioEntity> findAllById ( Integer id);
    List<InventarioEntity> findAllByIdIn (List<Integer> id);
    InventarioEntity findByInventarioItem (Inventario_X_Item inventario_x_item);
    List<InventarioEntity> findAllByInventarioItem (Inventario_X_Item inventario_x_item);
    List<InventarioEntity> findAllByInventarioItemIn (List<Inventario_X_Item> inventario_x_item) ;
    InventarioEntity findByPersonagem (PersonagemEntity personagem);
    List<InventarioEntity> findAllByPersonagemIn (List<PersonagemEntity> personagem);
}
