package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import br.com.dbccompany.lotr.Entity.StatusEnum;

public abstract class PersonagemDTO <E extends PersonagemEntity, D extends PersonagemDTO> {
    protected String nome;
    protected Double vida;
    protected StatusEnum status;
    protected Double qtdDano = 0.0;
    protected Integer experiencia = 0;
    protected Integer qtdExperienciaPorAtaque = 1;

    public PersonagemDTO (E e ) {
        this.nome = e.getNome();
        this.vida = e.getVida();
        this.status = e.getStatus();
    }

    public PersonagemDTO() {
    }

    public E converter (E e ) {
        e.setNome(this.nome);
        e.setVida(this.vida);
        e.setStatus(this.status);
        e.setQtdDano(this.qtdDano);
        e.setExperiencia(this.experiencia);
        e.setQtdExperienciaPorAtaque(this.qtdExperienciaPorAtaque);
        return e;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getVida() {
        return vida;
    }

    public void setVida(Double vida) {
        this.vida = vida;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    public Double getQtdDano() {
        return qtdDano;
    }

    public void setQtdDano(Double qtdDano) {
        this.qtdDano = qtdDano;
    }

    public Integer getExperiencia() {
        return experiencia;
    }

    public void setExperiencia(Integer experiencia) {
        this.experiencia = experiencia;
    }

    public Integer getQtdExperienciaPorAtaque() {
        return qtdExperienciaPorAtaque;
    }

    public void setQtdExperienciaPorAtaque(Integer qtdExperienciaPorAtaque) {
        this.qtdExperienciaPorAtaque = qtdExperienciaPorAtaque;
    }

}
