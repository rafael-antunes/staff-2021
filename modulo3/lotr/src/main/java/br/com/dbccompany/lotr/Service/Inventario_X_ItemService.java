package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.Inventario_X_ItemDTO;
import br.com.dbccompany.lotr.Entity.*;
import br.com.dbccompany.lotr.Exception.ArgumentosInvalidosInventario_X_Item;
import br.com.dbccompany.lotr.Repository.Inventario_X_ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class Inventario_X_ItemService {

    @Autowired
    private Inventario_X_ItemRepository repository;

    public List<Inventario_X_ItemDTO> trazerTodosOsInventarioItem() {
        List<Inventario_X_Item> inventarioItem = (List<Inventario_X_Item>) repository.findAll();
        if ( inventarioItem != null ) {
            return this.transformarList(inventarioItem);
        }
        return null;
    }

    private List<Inventario_X_ItemDTO> transformarList (List<Inventario_X_Item> inventarioItem ) {
        List<Inventario_X_ItemDTO> retorno = new ArrayList<>();
        for ( Inventario_X_Item presente : inventarioItem ) {
            retorno.add(new Inventario_X_ItemDTO(presente));
        }
        return retorno;
    }

    @Transactional( rollbackFor = Exception.class )
    public Inventario_X_ItemDTO salvar(Inventario_X_Item inventario ) {
        Inventario_X_Item inventarioItemNovo = this.salvarEEditar(inventario);
        return new Inventario_X_ItemDTO(inventario);
    }

    @Transactional( rollbackFor = Exception.class )
    public Inventario_X_ItemDTO editar(Inventario_X_Item inventario, Inventario_X_ItemId id ) {
        inventario.setId(id);
        Inventario_X_Item inventarioItemNovo = this.salvarEEditar(inventario);
        return new Inventario_X_ItemDTO(inventario);
    }

    private Inventario_X_Item salvarEEditar (Inventario_X_Item inventario ) {
        return repository.save(inventario);
    }

    public Inventario_X_ItemDTO buscarPorId(Inventario_X_ItemId id ) {
        return new Inventario_X_ItemDTO(repository.findById(id).get());
    }

    public List<Inventario_X_ItemDTO> buscarTodosPorId (Inventario_X_ItemId id ) {
        List<Inventario_X_Item> inventarioItem = repository.findAllById(id);
        if ( inventarioItem != null ) {
            return this.transformarList(inventarioItem);
        }
        return null;
    }

    public List<Inventario_X_ItemDTO> buscarTodosPorIdIn ( List<Inventario_X_ItemId> id ) {
        List<Inventario_X_Item> inventarioItem = repository.findAllByIdIn(id);
        if ( inventarioItem != null ) {
            return this.transformarList(inventarioItem);
        }
        return null;
    }

    public Inventario_X_ItemDTO buscarPorQuantidade( Integer quantidade ) {
        Inventario_X_Item inventarioItem = repository.findByQuantidade(quantidade);
        if( inventarioItem != null ) {
            return new Inventario_X_ItemDTO(inventarioItem);
        }
        return null;
    }

    public List<Inventario_X_ItemDTO> buscarTodosPorQuantidade ( Integer quantidade ) {
        List<Inventario_X_Item> inventarioItem = repository.findAllByQuantidade(quantidade);
        if ( inventarioItem != null ) {
            return this.transformarList(inventarioItem);
        }
        return null;
    }

    public List<Inventario_X_ItemDTO> buscarTodosPorQuantidadeIn ( List<Integer> quantidade ) {
        List<Inventario_X_Item> inventarioItem = repository.findAllByQuantidadeIn(quantidade);
        if ( inventarioItem != null ) {
            return this.transformarList(inventarioItem);
        }
        return null;
    }

    public Inventario_X_ItemDTO buscarPorInventario( InventarioEntity inventario ) {
        Inventario_X_Item inventarioItem = repository.findByInventario(inventario);
        if( inventarioItem != null ) {
            return new Inventario_X_ItemDTO(inventarioItem);
        }
        return null;
    }

    public List<Inventario_X_ItemDTO> buscarTodosPorInventario ( InventarioEntity inventario ) {
        List<Inventario_X_Item> inventarioItem = repository.findAllByInventario(inventario);
        if ( inventarioItem != null ) {
            return this.transformarList(inventarioItem);
        }
        return null;
    }

    public List<Inventario_X_ItemDTO> buscarTodosPorInventarioIn ( List<InventarioEntity> inventario ) {
        List<Inventario_X_Item> inventarioItem = repository.findAllByInventarioIn(inventario);
        if ( inventarioItem != null ) {
            return this.transformarList(inventarioItem);
        }
        return null;
    }

    public Inventario_X_ItemDTO buscarPorItem( ItemEntity item ) {
        Inventario_X_Item inventarioItem = repository.findByItem(item);
        if( inventarioItem != null ) {
            return new Inventario_X_ItemDTO(inventarioItem);
        }
        return null;
    }

    public List<Inventario_X_ItemDTO> buscarTodosPorItem ( ItemEntity item ) {
        List<Inventario_X_Item> inventarioItem = repository.findAllByItem(item);
        if ( inventarioItem != null ) {
            return this.transformarList(inventarioItem);
        }
        return null;
    }

    public List<Inventario_X_ItemDTO> buscarTodosPorItemIn ( List<ItemEntity> item ) {
        List<Inventario_X_Item> inventarioItem = repository.findAllByItemIn(item);
        if ( inventarioItem != null ) {
            return this.transformarList(inventarioItem);
        }
        return null;
    }
}
