package br.com.dbccompany.lotr.Exception;

public class ElfoNaoEncontrado extends ElfoException {
    public ElfoNaoEncontrado() {
        super("Este elfo não existe!");
    }
}
