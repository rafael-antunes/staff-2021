package br.com.dbccompany.lotr.Repository;

import br.com.dbccompany.lotr.Entity.Inventario_X_Item;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface ItemRepository extends CrudRepository<ItemEntity, Integer> {
    ItemEntity findByDescricao( String descricao );
    List<ItemEntity> findAllByDescricao( String descricao );
    List<ItemEntity> findAllByDescricaoIn( List<String> descricao );
    //<ItemEntity> ItemEntity findById (Integer id );
    List<ItemEntity> findAllById ( Integer id);
    List<ItemEntity> findAllByIdIn (List<Integer> id);
    ItemEntity findByInventarioItem (Inventario_X_Item inventario_x_item);
    List<ItemEntity> findAllByInventarioItem (Inventario_X_Item inventario_x_item);
    List<ItemEntity> findAllByInventarioItemIn (List<Inventario_X_Item> inventario_x_item);
}
