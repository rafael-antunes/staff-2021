package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.InventarioDTO;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.Inventario_X_Item;
import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import br.com.dbccompany.lotr.Exception.ArgumentosInvalidosInventario;
import br.com.dbccompany.lotr.Exception.InventarioNaoEncontrado;
import br.com.dbccompany.lotr.Repository.InventarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class InventarioService {

    @Autowired
    private InventarioRepository repository;

    public List<InventarioDTO> trazerTodosOsInventarios() {
        List<InventarioEntity> inventario = (List<InventarioEntity>) repository.findAll();
        if ( inventario != null ) {
            return this.transformarList(inventario);
        }
        return null;
    }

    private List<InventarioDTO> transformarList (List<InventarioEntity> inventario ) {
        List<InventarioDTO> retorno = new ArrayList<>();
        for ( InventarioEntity presente : inventario ) {
            retorno.add(new InventarioDTO(presente));
        }
        return retorno;
    }

    @Transactional( rollbackFor = Exception.class )
    public InventarioDTO salvar( InventarioEntity inventario ) {
        InventarioEntity inventarioNovo = this.salvarEEditar(inventario);
        return new InventarioDTO(inventarioNovo);
    }


    @Transactional( rollbackFor = Exception.class )
    public InventarioDTO editar( InventarioEntity inventario, Integer id ) {
        inventario.setId(id);
        this.salvarEEditar(inventario);
        return new InventarioDTO(inventario);
    }

    private InventarioEntity salvarEEditar ( InventarioEntity inventario ) {
        return repository.save(inventario);
    }

    public InventarioDTO buscarPorId(Integer id ) {
        return new InventarioDTO(repository.findById(id).get());
    }

    public List<InventarioDTO> buscarTodosPorId (Integer id ) {
        List<InventarioEntity> inventario = repository.findAllById(id);
        if ( inventario != null ) {
            return this.transformarList(inventario);
        }
        return null;
    }

    public List<InventarioDTO> buscarTodosPorIdIn ( List<Integer> id ) {
        List<InventarioEntity> inventario = repository.findAllByIdIn(id);
        if ( inventario != null ) {
            return this.transformarList(inventario);
        }
        return null;
    }

    public InventarioDTO buscarPorInventarioItem( Inventario_X_Item inventario_x_item ) {
        InventarioEntity inventario = repository.findByInventarioItem(inventario_x_item);
        if( inventario != null ) {
            return new InventarioDTO(repository.findByInventarioItem(inventario_x_item));
        }
        return null;
    }

    public List<InventarioDTO> buscarTodosInventarioItem ( Inventario_X_Item inventario_x_item ) {
        List<InventarioEntity> inventario = repository.findAllByInventarioItem(inventario_x_item);
        if ( inventario != null ) {
            return this.transformarList(inventario);
        }
        return null;
    }

    public List<InventarioDTO> buscarTodosPorInventarioItemIn ( List<Inventario_X_Item> inventario_x_item ) {
        List<InventarioEntity> inventario = repository.findAllByInventarioItemIn(inventario_x_item);
        if ( inventario != null ) {
            return this.transformarList(inventario);
        }
        return null;
    }

    public InventarioDTO buscarPorPersonagem (PersonagemEntity personagem ) {
        InventarioEntity inventario = repository.findByPersonagem(personagem);
        if( inventario != null ) {
            return new InventarioDTO(repository.findByPersonagem(personagem));
        }
        return null;
    }

    public List<InventarioDTO> buscarPorTodosPersonagemIn ( List<PersonagemEntity> personagem ) {
        List<InventarioEntity> inventario = repository.findAllByPersonagemIn(personagem);
        if ( inventario != null ) {
            return this.transformarList(inventario);
        }
        return null;
    }
}
