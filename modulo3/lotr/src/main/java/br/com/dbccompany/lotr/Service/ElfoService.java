package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.ElfoDTO;
import br.com.dbccompany.lotr.Entity.ElfoEntity;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.StatusEnum;
import br.com.dbccompany.lotr.Exception.ArgumentosInvalidosElfo;
import br.com.dbccompany.lotr.Exception.ElfoNaoEncontrado;
import br.com.dbccompany.lotr.Repository.PersonagemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class ElfoService {

    @Autowired
    private PersonagemRepository repository;

    public List<ElfoDTO> trazerTodosOsElfos() {
        List<ElfoEntity> elfos = (List<ElfoEntity>) repository.findAll();
        if ( elfos != null ) {
            return this.transformarList(elfos);
        }
        return null;
    }

    private List<ElfoDTO> transformarList ( List<ElfoEntity> elfos ) {
        List<ElfoDTO> retorno = new ArrayList<>();
        for ( ElfoEntity presente : elfos ) {
            retorno.add(new ElfoDTO(presente));
        }
        return retorno;
    }

    @Transactional( rollbackFor = Exception.class )
    public ElfoDTO salvar( ElfoEntity elfo ) {
        ElfoEntity elfoNovo = this.salvarEEditar(elfo);
        return new ElfoDTO(elfoNovo);
    }

    @Transactional( rollbackFor = Exception.class )
    public ElfoDTO editar( ElfoEntity elfo, Integer id ) {
        elfo.setId(id);
        this.salvarEEditar(elfo);
        return new ElfoDTO(elfo);
    }

    private ElfoEntity salvarEEditar ( ElfoEntity elfo ) {
        return (ElfoEntity) repository.save(elfo);
    }

    public ElfoDTO buscarPorId( Integer id ) {
        return new ElfoDTO( (ElfoEntity) repository.findById(id).get());
    }

    public List<ElfoDTO> buscarTodosPorId (Integer id ) {
        List<ElfoEntity> elfo = repository.findAllById(id);
        if ( elfo != null ) {
            return this.transformarList(elfo);
        }
        return null;
    }

    public List<ElfoDTO> buscarTodosPorIdIn ( List<Integer> id ) {
        List<ElfoEntity> elfo = repository.findAllByIdIn(id);
        if ( elfo != null ) {
            return this.transformarList(elfo);
        }
        return null;
    }

    public ElfoDTO buscarPorInventario( InventarioEntity inventario ) {
        ElfoEntity elfo = (ElfoEntity) repository.findByInventario(inventario);
        if( elfo != null) {
            return new ElfoDTO(elfo);
        }
        return null;
    }

    public List<ElfoDTO> buscarTodosPorInventarioIn (List<InventarioEntity> inventario ) {
        List<ElfoEntity> elfo = repository.findAllByInventarioIn(inventario);
        if ( elfo != null ) {
            return this.transformarList(elfo);
        }
        return null;
    }

    public ElfoDTO buscarPorNome( String nome ) {
        ElfoEntity elfo = (ElfoEntity) repository.findByNome(nome);
        if( elfo != null) {
            return new ElfoDTO(elfo);
        }
        return null;
    }

    public List<ElfoDTO> buscarTodosPorNome (String nome ) {
        List<ElfoEntity> elfo = repository.findAllByNome(nome);
        if ( elfo != null ) {
            return this.transformarList(elfo);
        }
        return null;
    }

    public List<ElfoDTO> buscarTodosPorNomeIn ( List<String> nome ) {
        List<ElfoEntity> elfo = repository.findAllByNomeIn(nome);
        if ( elfo != null ) {
            return this.transformarList(elfo);
        }
        return null;
    }

    public ElfoDTO buscarPorVida( Double vida) {
        ElfoEntity elfo = (ElfoEntity) repository.findByVida(vida);
        if( elfo != null) {
            return new ElfoDTO(elfo);
        }
        return null;
    }

    public List<ElfoDTO> buscarTodosPorVida ( Double vida ) {
        List<ElfoEntity> elfo = repository.findAllByVida(vida);
        if ( elfo != null ) {
            return this.transformarList(elfo);
        }
        return null;
    }

    public List<ElfoDTO> buscarTodosPorVidaIn ( List<Double> vida ) {
        List<ElfoEntity> elfo = repository.findAllByVidaIn(vida);
        if ( elfo != null ) {
            return this.transformarList(elfo);
        }
        return null;
    }

    public ElfoDTO buscarPorStatus( StatusEnum status ) {
        ElfoEntity elfo = (ElfoEntity) repository.findByStatus(status);
        if( elfo != null ) {
            return new ElfoDTO(elfo);
        }
        return null;
    }

    public List<ElfoDTO> buscarTodosPorStatus ( StatusEnum status ) {
        List<ElfoEntity> elfo = repository.findAllByStatus(status);
        if ( elfo != null ) {
            return this.transformarList(elfo);
        }
        return null;
    }

    public List<ElfoDTO> buscarTodosPorStatusIn ( List<StatusEnum> status ) {
        List<ElfoEntity> elfo = repository.findAllByStatusIn(status);
        if ( elfo != null ) {
            return this.transformarList(elfo);
        }
        return null;
    }

    public ElfoDTO buscarPorQtdDano( Double qtdDano ) {
        ElfoEntity elfo = (ElfoEntity) repository.findByQtdDano(qtdDano);
        if( elfo != null ) {
            return new ElfoDTO(elfo);
        }
        return null;
    }

    public List<ElfoDTO> buscarTodosPorQtdDano ( Double qtdDano ) {
        List<ElfoEntity> elfo = repository.findAllByQtdDano(qtdDano);
        if ( elfo != null ) {
            return this.transformarList(elfo);
        }
        return null;
    }

    public List<ElfoDTO> buscarTodosPorQtdDanoIn ( List<Double> qtdDano ) {
        List<ElfoEntity> elfo = repository.findAllByQtdDanoIn(qtdDano);
        if ( elfo != null ) {
            return this.transformarList(elfo);
        }
        return null;
    }

    public ElfoDTO buscarPorExperiencia( Integer experiencia ) {
        ElfoEntity elfo = (ElfoEntity) repository.findByExperiencia(experiencia);
        if( elfo != null ) {
            return new ElfoDTO(elfo);
        }
        return null;
    }

    public List<ElfoDTO> buscarTodosPorExperiencia ( Integer experiencia ) {
        List<ElfoEntity> elfo = repository.findAllByExperiencia(experiencia);
        if ( elfo != null ) {
            return this.transformarList(elfo);
        }
        return null;
    }

    public List<ElfoDTO> buscarTodosPorExperienciaIn ( List<Integer> experiencia ) {
        List<ElfoEntity> elfo = repository.findAllByExperienciaIn(experiencia);
        if ( elfo != null ) {
            return this.transformarList(elfo);
        }
        return null;
    }

    public ElfoDTO buscarPorQtdExperienciaPorAtaque( Integer qtdExperienciaPorAtaque ) {
        ElfoEntity elfo = (ElfoEntity) repository.findByQtdExperienciaPorAtaque(qtdExperienciaPorAtaque);
        if( elfo != null) {
            return new ElfoDTO(elfo);
        }
        return null;
    }

    public List<ElfoDTO> buscarTodosPorQtdExperienciaPorAtaque ( Integer qtdExperienciaPorAtaque ) {
        List<ElfoEntity> elfo = repository.findAllByQtdExperienciaPorAtaque(qtdExperienciaPorAtaque);
        if ( elfo != null ) {
            return this.transformarList(elfo);
        }
        return null;
    }

    public List<ElfoDTO> buscarTodosPorQtdExperienciaPorAtaqueIn ( List<Integer> qtdExperienciaPorAtaque ) {
        List<ElfoEntity> elfo = repository.findAllByQtdExperienciaPorAtaqueIn(qtdExperienciaPorAtaque);
        if ( elfo != null ) {
            return this.transformarList(elfo);
        }
        return null;
    }
}
