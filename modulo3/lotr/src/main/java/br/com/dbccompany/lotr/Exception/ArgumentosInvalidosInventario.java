package br.com.dbccompany.lotr.Exception;

public class ArgumentosInvalidosInventario extends InventarioException {

    public ArgumentosInvalidosInventario() {
        super("Faltou campos");
    }
}
