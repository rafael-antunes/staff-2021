package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.InventarioDTO;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.Inventario_X_Item;
import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import br.com.dbccompany.lotr.Exception.ArgumentosInvalidosInventario;
import br.com.dbccompany.lotr.Exception.InventarioNaoEncontrado;
import br.com.dbccompany.lotr.Service.InventarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/inventario")
public class InventarioController {

    @Autowired
    private InventarioService service;

    @GetMapping( value = "/")
    @ResponseBody
    private List<InventarioDTO> trazerTodosInventarios () {
        return service.trazerTodosOsInventarios();
    }

    @GetMapping( value = "/{id}")
    @ResponseBody
    public InventarioDTO trazerInventarioEspecifico(@PathVariable Integer id ) throws InventarioNaoEncontrado {
        try {
            return service.buscarPorId(id);
        } catch ( Exception e ) {
            throw new InventarioNaoEncontrado();
        }
    }

    @PostMapping( value = "/salvar")
    @ResponseBody
    public InventarioDTO salvarInventario(@RequestBody InventarioDTO inventario ) throws ArgumentosInvalidosInventario {
        try {
            return service.salvar( inventario.converter() );
        } catch ( Exception e ) {
            throw new ArgumentosInvalidosInventario();
        }
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public InventarioDTO editarInventario(@RequestBody InventarioEntity inventario, @PathVariable Integer id ) {
        return service.editar(inventario, id);
    }

    @GetMapping( value = "/buscar/todos/porId/{id}" )
    @ResponseBody
    public List<InventarioDTO> trazerTodosPorId( @PathVariable Integer id ) {
        return service.buscarTodosPorId(id);
    }

    @GetMapping( value = "/buscar/todosIn/porId/" )
    @ResponseBody
    public List<InventarioDTO> trazerTodosPorIdIn( @RequestBody List<Integer> id ) {
        return service.buscarTodosPorIdIn(id);
    }

    @GetMapping( value = "/buscar/porItem/" )
    @ResponseBody
    public InventarioDTO trazerPorItem( @RequestBody Inventario_X_Item inventario ) {
        return service.buscarPorInventarioItem(inventario);
    }

    @GetMapping( value = "/buscar/todos/porItem/" )
    @ResponseBody
    public List<InventarioDTO> trazerTodosPorItem( @RequestBody Inventario_X_Item inventario ) {
        return service.buscarTodosInventarioItem(inventario);
    }

    @GetMapping( value = "/buscar/todosIn/porItem/" )
    @ResponseBody
    public List<InventarioDTO> trazerTodosPorItemIn( @RequestBody List<Inventario_X_Item> inventario ) {
        return service.buscarTodosPorInventarioItemIn(inventario);
    }

    @GetMapping( value = "/buscar/porPersonagem/" )
    @ResponseBody
    public InventarioDTO trazerPorInventario(@RequestBody PersonagemEntity personagem) {
        return service.buscarPorPersonagem(personagem);
    }

    @GetMapping( value = "/buscar/todosIn/porPersonagem/" )
    @ResponseBody
    public List<InventarioDTO> trazerTodosPorInventario( @RequestBody List<PersonagemEntity> personagem ) {
        return service.buscarPorTodosPersonagemIn(personagem);
    }
}
