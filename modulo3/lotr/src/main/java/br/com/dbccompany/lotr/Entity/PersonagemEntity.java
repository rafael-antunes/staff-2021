package br.com.dbccompany.lotr.Entity;

import javax.persistence.*;

@Entity
@Inheritance( strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorValue( value = "personagem" )
public abstract class PersonagemEntity {

    @Id
    @SequenceGenerator( name = "PERSONAGEM_SEQ", sequenceName = "PERSONAGEM_SEQ")
    @GeneratedValue( generator = "PERSONAGEM_SEQ", strategy = GenerationType.SEQUENCE)
    protected Integer id;

    @OneToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "id_inventario")
    private InventarioEntity inventario;

    @Column( nullable = false, unique = true )
    protected String nome;

    @Column( nullable = false, precision = 4, scale = 2 )
    protected Double vida;

    @Enumerated( EnumType.STRING )
    protected StatusEnum status;

    @Column( nullable = false, precision = 4, scale = 2 )
    protected Double qtdDano = 0.0;

    @Column( nullable = false )
    protected Integer experiencia = 0;

    @Column( nullable = false )
    protected Integer qtdExperienciaPorAtaque = 1;

    public PersonagemEntity( String nome ) {
        this.nome = nome;
    }

    public PersonagemEntity() {
        this.nome = "";
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getVida() {
        return vida;
    }

    public void setVida(Double vida) {
        this.vida = vida;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    public Double getQtdDano() {
        return qtdDano;
    }

    public void setQtdDano(Double qtdDano) {
        this.qtdDano = qtdDano;
    }

    public Integer getExperiencia() {
        return experiencia;
    }

    public void setExperiencia(Integer experiencia) {
        this.experiencia = experiencia;
    }

    public Integer getQtdExperienciaPorAtaque() {
        return qtdExperienciaPorAtaque;
    }

    public void setQtdExperienciaPorAtaque(Integer qtdExperienciaPorAtaque) {
        this.qtdExperienciaPorAtaque = qtdExperienciaPorAtaque;
    }

    public InventarioEntity getInventario() {
        return inventario;
    }

    public void setInventario(InventarioEntity inventario) {
        this.inventario = inventario;
    }
}
