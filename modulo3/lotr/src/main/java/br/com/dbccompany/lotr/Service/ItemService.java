package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.ItemDTO;
import br.com.dbccompany.lotr.Entity.Inventario_X_Item;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import br.com.dbccompany.lotr.Exception.ArgumentosInvalidosItem;
import br.com.dbccompany.lotr.Exception.ItemNaoEncontrado;
import br.com.dbccompany.lotr.LotrApplication;
import br.com.dbccompany.lotr.Repository.ItemRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@Service
public class ItemService {

    @Autowired
    private ItemRepository repository;

    private Logger logger = LoggerFactory.getLogger(LotrApplication.class);

    RestTemplate restTemplate = new RestTemplate();
    String url = "http://localhost:8081/salvar/logs";

    public List<ItemDTO> trazerTodosOsItens() {
        List<ItemEntity> item = (List<ItemEntity>) repository.findAll();
        if ( item != null ) {
            return this.transformarList(item);
        }
        return null;
    }

    private List<ItemDTO> transformarList (List<ItemEntity> item ) {
        List<ItemDTO> retorno = new ArrayList<>();
        for ( ItemEntity presente : item ) {
            retorno.add(new ItemDTO(presente));
        }
        return retorno;
    }

    @Transactional( rollbackFor = Exception.class )
    public ItemDTO salvar(ItemEntity item ) throws ArgumentosInvalidosItem {
        try {
            ItemEntity itemNovo = this.salvarEEditar(item);
            return new ItemDTO(itemNovo);
        } catch ( Exception e ) {
            throw new ArgumentosInvalidosItem();
        }

    }

    @Transactional( rollbackFor = Exception.class )
    public ItemDTO editar( ItemEntity item, Integer id ) {
        item.setId(id);
        this.salvarEEditar(item);
        return new ItemDTO(item);
    }

    private ItemEntity salvarEEditar ( ItemEntity item ) {
        return repository.save(item);
    }

    public ItemDTO buscarPorId( Integer id ) throws ItemNaoEncontrado {
        try {
            return new ItemDTO(repository.findById(id).get());
        } catch ( Exception e ) {
            logger.error("Item teve problema ao buscar específico: " + e.getMessage());
            restTemplate.postForObject(url, e.getMessage(), Object.class);
            throw new ItemNaoEncontrado();
        }
    }

    public List<ItemDTO> buscarTodosPorId (Integer id ) {
        List<ItemEntity> item = repository.findAllById(id);
        List<ItemDTO> retorno = null;
        for ( ItemEntity presente : item ) {
            retorno.add(new ItemDTO(presente));
        }
        if ( retorno != null ) {
            return retorno;
        }
        return null;
    }

    public List<ItemDTO> buscarTodosPorIdIn ( List<Integer> id ) {
        List<ItemEntity> item = repository.findAllByIdIn(id);
        List<ItemDTO> retorno = null;
        for ( ItemEntity presente : item ) {
            retorno.add(new ItemDTO(presente));
        }
        if ( retorno != null ) {
            return retorno;
        }
        return null;
    }

    public ItemDTO buscarPorDescricao( String descricao ){
        ItemEntity item = repository.findByDescricao(descricao);
        if( item != null ) {
            return new ItemDTO(repository.findByDescricao(descricao));
        }
        return null;
    }

    public List<ItemDTO> buscarTodosPorDescricao ( String descricao ) {
        List<ItemEntity> item = repository.findAllByDescricao(descricao);
        List<ItemDTO> retorno = null;
        for ( ItemEntity presente : item ) {
            retorno.add(new ItemDTO(presente));
        }
        if ( retorno != null ) {
            return retorno;
        }
        return null;
    }

    public List<ItemDTO> buscarTodosPorDescricaoIn ( List<String> descricao ) {
        List<ItemEntity> item = repository.findAllByDescricaoIn(descricao);
        List<ItemDTO> retorno = null;
        for ( ItemEntity presente : item ) {
            retorno.add(new ItemDTO(presente));
        }
        if ( retorno != null ) {
            return retorno;
        }
        return null;
    }

    public ItemDTO buscarPorInventarioItem( Inventario_X_Item inventario_x_item ) {
        ItemEntity item = repository.findByInventarioItem(inventario_x_item);
        if( item != null ) {
            return new ItemDTO(repository.findByInventarioItem(inventario_x_item));
        }
        return null;
    }

    public List<ItemDTO> buscarTodosInventarioItem ( Inventario_X_Item inventario_x_item ) {
        List<ItemEntity> item = repository.findAllByInventarioItem(inventario_x_item);
        List<ItemDTO> retorno = null;
        for ( ItemEntity presente : item ) {
            retorno.add(new ItemDTO(presente));
        }
        if ( retorno != null ) {
            return retorno;
        }
        return null;
    }

    public List<ItemDTO> buscarTodosPorInventarioItemIn ( List<Inventario_X_Item> inventario_x_item ) {
        List<ItemEntity> item = repository.findAllByInventarioItemIn(inventario_x_item);
        List<ItemDTO> retorno = null;
        for ( ItemEntity presente : item ) {
            retorno.add(new ItemDTO(presente));
        }
        if ( retorno != null ) {
            return retorno;
        }
        return null;
    }

}
