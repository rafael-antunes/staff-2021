package br.com.dbccompany.lotr.Exception;

public class Inventario_X_ItemNaoEncontrado extends Inventario_X_ItemException {

    public Inventario_X_ItemNaoEncontrado() {
        super("Inventario x Item não encontrado!");
    }
}
