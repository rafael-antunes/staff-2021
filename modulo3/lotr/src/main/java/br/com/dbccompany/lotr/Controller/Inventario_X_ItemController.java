package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.Inventario_X_ItemDTO;
import br.com.dbccompany.lotr.Entity.*;
import br.com.dbccompany.lotr.Exception.ArgumentosInvalidosInventario_X_Item;
import br.com.dbccompany.lotr.Exception.Inventario_X_ItemNaoEncontrado;
import br.com.dbccompany.lotr.Service.Inventario_X_ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/api/inventario_X_item")
public class Inventario_X_ItemController {

    @Autowired
    private Inventario_X_ItemService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<Inventario_X_ItemDTO> trazerTodosInventarioItem(){
        return service.trazerTodosOsInventarioItem();
    }

    @GetMapping( value ="/id")
    @ResponseBody
    public Inventario_X_ItemDTO trazerItemEspecifico(@RequestBody Inventario_X_ItemId id ) throws Inventario_X_ItemNaoEncontrado {
        try {
            return service.buscarPorId(id);
        } catch (Exception e ) {
            throw new Inventario_X_ItemNaoEncontrado();
        }
    }

    @PostMapping( value = "/salvar")
    @ResponseBody
    public Inventario_X_ItemDTO salvarItem(@RequestBody Inventario_X_ItemDTO inventarioItem ) throws ArgumentosInvalidosInventario_X_Item {
        try {
            return service.salvar( inventarioItem.converter() );
        } catch (Exception e ) {
            throw new ArgumentosInvalidosInventario_X_Item();
        }
    }

    @PutMapping( value = "/editar/" )
    @ResponseBody
    public Inventario_X_ItemDTO editarItem(@RequestBody Inventario_X_Item inventarioItem, @PathVariable Inventario_X_ItemId id ) {
        return service.editar(inventarioItem, id);
    }

    @GetMapping( value = "/buscar/todos/porId/" )
    @ResponseBody
    public List<Inventario_X_ItemDTO> trazerTodosPorId( @RequestBody Inventario_X_ItemId id ) {
        return service.buscarTodosPorId(id);
    }

    @GetMapping( value = "/buscar/todosIn/porId/" )
    @ResponseBody
    public List<Inventario_X_ItemDTO> trazerTodosPorIdIn( @RequestBody List<Inventario_X_ItemId> id ) {
        return service.buscarTodosPorIdIn(id);
    }

    @GetMapping( value = "/buscar/porQuantidade/{qtd}" )
    @ResponseBody
    public Inventario_X_ItemDTO trazerPorQuantidade( @PathVariable Integer qtd ) {
        return service.buscarPorQuantidade(qtd);
    }

    @GetMapping( value = "/buscar/todos/porQuantidade/{qtd}" )
    @ResponseBody
    public List<Inventario_X_ItemDTO> trazerTodosPorQuantidade( @PathVariable Integer qtd ) {
        return service.buscarTodosPorQuantidade(qtd);
    }

    @GetMapping( value = "/buscar/todosIn/porQuantidade/" )
    @ResponseBody
    public List<Inventario_X_ItemDTO> trazerTodosPorQuantidadeIn( @RequestBody List<Integer> qtd ) {
        return service.buscarTodosPorQuantidadeIn(qtd);
    }

    @GetMapping( value = "/buscar/porInventario/" )
    @ResponseBody
    public Inventario_X_ItemDTO trazerPorInventario( @RequestBody InventarioEntity inventario ) {
        return service.buscarPorInventario(inventario);
    }

    @GetMapping( value = "/buscar/Todos/PorInventario/")
    @ResponseBody
    public List<Inventario_X_ItemDTO> trazerTodosPorInventario  ( @RequestBody InventarioEntity inventario ) {
        return service.buscarTodosPorInventario(inventario);
    }

    @GetMapping( value = "/buscar/todosIn/porInventario/" )
    @ResponseBody
    public List<Inventario_X_ItemDTO> trazerTodosPorInventario( @RequestBody List<InventarioEntity> inventario ) {
        return service.buscarTodosPorInventarioIn(inventario);
    }

    @GetMapping( value = "/buscar/porItem/" )
    @ResponseBody
    public Inventario_X_ItemDTO trazerPorItem( @RequestBody ItemEntity item ) {
        return service.buscarPorItem(item);
    }

    @GetMapping( value = "/buscar/Todos/porItem/")
    @ResponseBody
    public List<Inventario_X_ItemDTO> trazerTodosPorItem  ( @RequestBody ItemEntity item ) {
        return service.buscarTodosPorItem(item);
    }

    @GetMapping( value = "/buscar/todosIn/porItem/" )
    @ResponseBody
    public List<Inventario_X_ItemDTO> trazerTodosPorItem( @RequestBody List<ItemEntity> item ) {
        return service.buscarTodosPorItemIn(item);
    }
}
