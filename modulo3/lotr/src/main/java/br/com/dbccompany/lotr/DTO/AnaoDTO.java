package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.AnaoEntity;

public class AnaoDTO extends PersonagemDTO {

    public AnaoDTO(AnaoEntity anao) {
        super(anao);
    }

    public AnaoDTO() {
        super(new AnaoEntity("Gimli"));
    }
}
