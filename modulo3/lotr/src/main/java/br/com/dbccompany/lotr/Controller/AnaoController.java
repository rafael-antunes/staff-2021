package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.AnaoDTO;
import br.com.dbccompany.lotr.Entity.AnaoEntity;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.StatusEnum;
import br.com.dbccompany.lotr.Exception.AnaoNaoEncontrado;
import br.com.dbccompany.lotr.Exception.ArgumentosInvalidosAnao;
import br.com.dbccompany.lotr.Service.AnaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/anao" )
public class AnaoController {

    @Autowired
    private AnaoService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<AnaoDTO> trazerTodosAnoes(){
        return service.trazerTodosOsAnoes();
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public AnaoDTO trazerAnaoEspecifico(@PathVariable Integer id ) throws AnaoNaoEncontrado {
        try {
            return service.buscarPorId(id);
        } catch ( Exception e ) {
            throw new AnaoNaoEncontrado();
        }
    }

    @PostMapping( value = "/salvar")
    @ResponseBody
    public AnaoDTO salvarAnao(@RequestBody AnaoDTO anao ) throws ArgumentosInvalidosAnao {
        try {
            return service.salvar((AnaoEntity) anao.converter(new AnaoEntity(anao.getNome())));
        } catch ( Exception e ) {
            throw new ArgumentosInvalidosAnao();
        }
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public AnaoDTO editarAnao(@RequestBody AnaoEntity anao, @PathVariable Integer id ) {
        return service.editar(anao, id);
    }

    @GetMapping( value = "/buscar/todos/porId/{id}" )
    @ResponseBody
    public List<AnaoDTO> trazerTodosPorId( @PathVariable Integer id ) {
        return service.buscarTodosPorId(id);
    }

    @GetMapping( value = "/buscar/todosIn/porId/" )
    @ResponseBody
    public List<AnaoDTO> trazerTodosPorIdIn( @RequestBody List<Integer> id ) {
        return service.buscarTodosPorIdIn(id);
    }

    @GetMapping( value = "/buscar/porInventario/{inventario}" )
    @ResponseBody
    public AnaoDTO trazerPorInventario( @PathVariable InventarioEntity inventario ) {
        return service.buscarPorInventario(inventario);
    }

    @GetMapping( value = "/buscar/todosIn/porInventario/" )
    @ResponseBody
    public List<AnaoDTO> trazerTodosPorInventario( @RequestBody List<InventarioEntity> inventario ) {
        return service.buscarTodosPorInventarioIn(inventario);
    }

    @GetMapping( value = "/buscar/porNome/{nome}" )
    @ResponseBody
    public AnaoDTO trazerPorNome( @PathVariable String nome ) {
        return service.buscarPorNome(nome);
    }

    @GetMapping( value = "/buscar/todos/porNome/{nome}" )
    @ResponseBody
    public List<AnaoDTO> trazerTodosPorNome( @PathVariable String nome ) {
        return service.buscarTodosPorNome(nome);
    }

    @GetMapping( value = "/buscar/todosIn/porNome/" )
    @ResponseBody
    public List<AnaoDTO> trazerTodosPorNomeIn ( @RequestBody List<String> nome ) {
        return service.buscarTodosPorNomeIn(nome);
    }

    @GetMapping( value = "/buscar/porVida/{vida}" )
    @ResponseBody
    public AnaoDTO trazerPorVida( @PathVariable Double vida ) {
        return service.buscarPorVida(vida);
    }

    @GetMapping( value = "/buscar/todos/porVida/{vida}" )
    @ResponseBody
    public List<AnaoDTO> trazerTodosPorVida( @PathVariable Double vida ) {
        return service.buscarTodosPorVida(vida);
    }

    @GetMapping( value = "/buscar/todosIn/porVida/" )
    @ResponseBody
    public List<AnaoDTO> trazerTodosPorVidaIn( @RequestBody List<Double> vida ) {
        return service.buscarTodosPorVidaIn(vida);
    }

    @GetMapping( value = "/buscar/porStatus/{status}" )
    @ResponseBody
    public AnaoDTO trazerPorStatus( @PathVariable StatusEnum status ) {
        return service.buscarPorStatus(status);
    }

    @GetMapping( value = "/buscar/todos/porStatus/{status}" )
    @ResponseBody
    public List<AnaoDTO> trazerTodosPorStatus( @PathVariable StatusEnum status ) {
        return service.buscarTodosPorStatus(status);
    }

    @GetMapping( value = "/buscar/todosIn/porStatus/" )
    @ResponseBody
    public List<AnaoDTO> trazerTodosPorStatusIn( @RequestBody List<StatusEnum> status ) {
        return service.buscarTodosPorStatusIn(status);
    }

    @GetMapping( value = "/buscar/porQtdDano/{qtdDano}" )
    @ResponseBody
    public AnaoDTO trazerPorQtdDano( @PathVariable Double qtdDano ) {
        return service.buscarPorQtdDano(qtdDano);
    }

    @GetMapping( value = "/buscar/todos/porQtdDano/{qtdDano}" )
    @ResponseBody
    public List<AnaoDTO> trazerTodosPorQtdDano( @PathVariable Double qtdDano ) {
        return service.buscarTodosPorQtdDano(qtdDano);
    }

    @GetMapping( value = "/buscar/todosIn/porQtdDano/" )
    @ResponseBody
    public List<AnaoDTO> trazerTodosPorQtdDanoIn( @RequestBody List<Double> qtdDano ) {
        return service.buscarTodosPorQtdDanoIn(qtdDano);
    }

    @GetMapping( value = "/buscar/porExperiencia/{experiencia}" )
    @ResponseBody
    public AnaoDTO trazerPorExperiencia( @PathVariable Integer experiencia ) {
        return service.buscarPorExperiencia(experiencia);
    }

    @GetMapping( value = "/buscar/todos/porExperiencia/{experiencia}" )
    @ResponseBody
    public List<AnaoDTO> trazerTodosPorExperiencia( @PathVariable Integer experiencia ) {
        return service.buscarTodosPorExperiencia(experiencia);
    }

    @GetMapping( value = "/buscar/todosIn/porExperiencia/" )
    @ResponseBody
    public List<AnaoDTO> trazerTodosPorExperienciaIn( @RequestBody List<Integer> experiencia ) {
        return service.buscarTodosPorExperienciaIn(experiencia);
    }

    @GetMapping( value = "/buscar/porQtdExperienciaPorAtaque/{qtdExperienciaPorAtaque}" )
    @ResponseBody
    public AnaoDTO trazerPorQtdExperienciaPorAtaque( @PathVariable Integer qtdExperienciaPorAtaque ) {
        return service.buscarPorQtdExperienciaPorAtaque(qtdExperienciaPorAtaque);
    }

    @GetMapping( value = "/buscar/todos/porQtdExperienciaPorAtaque/{qtdExperienciaPorAtaque}" )
    @ResponseBody
    public List<AnaoDTO> trazerTodosPorQtdExperienciaPorAtaque( @PathVariable Integer qtdExperienciaPorAtaque ) {
        return service.buscarTodosPorQtdExperienciaPorAtaque(qtdExperienciaPorAtaque);
    }

    @GetMapping( value = "/buscar/todosIn/porQtdExperienciaPorAtaque/" )
    @ResponseBody
    public List<AnaoDTO> trazerTodosPorQtdExperienciaPorAtaque( @RequestBody List<Integer> qtdExperienciaPorAtaque ) {
        return service.buscarTodosPorQtdExperienciaPorAtaqueIn(qtdExperienciaPorAtaque);
    }
}
