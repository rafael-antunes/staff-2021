package br.com.dbccompany.lotr.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class ItemEntity {

    @Id
    @SequenceGenerator( name = "ITEM_SEQ", sequenceName = "ITEM_SEQ")
    @GeneratedValue( generator = "ITEM_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column( nullable = false, unique = true )
    private String descricao;

    @OneToMany( fetch = FetchType.LAZY, mappedBy = "id.id_item" )
    private List<Inventario_X_Item> inventarioItem;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public List<Inventario_X_Item> getInventarioItem() {
        return inventarioItem;
    }
}
