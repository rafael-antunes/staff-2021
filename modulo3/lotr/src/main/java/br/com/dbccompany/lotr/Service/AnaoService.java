package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.AnaoDTO;
import br.com.dbccompany.lotr.Entity.AnaoEntity;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.StatusEnum;
import br.com.dbccompany.lotr.Exception.AnaoNaoEncontrado;
import br.com.dbccompany.lotr.Exception.ArgumentosInvalidosAnao;
import br.com.dbccompany.lotr.Repository.PersonagemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class AnaoService {

    @Autowired
    private PersonagemRepository repository;

    public List<AnaoDTO> trazerTodosOsAnoes() {
        List<AnaoEntity> anoes = (List<AnaoEntity>) repository.findAll();
        if ( anoes != null) {
            return this.transformarList(anoes);
        }
        return null;
    }

    private List<AnaoDTO> transformarList ( List<AnaoEntity> anoes ) {
        List<AnaoDTO> retorno = new ArrayList<>();
        for ( AnaoEntity presente : anoes ) {
            retorno.add(new AnaoDTO(presente));
        }
        return retorno;
    }

    @Transactional( rollbackFor = Exception.class )
    public AnaoDTO salvar( AnaoEntity anao ) {
        AnaoEntity anaoNovo = this.salvarEEditar(anao);
        return new AnaoDTO(anaoNovo);
    }

    @Transactional( rollbackFor = Exception.class )
    public AnaoDTO editar( AnaoEntity anao, Integer id ) {
        anao.setId(id);
        this.salvarEEditar(anao);
        return new AnaoDTO(anao);
    }

    private AnaoEntity salvarEEditar ( AnaoEntity anao ) {
        return (AnaoEntity) repository.save(anao);
    }

    public AnaoDTO buscarPorId( Integer id ) {
        return new AnaoDTO((AnaoEntity) repository.findById(id).get());
    }

    public List<AnaoDTO> buscarTodosPorId (Integer id ) {
        List<AnaoEntity> anao = repository.findAllById(id);
        if ( anao != null ) {
            return this.transformarList(anao);
        }
        return null;
    }

    public List<AnaoDTO> buscarTodosPorIdIn ( List<Integer> id ) {
        List<AnaoEntity> anao = repository.findAllByIdIn(id);
        if ( anao != null ) {
            return this.transformarList(anao);
        }
        return null;
    }

    public AnaoDTO buscarPorInventario( InventarioEntity inventario ) {
        AnaoEntity anao = (AnaoEntity) repository.findByInventario(inventario);
        if( anao != null) {
            return new AnaoDTO(anao);
        }
        return null;
    }

    public List<AnaoDTO> buscarTodosPorInventarioIn (List<InventarioEntity> inventario ) {
        List<AnaoEntity> anao = repository.findAllByInventarioIn(inventario);
        if( anao != null) {
            return this.transformarList(anao);
        }
        return null;
    }

    public AnaoDTO buscarPorNome( String nome ) {
        AnaoEntity anao = (AnaoEntity) repository.findByNome(nome);
        if( anao != null) {
            return new AnaoDTO(anao);
        }
        return null;
    }

    public List<AnaoDTO> buscarTodosPorNome (String nome ) {
        List<AnaoEntity> anao = repository.findAllByNome(nome);
        if( anao != null) {
            return this.transformarList(anao);
        }
        return null;
    }

    public List<AnaoDTO> buscarTodosPorNomeIn ( List<String> nome ) {
        List<AnaoEntity> anao = repository.findAllByNomeIn(nome);
        if( anao != null) {
            return this.transformarList(anao);
        }
        return null;
    }

    public AnaoDTO buscarPorVida( Double vida) {
        AnaoEntity anao = (AnaoEntity) repository.findByVida(vida);
        if( anao != null) {
            return new AnaoDTO(anao);
        }
        return null;
    }

    public List<AnaoDTO> buscarTodosPorVida ( Double vida ) {
        List<AnaoEntity> anao = repository.findAllByVida(vida);
        if( anao != null) {
            return this.transformarList(anao);
        }
        return null;
    }

    public List<AnaoDTO> buscarTodosPorVidaIn ( List<Double> vida ) {
        List<AnaoEntity> anao = repository.findAllByVidaIn(vida);
        if( anao != null) {
            return this.transformarList(anao);
        }
        return null;
    }

    public AnaoDTO buscarPorStatus( StatusEnum status ) {
        AnaoEntity anao = (AnaoEntity) repository.findByStatus(status);
        if( anao != null ) {
            return new AnaoDTO(anao);
        }
        return null;
    }

    public List<AnaoDTO> buscarTodosPorStatus ( StatusEnum status ) {
        List<AnaoEntity> anao = repository.findAllByStatus(status);
        if( anao != null) {
            return this.transformarList(anao);
        }
        return null;
    }

    public List<AnaoDTO> buscarTodosPorStatusIn ( List<StatusEnum> status ) {
        List<AnaoEntity> anao = repository.findAllByStatusIn(status);
        if( anao != null) {
            return this.transformarList(anao);
        }
        return null;
    }

    public AnaoDTO buscarPorQtdDano( Double qtdDano ) {
        AnaoEntity anao = (AnaoEntity) repository.findByQtdDano(qtdDano);
        if( anao != null ) {
            return new AnaoDTO(anao);
        }
        return null;
    }

    public List<AnaoDTO> buscarTodosPorQtdDano ( Double qtdDano ) {
        List<AnaoEntity> anao = repository.findAllByQtdDano(qtdDano);
        if( anao != null) {
            return this.transformarList(anao);
        }
        return null;
    }

    public List<AnaoDTO> buscarTodosPorQtdDanoIn ( List<Double> qtdDano ) {
        List<AnaoEntity> anao = repository.findAllByQtdDanoIn(qtdDano);
        if( anao != null) {
            return this.transformarList(anao);
        }
        return null;
    }

    public AnaoDTO buscarPorExperiencia( Integer experiencia ) {
        AnaoEntity anao = (AnaoEntity) repository.findByExperiencia(experiencia);
        if( anao != null ) {
            return new AnaoDTO(anao);
        }
        return null;
    }

    public List<AnaoDTO> buscarTodosPorExperiencia ( Integer experiencia ) {
        List<AnaoEntity> anao = repository.findAllByExperiencia(experiencia);
        if( anao != null) {
            return this.transformarList(anao);
        }
        return null;
    }

    public List<AnaoDTO> buscarTodosPorExperienciaIn ( List<Integer> experiencia ) {
        List<AnaoEntity> anao = repository.findAllByExperienciaIn(experiencia);
        if( anao != null) {
            return this.transformarList(anao);
        }
        return null;
    }

    public AnaoDTO buscarPorQtdExperienciaPorAtaque( Integer qtdExperienciaPorAtaque ) {
        AnaoEntity anao = (AnaoEntity) repository.findByQtdExperienciaPorAtaque(qtdExperienciaPorAtaque);
        if( anao != null) {
            return new AnaoDTO(anao);
        }
        return null;
    }

    public List<AnaoDTO> buscarTodosPorQtdExperienciaPorAtaque ( Integer qtdExperienciaPorAtaque ) {
        List<AnaoEntity> anao = repository.findAllByQtdExperienciaPorAtaque(qtdExperienciaPorAtaque);
        if( anao != null) {
            return this.transformarList(anao);
        }
        return null;
    }

    public List<AnaoDTO> buscarTodosPorQtdExperienciaPorAtaqueIn ( List<Integer> qtdExperienciaPorAtaque ) {
        List<AnaoEntity> anao = repository.findAllByQtdExperienciaPorAtaqueIn(qtdExperienciaPorAtaque);
        if( anao != null) {
            return this.transformarList(anao);
        }
        return null;
    }
}
