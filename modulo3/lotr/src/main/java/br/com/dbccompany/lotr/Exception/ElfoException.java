package br.com.dbccompany.lotr.Exception;

public class ElfoException extends Exception {
    private String mensagem;

    public ElfoException ( String mensagem ) {
        this.mensagem = mensagem;
    }

    public String getMensagem() {
        return mensagem;
    }
}
