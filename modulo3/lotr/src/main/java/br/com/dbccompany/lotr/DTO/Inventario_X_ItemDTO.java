package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.Inventario_X_Item;
import br.com.dbccompany.lotr.Entity.Inventario_X_ItemId;

public class Inventario_X_ItemDTO {
    private Inventario_X_ItemId id;
    private int quantidade;

    public Inventario_X_ItemDTO(Inventario_X_Item inventarioItem) {
        this.id = inventarioItem.getId();
        this.quantidade = inventarioItem.getQuantidade();
    }

    public Inventario_X_ItemDTO() {
    }

    public Inventario_X_Item converter () {
        Inventario_X_Item inventarioItem = new Inventario_X_Item();
        inventarioItem.setId(this.id);
        inventarioItem.setQuantidade(this.quantidade);
        return inventarioItem;
    }

    public Inventario_X_ItemId getId() {
        return id;
    }

    public void setId(Inventario_X_ItemId id) {
        this.id = id;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }
}
