import java.util.*;

public class ExercitoQueAtaca extends Exercito {
    private Estrategia estrategia;
    
    public ExercitoQueAtaca ( Estrategia estrategia ) {
        this.estrategia = estrategia;
    }
    
    public void trocarEstrategia ( Estrategia estrategia ) {
        this.estrategia = estrategia;   
    }
    
    public void atacar (ArrayList<Dwarf> dwarves ) {
        ArrayList<Elfo> ordem = this.estrategia.estrategia( this.getList() );
        for ( Elfo elfo : ordem ){
            for ( Dwarf dwarf : dwarves ){
                elfo.atirarFlecha(dwarf);
            }
        }
    }
}
