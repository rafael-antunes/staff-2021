import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ElfoNoturnoTest 
{
    @Test
    public void atiraUmaFlechaGanha3XPPerde15HP () {
        ElfoNoturno elfo = new ElfoNoturno ( "Noturno" );
        Dwarf dwarf = new Dwarf ( "Gimli" );
        elfo.atirarFlecha ( dwarf );
        assertEquals ( 1, elfo.getFlecha().getQuantidade() );
        assertEquals ( 3, elfo.getExperiencia());
        assertEquals ( 85, elfo.getHP(), 1e-8 );
    }
}
