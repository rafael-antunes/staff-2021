import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.*;

public class ExercitoTest {
    
    @Test
    public void tentaRecrutarElfoDaLuzENaoConsegue () {
        Exercito exercito = new Exercito ();
        exercito.recrutar( new ElfoDaLuz( "Feanor" ) );
        assertEquals ( 0, exercito.getList().size() );
    }
    
    @Test
    public void tentaRecrutarUmElfoVerdeEUmElfoNoturno () {
        Exercito exercito = new Exercito ();
        exercito.recrutar( new ElfoNoturno( "Dark" ) );
        exercito.recrutar( new ElfoVerde( "Green" ) );
        assertEquals ( 2, exercito.getList().size() );
    }
    
    @Test
    public void fazerListaDeDoisElfosQueTomaramDanoEUmQueNao () {
        Exercito exercito = new Exercito ();
        exercito.recrutar( new ElfoNoturno( "Dark" ) );
        exercito.recrutar( new ElfoVerde( "Green" ) );
        exercito.recrutar( new ElfoVerde( "Greener" ) );
        assertEquals ( 3, exercito.buscarPorStatus( Status.RECEM_CRIADO ).size() );
    }
    
    @Test 
    public void fazerOrdemDeAtaqueNoturnosPorUltimoEAtacar () {
        Exercito exercito = new Exercito();
        ElfoNoturno noturnoVivo = new ElfoNoturno( "NVivo" );
        ElfoNoturno noturnoMorto = new ElfoNoturno( "NMorto" );
        ElfoVerde verdeVivo = new ElfoVerde( "VVivo" );
        ElfoVerde verdeMorto = new ElfoVerde( "VMorto" );
        Dwarf dwarf = new Dwarf ("Gimli");
        noturnoMorto.setStatus( Status.MORTO );
        verdeMorto.setStatus( Status.MORTO );
        ArrayList<Elfo> ataque = new ArrayList<>();
        ArrayList<Elfo> ataqueOrdenado = new ArrayList<>();
        ataque.add( noturnoVivo );
        ataque.add( noturnoMorto );
        ataque.add( verdeVivo );
        ataque.add( verdeMorto );
        ataqueOrdenado.add( verdeVivo );
        ataqueOrdenado.add( noturnoVivo );
        assertEquals ( ataqueOrdenado, exercito.estrategia.estrategia( ataque ) );
        exercito.realizarAtaque( exercito.estrategia.estrategia( ataque ), dwarf );
        assertEquals ( 90.0, dwarf.getHP(), 1e-8 );
    }
    
    @Test
    public void fazerOrdemDeAtaqueIntercaladoEAtacar () {
        Estrategia estrategia = new AtaqueIntercalado();
        Exercito exercito = new Exercito( estrategia );
        ElfoNoturno noturnoVivo1 = new ElfoNoturno( "NVivo1" );
        ElfoNoturno noturnoVivo2 = new ElfoNoturno( "NVivo2" );
        ElfoNoturno noturnoVivo3 = new ElfoNoturno( "NVivo3" );
        ElfoNoturno noturnoMorto = new ElfoNoturno( "NMorto" );
        ElfoVerde verdeVivo1 = new ElfoVerde( "VVivo1" );
        ElfoVerde verdeVivo2 = new ElfoVerde( "VVivo2" );
        ElfoVerde verdeMorto = new ElfoVerde( "VMorto" );
        noturnoMorto.setStatus( Status.MORTO );
        verdeMorto.setStatus( Status.MORTO );
        ArrayList<Elfo> ataque = new ArrayList<>();
        ArrayList<Elfo> ataqueOrdenado = new ArrayList<>();
        ataque.add( noturnoVivo1 );
        ataque.add( noturnoVivo2 );
        ataque.add( noturnoVivo3 );
        ataque.add( noturnoMorto );
        ataque.add( verdeVivo1 );
        ataque.add( verdeVivo2 );
        ataque.add( verdeMorto );
        ataqueOrdenado.add( noturnoVivo1 );
        ataqueOrdenado.add( verdeVivo1 );
        ataqueOrdenado.add( noturnoVivo2 );
        ataqueOrdenado.add( verdeVivo2 );
        Dwarf dwarf = new Dwarf ("Gimli");
        assertEquals ( ataqueOrdenado, estrategia.estrategia( ataque ) );
    }
    
    @Test
    public void fazerOrdemDeAtaqueApenasUmTercoAtaca () {
        Estrategia estrategia = new ApenasUmTerco();
        Exercito exercito = new Exercito( estrategia );
        ElfoNoturno noturnoVivo1 = new ElfoNoturno( "NVivo1" );
        ElfoNoturno noturnoVivo2 = new ElfoNoturno( "NVivo2" );
        ElfoNoturno noturnoMorto = new ElfoNoturno( "NMorto" );
        noturnoVivo1.getFlecha().setQuantidade(3);
        noturnoVivo2.getFlecha().setQuantidade(12);
        ElfoVerde verdeVivo1 = new ElfoVerde( "VVivo1" );
        ElfoVerde verdeVivo2 = new ElfoVerde( "VVivo2" );
        ElfoVerde verdeVivo3 = new ElfoVerde( "VVivo3" );
        ElfoVerde verdeMorto = new ElfoVerde( "VMorto" );
        verdeVivo1.getFlecha().setQuantidade(0);
        verdeVivo2.getFlecha().setQuantidade(40);
        verdeVivo3.getFlecha().setQuantidade(50);
        noturnoMorto.setStatus( Status.MORTO );
        verdeMorto.setStatus( Status.MORTO );
        ArrayList<Elfo> ataque = new ArrayList<>();
        ArrayList<Elfo> ataqueOrdenado = new ArrayList<>();
        ataque.add( noturnoVivo1 );
        ataque.add( noturnoVivo2 );
        ataque.add( noturnoMorto );
        ataque.add( verdeVivo1 );
        ataque.add( verdeVivo2 );
        ataque.add( verdeVivo3 );
        ataque.add( verdeMorto );
        ataqueOrdenado.add( verdeVivo3 );
        Dwarf dwarf = new Dwarf ("Gimli");
        assertEquals ( ataqueOrdenado, estrategia.estrategia( ataque ) );
    }
    
    @Test
    public void fazerOrdemDeAtaqueApenasUmTercoAtacaComMaisDe1NoPelotaoFinal () {
        Estrategia estrategia = new ApenasUmTerco();
        Exercito exercito = new Exercito( estrategia );
        ElfoNoturno noturnoVivo1 = new ElfoNoturno( "NVivo1" );
        ElfoNoturno noturnoVivo2 = new ElfoNoturno( "NVivo2" );
        ElfoNoturno noturnoVivo3 = new ElfoNoturno( "NVivo3" );
        ElfoNoturno noturnoVivo4 = new ElfoNoturno( "NVivo4" );
        ElfoNoturno noturnoVivo5 = new ElfoNoturno( "NVivo5" );
        ElfoNoturno noturnoMorto = new ElfoNoturno( "NMorto" );
        noturnoVivo1.getFlecha().setQuantidade(3);
        noturnoVivo2.getFlecha().setQuantidade(12);
        noturnoVivo3.getFlecha().setQuantidade(30);
        ElfoVerde verdeVivo1 = new ElfoVerde( "VVivo1" );
        ElfoVerde verdeVivo2 = new ElfoVerde( "VVivo2" );
        ElfoVerde verdeVivo3 = new ElfoVerde( "VVivo3" );
        ElfoVerde verdeVivo4 = new ElfoVerde( "VVivo4" );
        ElfoVerde verdeMorto = new ElfoVerde( "VMorto" );
        verdeVivo1.getFlecha().setQuantidade(7);
        verdeVivo2.getFlecha().setQuantidade(22);
        verdeVivo3.getFlecha().setQuantidade(25);
        noturnoMorto.setStatus( Status.MORTO );
        verdeMorto.setStatus( Status.MORTO );
        ArrayList<Elfo> ataque = new ArrayList<>();
        ArrayList<Elfo> ataqueOrdenado = new ArrayList<>();
        ataque.add( noturnoVivo1 );
        ataque.add( noturnoVivo2 );
        ataque.add( noturnoVivo3 );
        ataque.add( noturnoVivo4 );
        ataque.add( noturnoVivo5 );
        ataque.add( noturnoMorto );
        ataque.add( verdeVivo1 );
        ataque.add( verdeVivo2 );
        ataque.add( verdeVivo3 );
        ataque.add( verdeVivo4 );
        ataque.add( verdeMorto );
        ataqueOrdenado.add( verdeVivo3 );
        ataqueOrdenado.add( verdeVivo2 );
        ataqueOrdenado.add( noturnoVivo3 );
        Dwarf dwarf = new Dwarf ("Gimli");
        assertEquals ( ataqueOrdenado, estrategia.estrategia( ataque ) );        
    }
}
