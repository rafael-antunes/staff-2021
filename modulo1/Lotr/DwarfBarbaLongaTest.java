import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class DwarfBarbaLongaTest
{
    @Test
    public void tira1NoDadoETomaDano () {
        DadoFalso dado = new DadoFalso();
        dado.simularValor(1);
        DwarfBarbaLonga dwarf = new DwarfBarbaLonga ( "Barbalonga", dado );
        dwarf.sofrerDano();
        assertEquals( 100.0, dwarf.getHP() );
        assertEquals( Status.SOFREU_DANO, dwarf.getStatus() );        
    }
    
    @Test
    public void tira6NoDadoENaoTomaDano () {
        DadoFalso dado = new DadoFalso();
        dado.simularValor(6);
        DwarfBarbaLonga dwarf = new DwarfBarbaLonga ( "Barbalonga", dado );
        dwarf.sofrerDano();
        assertEquals( 110.0, dwarf.getHP() );
        assertEquals( Status.RECEM_CRIADO, dwarf.getStatus() );        
    }
}
