

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class DwarfTest{
    @Test
    public void dwarfDeveNascerCom110DeVida() {
        Dwarf dwarf = new Dwarf ( "Gimli" );
        assertEquals( 110.0, dwarf.getHP() );
        assertEquals( Status.RECEM_CRIADO, dwarf.getStatus() );
    }
    
    @Test
    public void dwarfSofreDanoEFicaCom100DeVida() {
        Dwarf dwarf = new Dwarf ( "Gimli" );
        dwarf.sofrerDano();
        assertEquals( 100.0, dwarf.getHP() );
        assertEquals( Status.SOFREU_DANO, dwarf.getStatus() );
    }
    
    @Test
    public void dwarfSofreDanoEFicaCom105DeVidaPorqueTemEscudo() {
        Dwarf dwarf = new Dwarf ( "Gimli" );
        dwarf.equiparEDesequiparEscudo();
        dwarf.sofrerDano();
        assertEquals( 105.0, dwarf.getHP() );
        assertEquals( Status.SOFREU_DANO, dwarf.getStatus() );
    }
    
    @Test
    public void dwarfSofreDano12VEzesEFicaCom0DeVida() {
        Dwarf dwarf = new Dwarf ( "Gimli" );
        Item item = (dwarf.getInventario().getItem(0));
         for ( int i = 0; i < 12; i++){
            dwarf.sofrerDano();
        }
        assertEquals( 0.0, dwarf.getHP(), 0.00000001);
        assertEquals( Status.MORTO, dwarf.getStatus() );
    }
    
    @Test
    public void ganhaItem () {
        Dwarf dwarf = new Dwarf ( "Gimli" );
        Item espada = new Item ( 1, "Espada" );
        dwarf.ganharItem( espada );
        assertEquals ( espada ,dwarf.getInventario().getItem(1) );
    }
    
    @Test
    public void perdeItem () {
        Dwarf dwarf = new Dwarf ( "Gimli" );
        Item espada = new Item ( 1, "Espada" );
        dwarf.getInventario().addItem(espada);
        dwarf.perderItem (espada);
        assertEquals ( 1 ,dwarf.getInventario().getInventario().size() );
    }
}
