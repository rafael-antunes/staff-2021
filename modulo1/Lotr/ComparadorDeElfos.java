import java.util.*;

public class ComparadorDeElfos implements Comparator<Elfo> {
   
    public int compare ( Elfo elfoAtual, Elfo elfoProximo ) {
        boolean mesmoTipo = elfoAtual.getClass() == elfoProximo.getClass();
                
        if ( mesmoTipo ) return 0;
                
        return elfoAtual instanceof ElfoVerde && elfoProximo instanceof ElfoNoturno ? 
                                                                                -1 : 1;
    } 
}
