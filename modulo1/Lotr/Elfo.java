public class Elfo extends Personagem  {
    private int indiceFlecha;
    private static int qtdElfos = 0;
    
    {
        this.indiceFlecha =  1;
    }
    
    public Elfo ( String nome ) {
        super(nome);
        this.HP = 100.0;
        this.inventario.addItem(new Item( 1, "Arco" ));
        this.inventario.addItem(new Item( 2, "Flecha" ));
        Elfo.qtdElfos++;
    }
    
    public Item getFlecha() {
        return this.inventario.getItem(indiceFlecha);    
    }
    
    public int getQtdElfos() {
        return this.qtdElfos;
    }
    
    public void finalize() throws Throwable  {
        Elfo.qtdElfos--;
    }

    protected int getQtdFlecha() {
        return this.getFlecha().getQuantidade();
    }
    
    protected boolean podeAtirar() {
        return this.getQtdFlecha() > 0;
    }    
    
    public void atirarFlecha( Dwarf dwarf ) {
        if ( this.podeAtirar() ) {
            this.getFlecha().setQuantidade( this.getQtdFlecha() - 1 );
            this.aumentarXP();
            this.sofrerDano();
            dwarf.sofrerDano();
        }
    }
    
    public void atirarMultiplasFlechas( int vezes, Dwarf dwarf ) {
        for ( int i = 0; i < vezes; i++ ) {
            this.atirarFlecha(dwarf);
        }
    }
    
    public String imprimirNomeClasse() {
        return "Dwarf";
    }
}
