public class Dwarf extends Personagem {
    private boolean escudoEquipado;
    
    public Dwarf ( String nome ) {
        super (nome);
        this.HP = 110.0;
        this.inventario.addItem(new Item( 1, "Escudo" ));
        this.escudoEquipado = false;
        this.qtdDano = 10.0;
    }
    
    public void equiparEDesequiparEscudo() {
        this.escudoEquipado = !this.escudoEquipado;
        super.qtdDano = this.escudoEquipado ? 5.0 : 10.0;
    }
    
    public String imprimirNomeClasse() {
        return "Dwarf";
    }
}


