import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ElfoTest {
    
    @AfterEach
    public void tearDown() {
        System.gc();
    }
    
    @Test
    public void exemploMudancaValoresItem () {
        Elfo elfo = new Elfo( "Legolas" );
        Item flecha = elfo.getFlecha();
        flecha.setQuantidade(1000);
        assertEquals( 1000, flecha.getQuantidade() );
    }
    
    @Test
    public void elfoDeveNascerCom2Flechas () {
        Elfo elfo = new Elfo( "Legolas" );
        assertEquals( 2, elfo.getFlecha().getQuantidade() );
    }
    
    @Test 
    public void elfoAtiraFlechaPerdeUmaUnidadeGanhaXP() {
        Elfo elfo = new Elfo( "Legolas" );
        Dwarf dwarf = new Dwarf ( "Gimli" );
        elfo.atirarFlecha(dwarf);
        assertEquals( 1, elfo.getFlecha().getQuantidade() );
        assertEquals( 1, elfo.getExperiencia() );
    }
    
    @Test
    public void elftoAtiraTresFlechasTemDuas() {
        Elfo elfo = new Elfo( "Legolas" );
        Dwarf dwarf = new Dwarf ( "Gimli" );
        for( int i = 0; i < 3; i++ ) {
            elfo.atirarFlecha(dwarf);
        }
        assertEquals( 2, elfo.getExperiencia() );
        assertEquals( 0, elfo.getFlecha().getQuantidade() );
    }
    
    @Test
    public void elfoAtiraUmaFlechaEmDwarfQuePerde10HP() {
        Elfo elfo = new Elfo( "Legolas" );
        Dwarf dwarf = new Dwarf ( "Gimli" );
        elfo.atirarFlecha(dwarf);
        assertEquals( 1, elfo.getFlecha().getQuantidade() );
        assertEquals( 100, dwarf.getHP() );
    }
    
    @Test
    public void elfoAtiraUmaFlechaEmDwarfQuePerde5HPPorqueTemEscudo() {
        Elfo elfo = new Elfo( "Legolas" );
        Dwarf dwarf = new Dwarf ( "Gimli" );
        dwarf.equiparEDesequiparEscudo();
        elfo.atirarFlecha(dwarf);
        assertEquals( 1, elfo.getFlecha().getQuantidade() );
        assertEquals( 105, dwarf.getHP() );
    }
    
    @Test
    public void ganhaItem () {
        Elfo elfo = new Elfo ( "Legolas" );
        Item espada = new Item ( 1, "Espada" );
        elfo.ganharItem( espada );
        assertEquals ( espada ,elfo.getInventario().getItem(2) );
    }
    
    @Test
    public void perdeItem () {
        Elfo elfo = new Elfo ( "Legolas" );
        Item espada = new Item ( 1, "Espada" );
        elfo.getInventario().addItem(espada);
        elfo.perderItem (espada);
        assertEquals ( 2 ,elfo.getInventario().getInventario().size() );
    }
    
    @Test
    public void criaUmElfo () {
        Elfo elfo = new Elfo ( "Legolas" );
        assertEquals ( 1, elfo.getQtdElfos() );
    }
    
    @Test
    public void criaTresElfos () {
        ElfoNoturno dark = new ElfoNoturno( "Dark" );
        ElfoVerde green = new ElfoVerde( "Green" );
        assertEquals ( 2, green.getQtdElfos() );
    }
}
