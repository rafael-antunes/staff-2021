import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class EstatisicasInventarioTest
{
    @Test
    public void calcularMediaInventarioVazio () {
        Inventario inventario = new Inventario();   
        EstatisicasInventario estatisticas = new EstatisicasInventario(inventario);
        double resultado = estatisticas.calcularMedia();
        assertTrue ( Double.isNaN(resultado ) );
    }
    
    @Test
    public void calcularMediaUmItem () {
        Inventario inventario = new Inventario();
        inventario.addItem ( new Item ( 5 , "Flechas" ) );
        EstatisicasInventario estatisticas = new EstatisicasInventario(inventario);
        double resultado = estatisticas.calcularMedia();
        assertEquals( 5, resultado, 1e-8 );
    }
    
    @Test
    public void calcularMediaQtdsIguais () {
        Inventario inventario = new Inventario();
        inventario.addItem ( new Item ( 5 , "Flechas" ) );
        inventario.addItem ( new Item ( 5 , "Adagas" ) );
        EstatisicasInventario estatisticas = new EstatisicasInventario(inventario);
        double resultado = estatisticas.calcularMedia();
        assertEquals( 5, resultado, 1e-8 );
    }
    
    @Test
    public void calcularMediaQtdsDiferentes () {
        Inventario inventario = new Inventario();
        inventario.addItem ( new Item ( 5 , "Flechas" ) );
        inventario.addItem ( new Item ( 1 , "Arco" ) );
        inventario.addItem ( new Item ( 2 , "Adagas" ) );
        inventario.addItem ( new Item ( 12 , "Moedas" ) );
        EstatisicasInventario estatisticas = new EstatisicasInventario(inventario);
        double resultado = estatisticas.calcularMedia();
        assertEquals( 5, resultado, 1e-8 );
    }
    
    public void calcularMedianaUmItem() {
        Inventario inventario = new Inventario();
        inventario.addItem ( new Item ( 5 , "Flechas" ) );
        EstatisicasInventario estatisticas = new EstatisicasInventario(inventario);
        double resultado = estatisticas.calcularMediana();
        assertEquals( 5, resultado, 1e-8 );
    }
    
    @Test 
    public void calcularMedianaQtsImpares () {
        Inventario inventario = new Inventario();
        inventario.addItem ( new Item ( 5 , "Flechas" ) );
        inventario.addItem ( new Item ( 1 , "Arco" ) );
        inventario.addItem ( new Item ( 2 , "Adagas" ) );
        inventario.addItem ( new Item ( 12 , "Moedas" ) );
        inventario.addItem ( new Item ( 6 , "Racoes de viagem" ) );
        EstatisicasInventario estatisticas = new EstatisicasInventario(inventario);
        double resultado = estatisticas.calcularMediana();
        assertEquals( 5, resultado, 1e-8 );
    }
    
    @Test 
    public void calcularMedianaQtsPares () {
        Inventario inventario = new Inventario();
        inventario.addItem ( new Item ( 5 , "Flechas" ) );
        inventario.addItem ( new Item ( 1 , "Arco" ) );
        inventario.addItem ( new Item ( 2 , "Adagas" ) );
        inventario.addItem ( new Item ( 12 , "Moedas" ) );
        EstatisicasInventario estatisticas = new EstatisicasInventario(inventario);
        double resultado = estatisticas.calcularMediana();
        assertEquals( 3.5, resultado, 1e-8 );
    }
    
    @Test
    public void calcularQtdItensAcimaDaMediaUmItem () {
        Inventario inventario = new Inventario();
        inventario.addItem ( new Item ( 5 , "Flechas" ) );
        EstatisicasInventario estatisticas = new EstatisicasInventario(inventario);
        int resultado = estatisticas.qtdItensAcimaDaMedia();
        assertEquals( 0, resultado );
    }
    
    @Test
    public void calcularQtdItensAcimaDaMediaVariosItens () {
        Inventario inventario = new Inventario();
        inventario.addItem ( new Item ( 5 , "Flechas" ) );
        inventario.addItem ( new Item ( 1 , "Arco" ) );
        inventario.addItem ( new Item ( 2 , "Adagas" ) );
        inventario.addItem ( new Item ( 12 , "Moedas" ) );
        inventario.addItem ( new Item ( 6 , "Racoes de viagem" ) );
        EstatisicasInventario estatisticas = new EstatisicasInventario(inventario);
        int resultado = estatisticas.qtdItensAcimaDaMedia();
        assertEquals ( 2 , resultado  );
    }
    
     @Test
    public void calcularQtdItensAcimaDaMediaComItensIgualMedia () {
        Inventario inventario = new Inventario();
        inventario.addItem ( new Item ( 5 , "Flechas" ) );
        inventario.addItem ( new Item ( 1 , "Arco" ) );
        inventario.addItem ( new Item ( 2 , "Adagas" ) );
        inventario.addItem ( new Item ( 11 , "Moedas" ) );
        inventario.addItem ( new Item ( 6 , "Racoes de viagem" ) );
        EstatisicasInventario estatisticas = new EstatisicasInventario(inventario);
        int resultado = estatisticas.qtdItensAcimaDaMedia();
        assertEquals ( 2 , resultado  );
    }
}
