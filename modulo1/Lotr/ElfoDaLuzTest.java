import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ElfoDaLuzTest
{
    @Test
    public void atacaUmaVezPerde21HP () {
        ElfoDaLuz feanor = new ElfoDaLuz( "Feanor" );
        feanor.atacarComEspada(new Dwarf("Farlum"));
        assertEquals ( 79.0, feanor.HP, 1e-8 );
    }
    
    @Test 
    void atacaDuasVezesPerde21EGanha10HP () {
        ElfoDaLuz feanor = new ElfoDaLuz( "Feanor" );
        feanor.atacarComEspada(new Dwarf( "Farlum" ) );
        feanor.atacarComEspada(new Dwarf( "Gul" ) );
        assertEquals ( 89.0, feanor.HP, 1e-8 );
    }
    
    @Test
    void naoPerdeItemGalvorn () {
        ElfoDaLuz feanor = new ElfoDaLuz( "Feanor" );
        feanor.perderItem( new Item ( 1, "Espada de Galvorn" ) );
        assertEquals ( 3, feanor.inventario.getInventario().size() );
    }
}
