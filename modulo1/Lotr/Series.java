import java.util.*;

public class Series {
    
    public void imprimirSeries() {
        ArrayList<String> series = new ArrayList<>();
        
        series.add("Greys Anatomy");
        series.add("Vikings");
        series.add("The good place");
        series.add("Breaking Bad");
        series.add("Supernatural");
        series.add("Mr Robot");
        series.add("Happy");
        series.add("Prison break");
        series.add("Ozark");
        series.add("To Your Eternity");
        series.add("Game of Thrones");
        series.add("Haikyuu");
        series.add("The Witcher");
        series.add("The Office");
        series.add("Naruto");
        series.add("Peaky Blinders");
        
        /*System.out.println( series.get(7) );
        series.isEmpty();
        series.remove(9);
        series.remove(new Object("Peaky Blinders"));*/
        
        for( String serie : series ){
            System.out.println( serie );
        }
    }
    
}
