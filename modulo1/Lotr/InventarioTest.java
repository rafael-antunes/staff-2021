import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class InventarioTest
{
    @Test
    public void criarInventario () {
        Inventario inventario = new Inventario();
        assertEquals ( 0 , inventario.getInventario().size() );
    }
    
    @Test
    public void adicionarUmItemInventario () {
        Inventario inventario = new Inventario ();
        Item flechas = new Item ( 5 , "Flechas" );
        inventario.addItem ( flechas );
        assertEquals ( flechas , inventario.getInventario().get(0) );
    }
    
    public void adicionarDoisItensInventario() {
        Inventario inventario = new Inventario ();
        Item flechas = new Item ( 5 , "Flechas" );
        Item arco = new Item ( 1 , "Arco" );
        inventario.addItem ( flechas );
        inventario.addItem ( arco );
        assertEquals ( flechas , inventario.getItem(0));
        assertEquals ( arco , inventario.getItem(1) );
    }
    
    @Test
    public void getItemFunciona () {
        Inventario inventario = new Inventario ();
        Item flechas = new Item ( 5 , "Flechas" );
        inventario.addItem ( flechas );
        assertEquals ( inventario.getItem(0) , flechas );
    }
    
    @Test
    public void removerItemFunciona () {
        Inventario inventario = new Inventario ();
        Item flechas = new Item ( 5 , "Flechas" );
        inventario.addItem ( flechas );
        inventario.removeItem( 0 );
        assertEquals ( 0, inventario.getInventario().size() );
    }
    
    @Test
    public void getItemDeMaiorQuantidadeFunciona () {
        Inventario inventario = new Inventario ();
        Item flechas = new Item ( 5 , "Flechas" );
        Item arco = new Item ( 1 , "Arco" );
        Item adagas = new Item ( 2 , "Adagas" );
        Item moedas = new Item ( 12 , "Moedas" );
        inventario.addItem ( flechas );
        inventario.addItem ( arco );
        inventario.addItem ( adagas );
        inventario.addItem ( moedas );
        assertEquals ( inventario.qualItemTemMaiorQuantidade() , moedas );
    }
    
    @Test
    public void getDescricoesVariosItens() {
        Inventario inventario = new Inventario();
        Item espada = new Item( 1, "Espada" );
        Item escudo = new Item( 2, "Escudo" );
        inventario.addItem(espada);
        inventario.addItem(escudo);
        String resultado = inventario.getDescricoesItens();
        assertEquals( "Espada,Escudo", resultado );
    }
    
    @Test
    public void testarBuscaDeItem () {
        Inventario inventario = new Inventario ();
        Item flechas = new Item ( 5 , "Flechas" );
        Item arco = new Item ( 1 , "Arco" );
        Item adagas = new Item ( 2 , "Adagas" );
        Item moedas = new Item ( 12 , "Moedas" );
        inventario.addItem ( flechas );
        inventario.addItem ( arco );
        inventario.addItem ( adagas );
        inventario.addItem ( moedas );
        assertEquals ( inventario.buscarItem ( "Adagas" ) , adagas );
    }
    
    @Test
    public void testarInversao () {
        Inventario inventario = new Inventario ();
        Inventario invertido = new Inventario ();
        Item flechas = new Item ( 5 , "Flechas" ); 
        Item arco = new Item ( 1 , "Arco" ); 
        Item adagas = new Item ( 2 , "Adagas" );
        Item moedas = new Item ( 12 , "Moedas" );
        inventario.addItem ( flechas );
        inventario.addItem ( arco );
        inventario.addItem ( adagas );
        inventario.addItem ( moedas );
        invertido.addItem ( moedas );
        invertido.addItem ( adagas );
        invertido.addItem ( arco );
        invertido.addItem ( flechas );
        assertEquals ( inventario.inverter() , invertido.getInventario() );
    }
    
    @Test
    public void testarOrdemCrescente () {
        Inventario inventario = new Inventario ();
        Inventario crescente = new Inventario ();
        Item flechas = new Item ( 5 , "Flechas" ); 
        Item arco = new Item ( 1 , "Arco" ); 
        Item adagas = new Item ( 2 , "Adagas" );
        Item moedas = new Item ( 12 , "Moedas" );
        inventario.addItem ( flechas );
        inventario.addItem ( arco );
        inventario.addItem ( adagas );
        inventario.addItem ( moedas );
        crescente.addItem ( arco );
        crescente.addItem ( adagas );
        crescente.addItem ( flechas );
        crescente.addItem ( moedas );
        inventario.ordenarItens();
        assertEquals ( inventario.getInventario() , crescente.getInventario() );
    }
    
    @Test
    public void testarOrdemCrescenteComTipoOrdenacao () {
        Inventario inventario = new Inventario ();
        Inventario crescente = new Inventario ();
        Item flechas = new Item ( 5 , "Flechas" ); 
        Item arco = new Item ( 1 , "Arco" ); 
        Item adagas = new Item ( 2 , "Adagas" );
        Item moedas = new Item ( 12 , "Moedas" );
        inventario.addItem ( flechas );
        inventario.addItem ( arco );
        inventario.addItem ( adagas );
        inventario.addItem ( moedas );
        crescente.addItem ( arco );
        crescente.addItem ( adagas );
        crescente.addItem ( flechas );
        crescente.addItem ( moedas );
        TipoOrdenacao ordem = TipoOrdenacao.ASC;
        inventario.ordenarItens(ordem);
        assertEquals ( inventario.getInventario() , crescente.getInventario() );
    }
    
    @Test
    public void testarOrdemDecrescenteComTipoOrdenacao () {
        Inventario inventario = new Inventario ();
        Inventario decrescente = new Inventario ();
        Item flechas = new Item ( 5 , "Flechas" ); 
        Item arco = new Item ( 1 , "Arco" ); 
        Item adagas = new Item ( 2 , "Adagas" );
        Item moedas = new Item ( 12 , "Moedas" );
        inventario.addItem ( flechas );
        inventario.addItem ( arco );
        inventario.addItem ( adagas );
        inventario.addItem ( moedas );
        decrescente.addItem ( moedas );
        decrescente.addItem ( flechas );
        decrescente.addItem ( adagas );
        decrescente.addItem ( arco );
        TipoOrdenacao ordem = TipoOrdenacao.DESC;
        inventario.ordenarItens(ordem);
        assertEquals ( inventario.getInventario() , decrescente.getInventario() );
    }
}
