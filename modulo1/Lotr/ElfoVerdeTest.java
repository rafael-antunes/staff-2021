import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ElfoVerdeTest 
{
    @Test 
    public void elfoAtiraFlechaPerdeUmaUnidadeGanhaXP() {
        ElfoVerde elfo = new ElfoVerde ( "Verde" );
        Dwarf dwarf = new Dwarf ( "Gimli" );
        elfo.atirarFlecha(dwarf);
        assertEquals( 1, elfo.getFlecha().getQuantidade() );
        assertEquals( 2, elfo.getExperiencia() );    
    }
    
    @Test
    public void tentaGanharItemMasNaoPode () {
        ElfoVerde elfo = new ElfoVerde ( "Verde" );
        elfo.ganharItem ( new Item ( 2, "Espada de aco" ) );
        assertEquals ( 2, elfo.getInventario().getInventario().size() );
    }
    
    @Test
    public void tentaGanharItemEPode () {
        ElfoVerde elfo = new ElfoVerde ( "Verde" );
        elfo.ganharItem ( new Item ( 2, "Espada de aco valiriano" ) );
        assertEquals ( 3, elfo.getInventario().getInventario().size() );
    }
}
