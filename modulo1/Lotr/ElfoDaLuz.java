import java.util.*;

public class ElfoDaLuz extends Elfo {
    private final double QTD_VIDA_GANHA = 10;
    private int qtdAtaques;
    
    private final static ArrayList<String> DESCRICOES_VALIDAS = new ArrayList<> (
        Arrays.asList(
            "Espada de Galvorn"
        )
    );
    
    {
        this.qtdAtaques = 0;
    }
    
    public ElfoDaLuz( String nome ) {
        super(nome);
        qtdDano = 21;
        super.ganharItem( new ItemSempreExistente ( 1, DESCRICOES_VALIDAS.get(0) ) );
        this.qtdAtaques = 0;
    }
    
    @Override
    public void perderItem ( Item item ) {
        if ( !item.getDescricao().equals( DESCRICOES_VALIDAS.get(0) ) ) {
            this.inventario.removeItem( item );
        }
    }
    
    private boolean devePerderVida() {
        return qtdAtaques % 2 == 1;
    }
    
    private Item getEspada() {
        return this.getInventario().buscarItem( DESCRICOES_VALIDAS.get(0) );
    }
    
    private void ganharVida() {
        super.HP += QTD_VIDA_GANHA;
    }

    public void atacarComEspada ( Dwarf dwarf ) {
        Item espada = getEspada();
        if ( espada.getQuantidade() > 0 ) {
            qtdAtaques++;
            dwarf.sofrerDano();
            if( this.devePerderVida() ) {
                sofrerDano();
            } else {
                ganharVida();
            }
        }
    }
}
