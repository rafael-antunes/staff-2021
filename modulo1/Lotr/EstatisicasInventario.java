import java.util.*;

public class EstatisicasInventario {
    private Inventario inventario;
    
    public EstatisicasInventario( Inventario inventario ) {
        this.inventario = inventario ;
    }
    
    private boolean EVazio() {
        return this.inventario.getInventario().isEmpty();
    }
    
    public double calcularMedia () {
        if ( this.EVazio() ){
            return Double.NaN;
        }
        
        double soma = 0;
        for ( Item item : this.inventario.getInventario() ) {
            soma += item.getQuantidade();
        }
        return soma/this.inventario.getInventario().size();
    }
    
    public double calcularMediana () {
        if ( this.EVazio() ) {
            return Double.NaN;
        }
        
        this.inventario.ordenarItens();
        int qtdItens = this.inventario.getInventario().size();
        int meio = qtdItens/2;
        int qtdMeio = this.inventario.getItem( meio ).getQuantidade();
        boolean qtdImpar = qtdItens % 2 == 1;
        if ( qtdImpar ) {
            return qtdMeio;
        }
        
        int qtdMeioMenosUm = this.inventario.getItem( meio -1 ).getQuantidade();
        return ( qtdMeio + qtdMeioMenosUm ) / 2.0;
    }
    
    public int qtdItensAcimaDaMedia () {
        int qtd = 0;
        double media = this.calcularMedia();
        for ( Item item : this.inventario.getInventario()  ) {
            if ( item.getQuantidade() > media ) {
                qtd++;
            }           
        }
        return qtd;
    }
}
