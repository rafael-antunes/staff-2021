import java.util.*;

public class ElfoVerde extends Elfo
{
    private final static ArrayList<String> DESCRICOES_VALIDAS = new ArrayList<> (
        Arrays.asList(
            "Espada de aco valiriano",
            "Arco de vidro",
            "Flecha de vidro"
        )
    );
    
    public ElfoVerde( String nome ) {
        super(nome);
        super.qtdExperienciaPorAtaque = 2;
    }
    
    private boolean isDescricaoValida ( String descricao ) {
        return DESCRICOES_VALIDAS.contains( descricao );
    }
    
    public void ganharItem ( Item item ) {
        if ( isDescricaoValida ( item.getDescricao() ) ) {
            super.inventario.addItem( item );
        }
    }
    
    public void perderItem ( Item item ) {
        if ( isDescricaoValida ( item.getDescricao() ) ) {
            super.inventario.removeItem( item );
        }
    }
}
