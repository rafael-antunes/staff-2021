import java.util.*;

public class NoturnosPorUltimo implements Estrategia {
    //https://docs.oracle.com/javase/8/docs/api/java/util/Comparator.html
    private ArrayList<Elfo> collections( ArrayList<Elfo> elfos ) {
        Collections.sort( elfos, new ComparadorDeElfos() );
        return elfos;
    }
    
    public ArrayList<Elfo> estrategia( ArrayList<Elfo> atacantes ) {
        return collections ( atacantes );
    }
}
