import java.util.*;

public class AtaqueIntercalado implements Estrategia {
    private static ArrayList<Status> STATUS_VIVOS = new ArrayList<> (
        Arrays.asList(
            Status.RECEM_CRIADO,
            Status.SOFREU_DANO
        )
    );
    
    private boolean estaVivo ( Elfo elfo ) {
        return STATUS_VIVOS.contains( elfo.getStatus() );
    }
    
    public ArrayList<Elfo> estrategia ( ArrayList<Elfo> atacantes ) {
        ArrayList<Elfo> ordemDeAtaque = new ArrayList<>();
        ArrayList<Elfo> atacantesTemporarios = new ArrayList<>();
        int qtdElfosVerdes = 0, qtdElfosNoturnos = 0, contador, menorQtd, qtdTotal;
        for ( Elfo elfo : atacantes ) {
            if ( estaVivo( elfo ) ) {
                atacantesTemporarios.add( elfo );
            }
        }
        for (Elfo elfo : atacantesTemporarios ) {
            if ( elfo.getClass() == ElfoVerde.class ) {
                qtdElfosVerdes++;
            }
            if ( elfo.getClass() == ElfoNoturno.class ) {
                qtdElfosNoturnos++;
            }
        }
        menorQtd = qtdElfosVerdes < qtdElfosNoturnos ? qtdElfosVerdes : qtdElfosNoturnos;
        qtdTotal = menorQtd * 2;
        qtdElfosVerdes = 0;
        qtdElfosNoturnos = 0;
        
        for ( int i = 0; i < qtdTotal; i++) {
            for (Elfo elfo : atacantesTemporarios ) {
                if (ordemDeAtaque.size() == 0) {
                    ordemDeAtaque.add(elfo);
                    atacantesTemporarios.remove(elfo);
                    if ( elfo.getClass() == ElfoVerde.class )  {
                        qtdElfosVerdes++;
                        break;
                    }
                    if ( elfo.getClass() == ElfoNoturno.class ) {
                        qtdElfosNoturnos++;
                        break;
                    }
                }
                if (ordemDeAtaque.get( ordemDeAtaque.size()-1 ).getClass() == ElfoNoturno.class ) {
                    if ( elfo.getClass() == ElfoVerde.class && qtdElfosVerdes <=  menorQtd ) {
                        ordemDeAtaque.add(elfo);
                        atacantesTemporarios.remove(elfo);
                        qtdElfosVerdes++;
                        break;
                    }
                }
                if (ordemDeAtaque.get( ordemDeAtaque.size()-1 ).getClass() == ElfoVerde.class ) {
                    if ( elfo.getClass() == ElfoNoturno.class && qtdElfosNoturnos <=  menorQtd ) {
                        ordemDeAtaque.add(elfo);
                        atacantesTemporarios.remove(elfo);
                        qtdElfosNoturnos++;
                        break;                            
                    }
                }
            }
        }
                   
        return ordemDeAtaque;
    }
}
