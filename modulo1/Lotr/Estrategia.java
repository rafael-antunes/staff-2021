import java.util.*;

public interface Estrategia
{
    ArrayList<Elfo> estrategia( ArrayList<Elfo> atacantes );
}
