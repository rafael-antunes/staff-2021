import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.*;

public class PaginadorInventarioTest
{
    @Test
    public void pularLimitarComApenasUmItem () {
        Inventario inventario = new Inventario ();
        Item espada = new Item ( 1, "Espada" );
        inventario.addItem( espada );
        PaginadorInventario paginador = new PaginadorInventario ( inventario );
        paginador.pular(0);
        ArrayList<Item> primeiraPagina = paginador.limitar(1);
        assertEquals( espada, primeiraPagina.get(0) );
        assertEquals( 1, primeiraPagina.size() );
    }
    
    @Test
    public void pularLimitarComDentroDosLimites () {
        Inventario inventario = new Inventario ();
        Item espada = new Item ( 1, "Espada" );
        Item escudo = new Item ( 2, "Escudo" );
        Item lanca = new Item ( 2, "Lança" );
        inventario.addItem( espada );
        inventario.addItem( escudo );
        inventario.addItem( lanca );
        PaginadorInventario paginador = new PaginadorInventario ( inventario );
        
        paginador.pular(0);
        ArrayList<Item> primeiraPagina = paginador.limitar(2);
        
        paginador.pular(2);
        ArrayList<Item> segundaPagina = paginador.limitar(1);
        
        assertEquals( espada, primeiraPagina.get(0) );
        assertEquals( escudo, primeiraPagina.get(1) );
        assertEquals( 2, primeiraPagina.size() );
        
        assertEquals( lanca, segundaPagina.get(0) );
        assertEquals( 1, segundaPagina.size() );
    }
    
     @Test
    public void pularLimitarComForaDosLimites () {
        Inventario inventario = new Inventario ();
        Item espada = new Item ( 1, "Espada" );
        Item escudo = new Item ( 2, "Escudo" );
        Item lanca = new Item ( 2, "Lança" );
        inventario.addItem( espada );
        inventario.addItem( escudo );
        inventario.addItem( lanca );
        PaginadorInventario paginador = new PaginadorInventario ( inventario );
        
        paginador.pular(0);
        ArrayList<Item> primeiraPagina = paginador.limitar(2);
        
        paginador.pular(2);
        ArrayList<Item> segundaPagina = paginador.limitar(1000);
        
        assertEquals( espada, primeiraPagina.get(0) );
        assertEquals( escudo, primeiraPagina.get(1) );
        assertEquals( 2, primeiraPagina.size() );
        
        assertEquals( lanca, segundaPagina.get(0) );
        assertEquals( 1, segundaPagina.size() );
    }
}
