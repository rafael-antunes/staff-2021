public abstract class Personagem {
    protected String nome;
    protected double HP, qtdDano;
    protected int experiencia, qtdExperienciaPorAtaque;
    protected Inventario inventario = new Inventario ();
    protected Status status;
    
    {
        this.status = Status.RECEM_CRIADO;
        this.inventario = new Inventario ();
        this.qtdExperienciaPorAtaque = 1;
        this.experiencia = 0;
        this.qtdDano = 0.0;
    }
    
    public Personagem ( String nome ) {
        this.nome = nome;
    }
    
    //get & set
    public String getNome() {
        return this.nome;
    }
    
    public void setNome( String nome ) {
        this.nome = nome;
    }
    
    public Inventario getInventario () {
        return this.inventario;
    }
    
    public int getExperiencia() {
        return this.experiencia;
    }
    
    public double getHP() {
        return this.HP;
    }
    
    public Status getStatus() {
        return this.status;
    }
    
    public void setStatus( Status status ) {
        this.status = status;
    }
    
    public void ganharItem ( Item item ) {
        this.inventario.addItem( item );
    }
    
    public void perderItem ( Item item ) {
        this.inventario.removeItem( item );
    }
    
    protected void aumentarXP() {
        this.experiencia += qtdExperienciaPorAtaque;
    } 
    
    protected boolean podeSofrerDano() {
        return this.HP > 0;
    }
    
    private Status validacaoStatus() {
        return this.HP == 0 ? Status.MORTO : Status.SOFREU_DANO;
    }
    
    public void sofrerDano() {
        if ( this.podeSofrerDano() ){
            this.HP -= this.HP >= qtdDano ? this.qtdDano : 0.0;
            this.status = this.validacaoStatus();
        }
    }
    
    public abstract String imprimirNomeClasse();
}
