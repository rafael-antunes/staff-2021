import java.util.*;

public class ApenasUmTerco implements Estrategia {
    private static ArrayList<Status> STATUS_VIVOS = new ArrayList<> (
        Arrays.asList(
            Status.RECEM_CRIADO,
            Status.SOFREU_DANO
        )
    );
    
    private boolean estaVivo ( Elfo elfo ) {
        return STATUS_VIVOS.contains( elfo.getStatus() );
    }
    
    private boolean temFlecha ( Elfo elfo ) {
        return elfo.getFlecha().getQuantidade() > 0;
    }
    
    private ArrayList<Elfo> ordenarPorQtdFlechas ( ArrayList<Elfo> atacantes ) {
        for ( int i = 0; i < atacantes.size(); i++ ){
            for ( int j = 0; j < atacantes.size() - 1; j++ ) {
                Elfo atual = atacantes.get(j);
                Elfo proximo = atacantes.get(j+1);
                boolean deveTrocar = atual.getFlecha().getQuantidade() < proximo.getFlecha().getQuantidade();
                if ( deveTrocar ) {
                    Elfo elfoTrocado = atual;
                    atacantes.set(j, proximo);
                    atacantes.set(j + 1, elfoTrocado);
                }
            }
        }
        return atacantes;
    }
    
    public ArrayList<Elfo> estrategia( ArrayList<Elfo> atacantes ) {
        ArrayList<Elfo> ordemDeAtaque = new ArrayList<>();
        ArrayList<Elfo> atacantesTemporarios = new ArrayList<>();
        for ( Elfo elfo : atacantes ) {
            if ( temFlecha( elfo ) && estaVivo( elfo ) ) {
                atacantesTemporarios.add( elfo );
            }
        }
        atacantesTemporarios = ordenarPorQtdFlechas( atacantesTemporarios );
        int numeroMaximo = atacantesTemporarios.size()/3;
        int contadorNoturnos = 1;
        int contadorVerdes = 0;
        
        for ( int i = 0; i < numeroMaximo; i++ ) {
            if ( atacantesTemporarios.size() > 0 ) {
                for ( Elfo elfo : atacantesTemporarios ) {
                    if ( elfo.getClass() == ElfoNoturno.class && contadorNoturnos == 0 )  {
                        ordemDeAtaque.add(elfo);
                        atacantesTemporarios.remove(elfo);
                        contadorNoturnos++;
                        break;
                    }
                    if ( elfo.getClass() == ElfoNoturno.class && contadorNoturnos != 0 ) {
                        contadorNoturnos++;
                    }
                    if ( elfo.getClass() == ElfoVerde.class ) {
                        ordemDeAtaque.add(elfo);
                        atacantesTemporarios.remove(elfo);
                        contadorVerdes++;
                        if ( contadorVerdes == 2 ) {
                            contadorNoturnos = 0;
                            contadorVerdes = 0;
                        }
                        break;
                    }
                }
            }
        }   
        return ordemDeAtaque;
    }
}