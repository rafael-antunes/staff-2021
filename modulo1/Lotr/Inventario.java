import java.util.*;

public class Inventario {
    private ArrayList<Item> inventario;
    
    public Inventario() {
        this.inventario = new ArrayList<>();
    }
    
    public ArrayList<Item> getInventario(){
        return this.inventario;
    }
    
    //metodos
    public void addItem ( Item item ){
        this.inventario.add( item );
    }
    
    public boolean validaSePosicao ( int posicao ) {
        return posicao <= this.inventario.size();
    }
        
    public Item getItem ( int posicao ){
        return this.validaSePosicao(posicao) ? this.inventario.get(posicao) : null ;        
    }
    
    public void removeItem ( int posicao ) {
        if (validaSePosicao(posicao)){
            this.inventario.remove(posicao);
        }
    }
    
    public void removeItem ( Item item ) {
        this.inventario.remove( item );
    }

    public String getDescricoesItens(){
        StringBuilder descricoes = new StringBuilder();
        for( Item item : inventario ) {
            descricoes.append(item.getDescricao());
            if ( item != this.getItem(inventario.size() - 1 ) ){
                descricoes.append(",");                
            }
        }
        return descricoes.toString();
    }
    
    public Item qualItemTemMaiorQuantidade () {
        int maiorNumero = 0, indice = 0;
        for ( Item item : inventario ){
            int qtdAtual = item.getQuantidade();
            if ( qtdAtual > maiorNumero ){
                maiorNumero = qtdAtual;
                indice = inventario.indexOf(item);
            }
        }
        //Ternario -> comparacao ? verdadeiro : falso
        return this.inventario.size() > 0 ? this.getItem(indice) : null;
    }
    
    public Item buscarItem(String nome) {
        int indice = inventario.size() + 1;
        for ( Item item : inventario ){
            if ( this.eNomeDoItem( item, nome ) ){
                indice = inventario.indexOf(item);
            }
        }
        return indice > this.inventario.size() ? null : inventario.get(indice);
    }
    
    public boolean eNomeDoItem (Item item, String nome) {
        return item.getDescricao() == nome;
    }
    
    public ArrayList inverter (){
        ArrayList<Item> invertido = new ArrayList<>();
        for ( int i = inventario.size() - 1; i >= 0; i-- ){
            Item temporario = this.inventario.get(i);
            invertido.add(temporario);
        }        
        return invertido;
    }
    
    public void ordenarItens() {
        this.ordenarItens( TipoOrdenacao.ASC );
    }
    
    public void ordenarItens( TipoOrdenacao ordenacao ) {
        for ( int i = 0; i < this.inventario.size(); i++ ){
            for ( int j = 0; j < this.inventario.size() - 1; j++ ) {
                Item atual = this.inventario.get(j);
                Item proximo = this.inventario.get(j+1);
                boolean deveTrocar = ordenacao == TipoOrdenacao.ASC ?
                (atual.getQuantidade() > proximo.getQuantidade() ) :
                (atual.getQuantidade() < proximo.getQuantidade() );
                if ( deveTrocar ) {
                    Item itemTrocado = atual;
                    this.inventario.set(j, proximo);
                    this.inventario.set(j + 1, itemTrocado);
                }
            }
        }
    }
}