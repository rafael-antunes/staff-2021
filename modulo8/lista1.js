let circulo = {
    raio: 3,
    tipoCalculo: "A"
};

function calcularCirculo ({ raio, tipoCalculo:tipo }) {
    return Math.ceil( circulo.tipo == "A" ? Math.PI * Math.pow( circulo.raio, 2 ) : 2 * Math.PI * circulo.raio );
}

console.log(calcularCirculo(circulo));


/** -----------------------Exercicio 2-------------------- */

function bissexto ( ano ) {
    return (ano % 400 == 0 ) || ( ano % 4 == 0 && ano % 100 != 0 );
}

console.log(bissexto( 2016 ));
console.log(bissexto( 2017 ));

/** -----------------------Exercicio 3-------------------- */

function somarPares ( numeros ) {
    let soma = 0;
    for (let i = 0; i < numeros.length; i+=2) {
        soma += numeros[i];
    }
    return soma;
}

console.log(somarPares([1, 56, 4.34, 6, -2]));


/** -----------------------Exercicio 4-------------------- */

// function adicionar ( valor1 ) {
//     return function ( valor2 ) {
//         return valor1 + valor2;   
//     }
// }

let adicionar = valor1 => valor2 => valor1 + valor2;

console.log(adicionar(3)(4));

/** -----------------------Exercicio Extra------------------ */

//currying
//2 -> 4,6,8,10

// let divisivel = ( divisor, numero ) => ( numero / divisor );
// const divisor = 2;
// console.log( divisivel( divisor, 4 ) );
// console.log( divisivel( divisor, 6 ) );
// console.log( divisivel( divisor, 8 ) );
// console.log( divisivel( divisor, 10 ) );

let divisao =  divisor => numero => ( numero / divisor );
const divisivelPor = divisao(2);

console.log( divisivel( 4 ) );
console.log( divisivel( 6 ) );
console.log( divisivel( 8 ) );
console.log( divisivel( 10 ) );