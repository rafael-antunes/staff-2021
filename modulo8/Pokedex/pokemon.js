class Pokemon { // eslint-disable-line no-unused-vars
  constructor( objDaApi ) {
    this._numero = objDaApi.id;
    this._nome = objDaApi.name;
    this._imagem = objDaApi.sprites.front_default;
    this._altura = objDaApi.height;
    this._tipos = objDaApi.types;
    this._estatisticas = objDaApi.stats;
    this._peso = objDaApi.weight;
    this.pegarNomesTipos();
    this.tratarEstatisticas();
  }

  get numero() {
    return this._numero;
  }

  get nome() {
    return ( this._nome.toUpperCase() );
  }

  get imagem() {
    return this._imagem;
  }

  get altura() {
    return `${ this._altura * 10 } cm`;
  }

  get tipos() {
    return this._tipos;
  }

  get estatisticas() {
    return this._estatisticas;
  }

  get peso() {
    return `${ this._peso / 10 } kg`;
  }

  pegarNomesTipos() {
    for ( let i = 0; i < this._tipos.length; i += 1 ) {
      this._tipos.splice( i, 1, this._tipos[i].type.name.toUpperCase() );
    }
  }

  tratarEstatisticas() {
    const novaArray = [];
    for ( let i = 0; i < this._estatisticas.length; i += 1 ) {
      novaArray.push( `${ this._estatisticas[i].stat.name.toUpperCase() }` );
      novaArray.push( `${ this._estatisticas[i].base_stat }` );
    }
    this._estatisticas = novaArray;
  }
}
