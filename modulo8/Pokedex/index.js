const pokeApi = new PokeApi();
const storage = localStorage;
let contador = 0;
let utilizado = 0;

function qualTipo( tipo ) {
  switch ( tipo ) {
    case 'NORMAL':
      return 'normal';
    case 'FIRE':
      return 'fire';
    case 'WATER':
      return 'water';
    case 'ELECTRIC':
      return 'electric';
    case 'GRASS':
      return 'grass';
    case 'ICE':
      return 'ice';
    case 'FIGHTING':
      return 'fighting';
    case 'POISON':
      return 'poison';
    case 'GROUND':
      return 'ground';
    case 'FLYING':
      return 'flying';
    case 'PSYCHIC':
      return 'psychic';
    case 'BUG':
      return 'bug';
    case 'ROCK':
      return 'rock';
    case 'GHOST':
      return 'ghost';
    case 'DRAGON':
      return 'dragon';
    case 'DARK':
      return 'dark';
    case 'STEEL':
      return 'steel';
    case 'FAIRY':
      return 'fairy';
    default:
      return 'none';
  }
}

const renderizar = ( pokemon ) => {
  const dadosPokemon1 = document.getElementById( 'dadosPokemon1' );
  const dadosPokemon2 = document.getElementById( 'dadosPokemon2' );

  dadosPokemon1.querySelector( '.numero' ).innerHTML = `No. ${ pokemon.numero }`;

  dadosPokemon1.querySelector( '.nome' ).innerHTML = pokemon.nome;

  dadosPokemon1.querySelector( '.thumb' ).src = pokemon.imagem;

  const altura = dadosPokemon2.querySelector( '.altura' );
  altura.innerHTML = `HT: ${ pokemon.altura }`;
  altura.className = 'altura tamanho-background';

  const peso = dadosPokemon2.querySelector( '.peso' );
  peso.innerHTML = `WT: ${ pokemon.peso }`;
  peso.className = 'peso tamanho-background';

  pokemon.tipos.forEach( ( element ) => {
    const tipo = document.createElement( 'li' );

    tipo.className = qualTipo( element );

    tipo.innerHTML = element;

    document.querySelector( '.tipo-list' ).appendChild( tipo );
  } )

  pokemon.estatisticas.forEach( ( element ) => {
    const estatistica = document.createElement( 'li' );

    estatistica.innerHTML = element;

    if ( pokemon.estatisticas.indexOf( element ) % 2 === 0 ) {
      document.querySelector( '.nome-list' ).appendChild( estatistica )
    } else {
      document.querySelector( '.valor-list' ).appendChild( estatistica )
    }
  } )
}

function limpar() {
  const dadosPokemon1 = document.getElementById( 'dadosPokemon1' );
  const dadosPokemon2 = document.getElementById( 'dadosPokemon2' );

  dadosPokemon1.querySelector( '.numero' ).innerHTML = '';

  dadosPokemon1.querySelector( '.nome' ).innerHTML = '';

  dadosPokemon1.querySelector( '.thumb' ).src = '';

  dadosPokemon2.querySelector( '.altura' ).innerHTML = '';
  dadosPokemon2.querySelector( '.altura' ).className = 'altura';

  dadosPokemon2.querySelector( '.peso' ).innerHTML = '';
  dadosPokemon2.querySelector( '.peso' ).className = 'peso';

  dadosPokemon2.querySelector( '.tipo-list' ).innerHTML = ''

  dadosPokemon2.querySelector( '.nome-list' ).innerHTML = ''

  dadosPokemon2.querySelector( '.valor-list' ).innerHTML = ''
}

function renderizarErro( erro ) {
  document.querySelector( '.nome' ).innerHTML = erro;
}

function renderizarMuitosNumerosGerados() {
  const dadosPokemmon1 = document.getElementById( 'dadosPokemon1' )

  dadosPokemmon1.querySelector( '.thumb' ).src = './Images/Poké_Ball_icon.svg'

  dadosPokemmon1.querySelector( '.numero' ).innerHTML = 'Favor limpar o armazenamento local'

  dadosPokemmon1.querySelector( '.nome' ).innerHTML = 'Pokémons sorteados mais de uma vez não são exibidos'
}

function buscarPokemon( input ) {
  if ( input !== utilizado ) {
    this.limpar();
    pokeApi.buscarEspecifico( input ).then( pokemon => {
      const poke = new Pokemon( pokemon );
      renderizar( poke );
    },
    ( ) => {
      renderizarErro( 'Pokémon não encontrado!' );
    } )
    utilizado = input;
  }
}

function estouComSorte() { // eslint-disable-line no-unused-vars
  const numero = Math.ceil( Math.random() * ( 893 - 0 ) + 0 );
  if ( storage.getItem( numero ) == null ) {
    buscarPokemon( numero );
    document.querySelector( '.form' ).value = numero;
    storage.setItem( numero, 'gerado' );
  } else {
    contador += 1;
    if ( contador > 30 ) {
      limpar();
      renderizarMuitosNumerosGerados();
    }
    console.log( contador )
  }
}
