class Jogador {
    constructor ( nome, numero ) {
        this._nome = nome;
        this._numero = numero;
    }

    get nome() {
        return this._nome;
    }

    get numero() {
        return this._numero;
    }
}

class Time {
    constructor ( nome, tipoEsporte, liga ) {
        this._nome = nome;
        this._tipoEsporte = tipoEsporte;
        this._status = "Ativo";
        this._liga = liga;
        this._jogadores = [];
    }

    adicionarJogador( jogador ) {
        this._jogadores.push( jogador );
    }

    buscarPorNome(nome) {
        return this._jogadores.find( jogador => jogador._nome == nome );
    }

    buscarPorNumero(numero) {
        return this._jogadores.find( jogador => jogador._numero == numero );
    }
    
}