import React, { Component } from 'react';

import ListaSeries from "../models/ListaSeries";

import './App.css';

export default class App extends Component {
  constructor( props ) {
    super( props );

    this.listaSeries = new ListaSeries();
    console.log(this.listaSeries.invalidas());
    console.log(this.listaSeries.filtrarPorAno( 2016));
    console.log(this.listaSeries.procurarPorNome( "Marcos" ));
    console.log(this.listaSeries.mediaDeEpisodios());
    console.log(this.listaSeries.totalSalarios( 0 ));
    console.log(this.listaSeries.queroGenero( 'Caos' ));
    console.log(this.listaSeries.queroTitulo( 'The' ));
    console.log(this.listaSeries.creditos( 0 ));
    console.log(this.listaSeries.hashtag());
  }
  
  render() {
    return (
      <div className="App">
        <div></div>
      </div>
      );
  }
}