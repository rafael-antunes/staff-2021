import React from 'react';
import ReactDOM from 'react-dom';
import Rotas from './Pages/Rotas';
import 'antd/dist/antd.css';

ReactDOM.render( <Rotas/>, document.getElementById('root') );

