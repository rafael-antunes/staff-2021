import React from 'react';

import Menu from '../Middleware/Menu/AntdMenuPrincipal';
export default function MenuPrincipal () {
  
  return (
    <Menu style={{ minHeight: '100%', width: '240px', padding: '10px'}}></Menu>
  )
}