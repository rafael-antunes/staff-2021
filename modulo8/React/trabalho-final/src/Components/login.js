import React from 'react';

export default function login ( props ) {

handleSubmit = (e) => {
  e.preventDefault();
  this.props.form.validateFields((err, values) => {
    if (!err) {
      console.log('Received values of form: ', values);
      window.location = 'dashboard';
    }
  });
};

render() {
  const { getFieldDecorator } = this.props.form;
  return (
    <React.Fragment>
      <section className="form-section default">
        <div className="content">
          <img src={logo} alt="" />
          <Form onSubmit={this.handleSubmit} className="login-form">
            <Form.Item>
              {getFieldDecorator('tipo', {
                rules: [
                  { required: true, message: 'Por favor, selecione seu perfil de acesso!' },
                ],
              })(
                <Select placeholder="Perfil de acesso" onChange={this.handleSelectChange}>
                  <Option value="admin">Administrador</Option>
                </Select>,
              )}
            </Form.Item>
            <Form.Item>
              {getFieldDecorator('usuario', {
                rules: [{ required: true, message: 'Por favor, insira seu usuário!' }],
              })(
                <Input
                  prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                  placeholder="Usuário"
                />,
              )}
            </Form.Item>
            <Form.Item>
              {getFieldDecorator('password', {
                rules: [{ required: true, message: 'Please input your Password!' }],
              })(
                <Input
                  prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                  type="password"
                  placeholder="Password"
                />,
              )}
            </Form.Item>
            <Form.Item>
              {getFieldDecorator('remember', {
                valuePropName: 'checked',
                initialValue: true,
              })(<Checkbox>Lembrar de mim</Checkbox>)}
              <Link className="login-form-forgot" to="/esqueci-minha-senha">
                Esqueci minha senha
              </Link>
              <Button type="primary" htmlType="submit" className="login-form-button">
                Log in
              </Button>
            </Form.Item>
          </Form>
        </div>
      </section>
    </React.Fragment>
  );
}
}