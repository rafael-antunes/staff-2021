import React from 'react';

import Layout from '../../Components/Middleware/Layout/AntdLayout';
import Content from '../../Components/Middleware/Layout/AntdContent';
import Header from '../../Components/Middleware/Layout/AntdHeader';
import Sider from '../../Components/Middleware/Layout/AntdSider'
import Menu from '../../Components/MenuPrincipal';
import Footer from '../../Components/Middleware/Layout/AntdFooter';

import './index.css'
import { Button } from 'antd';
import { Link } from 'react-router-dom';
import Path from '../../Constants/Path';

export default function LayoutPadrao ( props ) {
  const { children } = props;

  return (
    <Layout>
      <Header style={{ position: 'fixed', zIndex: 1, width: '100%' }}>
        <Button>
          <Link to={Path.APP}>Página Inicial</Link>
        </Button>
      </Header>
      <Layout className='site-layout'>
        <Sider style={{ height: '100%', position: 'fixed', left: 0, paddingTop: 64 }}>
          <Menu></Menu>
        </Sider>
        <Content style={{ minHeight: '100vh', marginLeft: '240px', marginTop: '64px', padding: 24, textAlign: 'center'  }} >
            <div className='site-layout-background'>
              { children }
            </div>
            
        </Content>
      </Layout>  
      <Footer></Footer>
    </Layout>
  )
}