import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const RotasPrivadas = ( { component: Component, ...resto } ) => { 
  console.log( localStorage.getItem('authorization') )
  return (
    <Route { ...resto } render={ props => (
      localStorage.getItem( 'authorization' ) ?
        <Component { ...props } /> :
        <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
    ) }
    />
  );
}

export { RotasPrivadas };