import React from 'react';

import Form from '../../Middleware/Form/AntdForm';
import Input from '../../Middleware/Form/AntdInput';
import Button from '../../Middleware/Form/AntdButton';
import FormItemInput from '../../Middleware/Form/AntdFormItemInput';
import Space from '../../Middleware/Layout/AntdSpace';
import Checkbox from '../../Middleware/Form/AntdCheckbox';
import FormItemButton from '../../Middleware/Form/AntdFormItemButton';
import FormItemCheckbox from '../../Middleware/Form/AntdFormItemCheckbox';


export default function FormularioEntradaSaida ( props ) {
  const { style, entrada } = props;

  return (
    <React.Fragment>
      <Form
        name="basic"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        initialValues={{ remember: true }}
        requiredMark={false}
        style={ style }
      >
        <Space align='center' direction='vertical'>
          <Space align='center' direction='vertical'>
          <FormItemInput 
            style={{ width: 400}}
            label="Número do cliente"
            name="clienteId"
            rules={[{ required: true, message: 'Campo obrigatório!' }]}
            >
              <Input password={false} />
          </FormItemInput>

          <FormItemInput
            style={{ width: 400}}
            label="Número da sala"
            name="espacoId"
            rules={[{ required: true, message: 'Campo obrigatório!' }]}
          >
            <Input password={false} />
          </FormItemInput>
          <Space align='center' direction='horizontal'>
            
          </Space>
        </Space>
        <Space align='baseline' direction='horizontal'>
          <FormItemCheckbox wrapperCol={{ offset: -16, span: 32}} valuePropName='checked' name='especial'>
            { entrada && (<Checkbox>Entrada especial</Checkbox>) }
          </FormItemCheckbox >         
          <FormItemButton wrapperCol={{ span: 32 }}>
            <Button type="primary" htmlType="submit" size='large'>{ `${ entrada ? 'Entrada' : 'Saída' }`}</Button>
          </FormItemButton>
        </Space>
        </Space>
      </Form>
    </React.Fragment>
  )
}