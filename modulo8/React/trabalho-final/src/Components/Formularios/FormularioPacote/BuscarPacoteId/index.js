import React from 'react';

import Form from '../../../Middleware/Form/AntdForm';
import Button from '../../../Middleware/Form/AntdButton';
import ItemInput from '../../../Middleware/Form/AntdFormItemInput';
import ItemButton from '../../../Middleware/Form/AntdFormItemButton';
import Space from '../../../Middleware/Layout/AntdSpace';
import InputNumber from '../../../Middleware/Form/AntdInputNumber';


export default function FormularioPacoteId ( props ) {
  const { style, onFinish } = props;

  return (
    <React.Fragment>
      <Form
        name="basic"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        initialValues={{ remember: true }}
        requiredMark={false}
        chamadaAPI={onFinish}
        style={ style }
      >
        <Space align='center' direction='horizontal'>
          
          <ItemInput
            label="Id"
            name="id"            
            style={{ width: 300 }}
            rules={[{ required: true, message: 'Campo obrigatório!' }]}
            >
              <InputNumber/>
          </ItemInput>
          <ItemButton
            wrapperCol={{ span: 16 }} >
            <Button type="primary" htmlType="submit">Submeter</Button>
          </ItemButton>
        </Space>        
      </Form>
    </React.Fragment>
  )
}