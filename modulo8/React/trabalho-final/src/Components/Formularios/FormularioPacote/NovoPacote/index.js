import React from 'react';

import Form from '../../../Middleware/Form/AntdForm';
import Input from '../../../Middleware/Form/AntdInput';
import Button from '../../../Middleware/Form/AntdButton';
import ItemInput from '../../../Middleware/Form/AntdFormItemInput';
import ItemButton from '../../../Middleware/Form/AntdFormItemButton';
import Space from '../../../Middleware/Layout/AntdSpace';
import InputNumber from '../../../Middleware/Form/AntdInputNumber';
import Option from '../../../Middleware/Form/AntdOption';
import Select from '../../../Middleware/Form/AntdSelect';


export default function FormularioNovoPacote ( props ) {
  const { style, onFinish } = props;

  return (
    <React.Fragment>
      <Form
        name="basic"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        initialValues={{ remember: true }}
        requiredMark={false}
        chamadaAPI={onFinish}
        style={ style }
      >
        <Space align='center' direction='vertical'>
          
          <ItemInput
            label="Espacos"
            name="espacos"
            help="Ids dos espacos contemplados pelo pacote, separados por virgulas apenas"
            style={{ width: 300 }}
            rules={[{ required: true, message: 'Campo obrigatório!' }]}
            >
              <Input/>
          </ItemInput>
          <ItemInput
            label="Quantidade"
            name="quantidade"
            style={{ width: 300 }}
            rules={[{ required: true, message: 'Campo obrigatório!' }]}
            >
              <InputNumber/>
          </ItemInput>
          <ItemInput
            label="Valor"
            name="valor"
            help="Apenas parte inteira"
            style={{ width: 300 }}
            rules={[{ required: true, message: 'Campo obrigatório!' }]}
          >
            <InputNumber/>
          </ItemInput>
          <ItemInput
            label="Prazo"
            name="prazo"
            style={{ width: 300 }}
            rules={[{ required: true, message: 'Campo obrigatório!' }]}
            >
              <InputNumber/>
          </ItemInput>
          <ItemInput
            label="Tipo de Contratação"
            name="tipoContratacao"
            style={{ width: 300 }}
            rules={[{ required: true, message: 'Campo obrigatório!' }]}
            >
              <Select placeholder='Tipo'>
                  <Option value='MINUTO'>Por minuto</Option>
                  <Option value='HORA'>Por hora</Option>
                  <Option value='TURNO'>Por turno</Option>
                  <Option value='DIARIA'>Diaria</Option>
                  <Option value='SEMANA'>Semanal</Option>
                  <Option value='MES'>Mensal</Option>
                </Select>
          </ItemInput>
          <ItemButton
            wrapperCol={{ span: 16 }} >
            <Button type="primary" htmlType="submit">Submeter</Button>
          </ItemButton>
        </Space>        
      </Form>
    </React.Fragment>
  )
}