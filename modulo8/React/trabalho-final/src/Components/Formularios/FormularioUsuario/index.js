import React from 'react';

import Form from '../../Middleware/Form/AntdForm';
import Input from '../../Middleware/Form/AntdInput';
import Button from '../../Middleware/Form/AntdButton';
import FormItemInput from '../../Middleware/Form/AntdFormItemInput';
import FormItemButton from '../../Middleware/Form/AntdFormItemButton';
import Space from '../../Middleware/Layout/AntdSpace';


export default function FormularioUsuario ( props ) {
  const { style, onFinish } = props;

  return (
    <React.Fragment>
      <Form
        name="basic"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        initialValues={{ remember: true }}
        requiredMark={false}
        chamadaAPI={onFinish}
        style={ style }
      >
        <Space align='center' direction='vertical'>
          <Space align='center' direction='vertical'>
            <Space align='center' direction='horizontal'>
              <FormItemInput
                label="Nome"
                name="nome"
                rules={[{ required: true, message: 'Campo obrigatório!' }]}
                >
                  <Input password={false} />
                </FormItemInput>

              <FormItemInput
                label="E-mail"
                name="email"
                rules={[{ required: true, message: 'Campo obrigatório!' }]}
              >
                <Input password={false} />
              </FormItemInput>
            </Space>
            <Space align='center' direction='horizontal'>
              <FormItemInput
                label="Login"
                name="login"
                rules={[{ required: true, message: 'Campo obrigatório!' }]}
                >
                  <Input password={false} />
              </FormItemInput>
              <FormItemInput
                label="Senha"
                name="senha"
                rules={[{ required: true, message: 'Campo obrigatório!' }]}
              >
                <Input password={false} />
              </FormItemInput>
            </Space>
          </Space>          
          <FormItemButton
            wrapperCol={{ offset: 8, span: 16 }} >
            <Button type="primary" htmlType="submit" nome="Login"/>
          </FormItemButton>
        </Space>        
      </Form>
    </React.Fragment>
  )
}