import React from 'react';

import Form from '../../../Middleware/Form/AntdForm';
import Input from '../../../Middleware/Form/AntdInput';
import Button from '../../../Middleware/Form/AntdButton';
import ItemInput from '../../../Middleware/Form/AntdFormItemInput';
import ItemButton from '../../../Middleware/Form/AntdFormItemButton';
import Space from '../../../Middleware/Layout/AntdSpace';
import InputNumber from '../../../Middleware/Form/AntdInputNumber';
import Option from '../../../Middleware/Form/AntdOption';
import Select from '../../../Middleware/Form/AntdSelect';


export default function FormularioNovoPacote ( props ) {
  const { style, onFinish } = props;

  return (
    <React.Fragment>
      <Form
        name="basic"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        initialValues={{ remember: true }}
        requiredMark={false}
        chamadaAPI={onFinish}
        style={ style }
      >
        <Space align='center' direction='vertical'>
          
          <ItemInput
            label="Espaco"
            name="espaco"
            help="Id do espaco contemplado pela contratação"
            style={{ width: 300 }}
            rules={[{ required: true, message: 'Campo obrigatório!' }]}
            >
              <Input/>
          </ItemInput>
          <ItemInput
            label="Cliente"
            name="cliente"
            style={{ width: 300 }}
            rules={[{ required: true, message: 'Campo obrigatório!' }]}
            >
              <Input/>
          </ItemInput>
          <ItemInput
            label="Quantidade"
            name="quantidade"
            style={{ width: 300 }}
            rules={[{ required: true, message: 'Campo obrigatório!' }]}
          >
            <InputNumber/>
          </ItemInput>
          <ItemInput
            label="Desconto"
            name="desconto"
            help="Apenas parte inteira"
            style={{ width: 300 }}
          >
            <InputNumber/>
          </ItemInput>
          <ItemInput
            label="Prazo"
            name="prazo"
            style={{ width: 300 }}
            rules={[{ required: true, message: 'Campo obrigatório!' }]}
          >
            <InputNumber/>
          </ItemInput>
          <ItemInput
            label="Forma de Pagamento"
            name="tipoPagamento"
            style={{ width: 300 }}
            rules={[{ required: true, message: 'Campo obrigatório!' }]}
            >
              <Select placeholder='Tipo'>
                  <Option value='DEBITO'>Débito</Option>
                  <Option value='CREDITO'>Crédito</Option>
                  <Option value='DINHEIRO'>Dinheiro</Option>
                  <Option value='TRANSFERENCIA'>Transferência</Option>
                </Select>
          </ItemInput>
          <ItemButton
            wrapperCol={{ span: 16 }} >
            <Button type="primary" htmlType="submit">Submeter</Button>
          </ItemButton>
        </Space>        
      </Form>
    </React.Fragment>
  )
}