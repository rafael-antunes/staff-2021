import React from 'react';

import Form from '../../Middleware/Form/AntdForm';
import Input from '../../Middleware/Form/AntdInput';
import Button from '../../Middleware/Form/AntdButton';
import FormItemInput from '../../Middleware/Form/AntdFormItemInput';
import FormItemButton from '../../Middleware/Form/AntdFormItemButton';


export default function FormularioLogin ( props ) {
  const { style, onFinish } = props;

  return (
    <React.Fragment>
      <Form
        name="basic"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        initialValues={{ remember: true }}
        requiredMark={false}
        chamadaAPI={onFinish}
        style={ style }
      >
          <FormItemInput
            label="Usuário"
            name="login"
            rules={[{ required: true, message: 'Campo obrigatório!' }]}
            >
              <Input password={false} />
            </FormItemInput>

          <FormItemInput
            label="Senha"
            name="senha"
            rules={[{ required: true, message: 'Campo obrigatório!' }]}
          >
            <Input password={true} />
          </FormItemInput>
          
          <FormItemButton
            wrapperCol={{ offset: 8, span: 16 }}
          >
            <Button type="primary" htmlType="submit">Login</Button>                  
          </FormItemButton>
      </Form>
    </React.Fragment>
  )
}