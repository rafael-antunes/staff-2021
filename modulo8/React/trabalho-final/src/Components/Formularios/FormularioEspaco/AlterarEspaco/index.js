import React from 'react';

import Form from '../../../Middleware/Form/AntdForm';
import Input from '../../../Middleware/Form/AntdInput';
import Button from '../../../Middleware/Form/AntdButton';
import ItemInput from '../../../Middleware/Form/AntdFormItemInput';
import ItemButton from '../../../Middleware/Form/AntdFormItemButton';
import Space from '../../../Middleware/Layout/AntdSpace';
import InputNumber from '../../../Middleware/Form/AntdInputNumber';


export default function FormularioAlterarEspaco ( props ) {
  const { style, onFinish, dados } = props;

  return (
    <React.Fragment>
      <Form
        name="basic"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        initialValues={{ remember: true }}
        requiredMark={false}
        chamadaAPI={onFinish}
        style={ style }
      >
        <Space align='center' direction='vertical'>
          <ItemInput
            label="Nome"
            name="nome"
            
            style={{ width: 300 }}
            rules={[{ required: true, message: 'Campo obrigatório!' }]}
            >
              <Input defaultValue={dados.nome}/>
          </ItemInput>

          <ItemInput
            label="Qtd de pessoas"
            name="qtdPessoas"
            style={{ width: 300 }}
            rules={[{ required: true, message: 'Campo obrigatório!' }]}
          >
            <InputNumber defaultValue={dados.qtdPessoas}/>
          </ItemInput>
          <ItemInput
            label="Valor"
            name="valor"
            help="Apenas parte inteira"
            style={{ width: 300 }}
            rules={[{ required: true, message: 'Campo obrigatório!' }]}
            >
              <Input defaultValue={dados.valor.replace( 'R$', '' ).replace( '.0', '' )}/>
          </ItemInput>
          <ItemButton
            wrapperCol={{ span: 16 }} >
            <Button type="primary" htmlType="submit">Submeter</Button>
          </ItemButton>
        </Space>        
      </Form>
    </React.Fragment>
  )
}