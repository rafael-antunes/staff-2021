import React from 'react';

import Form from '../../../Middleware/Form/AntdForm';
import Input from '../../../Middleware/Form/AntdInput';
import Button from '../../../Middleware/Form/AntdButton';
import ItemInput from '../../../Middleware/Form/AntdFormItemInput';
import ItemButton from '../../../Middleware/Form/AntdFormItemButton';
import Space from '../../../Middleware/Layout/AntdSpace';
import InputGroup from '../../../Middleware/Form/AntdInputGroup';
import Select from '../../../Middleware/Form/AntdSelect';
import Option from '../../../Middleware/Form/AntdOption';


export default function FormularioAlterarCliente ( props ) {
  const { style, onFinish } = props;

  return (
    <React.Fragment>
      <Form
        name="basic"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        initialValues={{ remember: true }}
        requiredMark={false}
        chamadaAPI={onFinish}
        style={ style }
      >
        <Space align='center' direction='vertical'>
          <ItemInput
            label="Identificação"
            name="clienteId"
            rules={[{ required: true, message: 'Campo obrigatório!'}]}
          >
            <Input/>
          </ItemInput>
          <ItemInput
            label="Nome"
            name="nome"
            rules={[{ required: true, message: 'Campo obrigatório!' }]}
            >
              <Input/>
          </ItemInput>

          <ItemInput
            label="CPF"
            name="cpf"
            help='14 números'
            rules={[{ required: true, message: 'Campo obrigatório!' }]}
          >
            <Input/>
          </ItemInput>
          <ItemInput
            label="Nascimento"
            name="dataNascimento"
            help='dd/mm/aaa'
            rules={[{ required: true, message: 'Campo obrigatório!' }]}
            >
              <Input/>
          </ItemInput>
          <ItemInput label='Contatos'>
            <InputGroup compact={true}>
              <ItemInput
                name={['Contato1', 'contato']}
                style={{width: '50%'}}
                rules={[{ required: true, message: 'Campo obrigatório!' }]}
              >
                <Input/>
              </ItemInput>
              <ItemInput
                name={['Contato1', 'tipoContato']}
                noStyle={true}
                rules={[{ required: true, message: 'Campo obrigatório' }]}
              >
                <Select placeholder='Tipo'>
                  <Option value='E-mail'>E-mail</Option>
                  <Option value='Telefone Fixo'>Telefone Fixo</Option>
                  <Option value='Celular'>Celular</Option>
                </Select>
              </ItemInput>
            </InputGroup>
            <InputGroup compact={true}>
              <ItemInput
                help={'Selecione dois tipos de contato diferentes'}
                name={['Contato2', 'contato']}
                style={{width: '50%'}}
                rules={[{ required: true, message: 'Campo obrigatório!' }]}
              >
                <Input/>
              </ItemInput>
              <ItemInput
                name={['Contato2', 'tipoContato']}
                noStyle={true}
                rules={[{ required: true, message: 'Campo obrigatório' }]}
              >
                <Select placeholder='Tipo'>
                  <Option value='E-mail'>E-mail</Option>
                  <Option value='Telefone Fixo'>Telefone Fixo</Option>
                  <Option value='Celular'>Celular</Option>
                </Select>
              </ItemInput>
            </InputGroup>
          </ItemInput>
          <ItemButton
            wrapperCol={{ offset: 8, span: 16 }} >
            <Button type="primary" htmlType="submit">Submeter</Button>
          </ItemButton>
        </Space>        
      </Form>
    </React.Fragment>
  )
}