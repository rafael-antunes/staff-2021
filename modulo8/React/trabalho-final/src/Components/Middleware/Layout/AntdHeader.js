import React from 'react';

import { Layout } from 'antd';

const { Header } = Layout;

export default function AntdHeader ( props ) {

  const { children, style } = props;

  return (
    <Header style={ style }>
      { children }
    </Header>
  )
}