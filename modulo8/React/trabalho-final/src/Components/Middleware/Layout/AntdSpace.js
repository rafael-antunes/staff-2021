import React from 'react';

import { Space } from 'antd';

export default function AntdSpace ( props ) {

  const { children, align, direction, size } = props;

  return (
    <Space align={ align } direction={ direction } size={ size }>
      { children }
    </Space>
  )
}