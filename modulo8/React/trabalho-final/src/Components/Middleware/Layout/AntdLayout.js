import React from 'react';

import { Layout } from 'antd';

export default function AntdLayout ( props ) {

  const { children } = props;

  return (
    <Layout>
      { children }
    </Layout>
  )
}