import React from 'react';

import { Row } from 'antd';

export default function AntdRow ( props ) {

  const { children, style } = props;

  return (
    <Row style={style}>
      { children }
    </Row>
  )
}