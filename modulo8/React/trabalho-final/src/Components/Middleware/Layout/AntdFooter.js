import React from 'react';

import { Layout } from 'antd';

const { Footer } = Layout;

export default function AntdFooter ( props ) {

  const { children } = props;

  return (
    <Footer position='fixed' style={{ textAlign: 'center' }}>
      { children }
    </Footer>
  )
}