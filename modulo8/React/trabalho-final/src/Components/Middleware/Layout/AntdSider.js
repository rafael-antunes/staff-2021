import React from 'react';

import { Layout } from 'antd';

const { Sider } = Layout;

export default function AntdSider ( props ) {

  const { children, style } = props;

  return (
    <Sider style={ style }>
      { children }
    </Sider>
  )
}