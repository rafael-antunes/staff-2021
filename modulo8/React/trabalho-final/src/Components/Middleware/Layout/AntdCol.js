import React from 'react';

import { Col } from 'antd';

export default function AntdCol ( props ) {

  const { children, span } = props;

  return (
    <Col span={span}>
      { children }
    </Col>
  )
}