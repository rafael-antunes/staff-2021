import React from 'react';

import { Layout } from 'antd';

const { Content } = Layout;

export default function AntdContent ( props ) {

  const { children, style } = props;

  return (
    <Content style={ style }>
      { children }
    </Content>
  )
}