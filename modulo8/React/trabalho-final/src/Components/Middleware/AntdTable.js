import React from 'react';

import { Table } from 'antd';

export default function AntdTable ( props ) {
  const { columns, data } = props

  return (
    <Table columns={columns} dataSource={data}/>
  )
}