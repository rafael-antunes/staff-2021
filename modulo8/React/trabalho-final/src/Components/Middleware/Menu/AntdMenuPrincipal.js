import React from 'react';
import { Link } from 'react-router-dom';

import { Menu } from 'antd';
import Path from '../../../Constants/Path';

const { SubMenu } = Menu;
 
export default function AntdMenuPrincipal ( props ) {
  
  const { style } = props;

  const handleClick = e => {
    console.log('click ', e);
  };
  
  return (
    <Menu
      onClick={handleClick}
      style={ style }
      defaultSelectedKeys={['']}
      defaultOpenKeys={['']}
      mode="inline"
    >
       <SubMenu key='sub1' title='Usuario'>
        <Menu.Item key='1.1'><Link to={Path.USUARIO}>Cadastrar novo usuário</Link></Menu.Item>
      </SubMenu>
      <SubMenu key='sub2' title='Acessos'>
        <Menu.Item key='2.1'><Link to={`${Path.ACESSOS}${Path.ENTRADAS}`}>Entradas</Link></Menu.Item>
        <Menu.Item key='2.2'><Link to={`${Path.ACESSOS}${Path.SAIDAS}`}>Saídas</Link></Menu.Item>
        <Menu.Item key= '2.3'><Link to={`${Path.ACESSOS}${Path.BUSCA}`}>Buscas</Link></Menu.Item>
      </SubMenu>
      <SubMenu key='sub3' title='Pacotes e contratações'>
        <SubMenu key='sub3.1' title='Pacotes'>
          <Menu.Item key='3.1.1'><Link to={`${Path.PACOTES}${Path.NOVO}`}>Novo pacote</Link></Menu.Item>
          <Menu.Item key='3.1.2'><Link to={`${Path.PACOTES}${Path.ALTERAR}`}>Alterar pacote</Link></Menu.Item>
          <Menu.Item key='3.1.3'><Link to={`${Path.PACOTES}${Path.BUSCA}`}>Buscas</Link></Menu.Item>
        </SubMenu>
        <SubMenu key='sub3.2' title='Contratações'>
          <Menu.Item key='3.2.1'><Link to={`${Path.CONTRATACOES}${Path.NOVO}`}>Nova contratação</Link></Menu.Item>
          <Menu.Item key='3.2.2'><Link to={`${Path.CONTRATACOES}${Path.BUSCA}`}>Buscas</Link></Menu.Item>
        </SubMenu>
        <Menu.Item key='3.3'><Link to={`${Path.PAGAMENTOS}`}>Realizar pagamentos</Link></Menu.Item>
        <Menu.Item key='3.3'><Link to={`${Path.PAGAMENTOS}${Path.BUSCA}`}>Todos pagamentos</Link></Menu.Item>
      </SubMenu>
      <SubMenu key='sub4' title='Clientes'>
        <Menu.Item key='4.1'><Link to={`${Path.CLIENTES}${Path.NOVO}`}>Cadastrar novo cliente</Link></Menu.Item>
        <Menu.Item key='4.2'><Link to={`${Path.CLIENTES}${Path.ALTERAR}`}>Alterar dados de cliente</Link></Menu.Item>
        <Menu.Item key='4.3'><Link to={`${Path.CLIENTES}${Path.SALDOS}`}>Saldos</Link></Menu.Item>
        <Menu.Item key='4.4'><Link to={`${Path.CLIENTES}${Path.BUSCA}`}>Buscas</Link></Menu.Item>
      </SubMenu>
      <SubMenu key='sub5' title='Espaços'>
        <Menu.Item key='5.1'><Link to={`${Path.ESPACOS}${Path.NOVO}`}>Adicionar espaços</Link></Menu.Item>
        <Menu.Item key='5.2'><Link to={`${Path.ESPACOS}${Path.ALTERAR}`}>Alterar informações de espaços</Link></Menu.Item>
        <Menu.Item key='5.3'><Link to={`${Path.ESPACOS}${Path.BUSCA}`}>Buscas</Link></Menu.Item>
      </SubMenu>
    </Menu>
  );

}