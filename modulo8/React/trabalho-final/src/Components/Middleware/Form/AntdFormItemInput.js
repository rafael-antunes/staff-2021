import React from 'react';

import { Form } from 'antd'

import 'antd/dist/antd.css'

export default function AntdFormItemInput ( props ) {
  const { children, label, name, rules, style, validateStatus, help, noStyle } = props;

  return(
      <Form.Item 
        label={label}
        name={name}
        rules={rules}
        style={style}
        noStyle={noStyle}
        validateStatus={validateStatus}
        help={help}
      >
        { children }
      </Form.Item>  
  )
}