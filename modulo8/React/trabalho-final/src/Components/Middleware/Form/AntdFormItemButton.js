import React from 'react';

import { Form } from 'antd'

import 'antd/dist/antd.css'

export default function AntdFormItemButton ( props ) {
  const { children, wrapperCol } = props;

  return (
    <React.Fragment>
      <Form.Item wrapperCol={wrapperCol}>
        {children}
      </Form.Item>
    </React.Fragment>
  )
}