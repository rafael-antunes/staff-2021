import React from 'react';

import { Form } from 'antd'

import 'antd/dist/antd.css'

export default function AntdForm ( props ) {
  const { children, name, labelCol, wrapperCol, initialValues, requiredMark, chamadaAPI } = props;

  const onFinish = (values) => {
    console.log('Success:', values);
    chamadaAPI( values );
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <React.Fragment>
      <Form
        name={name}
        labelCol={labelCol}
        wrapperCol={wrapperCol}
        initialValues={initialValues}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        requiredMark={requiredMark}
      >
        {children}
      </Form>
    </React.Fragment>
  )
}