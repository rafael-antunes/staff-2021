import React from 'react';

import { Select } from 'antd';

export default function AntdOption ( props ) {
  const { children, value } = props;

  return (
    <Select.Option vale={value}>
      {children}
    </Select.Option>
  )
}