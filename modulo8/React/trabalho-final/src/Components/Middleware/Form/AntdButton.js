import React from 'react';

import { Button } from 'antd'

export default function AntdButton ( props ) {
  const { children, type, htmlType } = props;
  
  return(
    <Button type={type} htmlType={htmlType} onClick={ props.onClick }>
        {children}
    </Button>
  )
}