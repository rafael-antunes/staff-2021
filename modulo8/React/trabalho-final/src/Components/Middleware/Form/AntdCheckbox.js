import React from 'react';

import { Checkbox } from 'antd';

export default function AntdCheckbox ( props ) {
  const { children } = props;
  
  return (
    <Checkbox onChange={props.onChange}>{children}</Checkbox>
  )
  
}
