import React from 'react';

import { Form } from 'antd'

import 'antd/dist/antd.css'

export default function AntdFormItemInput ( props ) {
  const { children, name, valuePropName, wrapperCol } = props;

  return(
      <Form.Item 
        name={name}
        valuePropName={valuePropName}
        wrapperCol={wrapperCol}
      >
        { children }
      </Form.Item>  
  )
}