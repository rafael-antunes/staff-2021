import React from 'react';

import { InputNumber } from 'antd';

export default function AntdInputNumber ( props ) {
  const { children, min, max, defaultValue } = props;

  return (
    <InputNumber min={min} max={max} onChange={ props.onChange } defaultValue={defaultValue}>
      { children }
    </InputNumber>
  )  
}