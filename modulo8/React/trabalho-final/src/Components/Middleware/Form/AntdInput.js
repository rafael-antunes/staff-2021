import React from 'react';

import { Input } from 'antd'

export default function AntdInput ( props ) {
  const { password, defaultValue } = props;

  return( 
    password ?
    <Input.Password onChange={ props.onChange } /> :
    <Input defaultValue={defaultValue} onChange={ props.onChange } />
  );  
}