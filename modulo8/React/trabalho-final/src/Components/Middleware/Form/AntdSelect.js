import React from 'react';

import { Select } from 'antd';

export default function AntdSelect ( props ) {
  const { children, placeholder, defaultValue } = props;

  return (
    <Select placeholder={placeholder} onChange={props.onChange} defaultValue={defaultValue}>
      {children}
    </Select>
  )
}