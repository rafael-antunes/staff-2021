import React from 'react';

import { Input } from 'antd'

export default function AntdInputGroup ( props ) {
  const { children, compact } = props;

  return( 
    <Input.Group compact={compact}>
      {children}
    </Input.Group>
  );  
}