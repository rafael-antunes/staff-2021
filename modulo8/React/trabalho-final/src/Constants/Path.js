const Path = {
  APP: '/',
  LOGIN: '/login',
  USUARIO: '/usuario',
  ACESSOS: '/acessos',
  PACOTES: '/pacotes',
  CLIENTES: '/clientes',
  ESPACOS: '/espacos',
  CONTRATACOES: '/contratacoes',
  ENTRADAS: '/entradas',
  SAIDAS: '/saidas',
  NOVO: '/novo',
  ALTERAR: '/alterar',
  BUSCA: '/busca',
  SALDOS: '/saldos',
  PAGAMENTOS: '/pagamentos',
  API: {
    API: '/api',
    CADASTRAR: '/cadastrar',
    LOGIN: '/login',
    SALVAR: '/salvar',
    EDITAR: '/editar',
    ESPACO: '/espacos',
    CLIENTE: '/cliente',
    ACESSO: '/acesso',
    REGISTRAR_ACESSO: "/registrarAcesso",
    PACOTE: '/pacote',
    ESPACO_PACOTE: '/espacoPacote',
    BUSCAR: '/buscar',
    POR_PACOTE: '/porPacote',
    CONTRATACAO: '/contratacao',
    PAGAMENTO: '/pagamento',
    SALDOS: '/saldoCliente',
    POR_CLIENTE: '/cliente'
  }
}

export default Path;