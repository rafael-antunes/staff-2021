import React, { Component } from 'react';
import CoworkingAPI from '../Models/CoworkingAPI';

import './App.css'
import LayoutPadrao from '../Components/LayoutPadrao';
import Row from '../Components/Middleware/Layout/AntdRow';
import Col from '../Components/Middleware/Layout/AntdCol';
import Button from '../Components/Middleware/Form/AntdButton';
import { Link } from 'react-router-dom';
import Path from '../Constants/Path';

export default class App extends Component {
  constructor ( props ) {
    super ( props );
    this.coworkingAPi = new CoworkingAPI();
  }

  render() {
    return(
      <LayoutPadrao>
        <Row style={{ paddingTop: '50px' }}>
          <Col span={8}>
            <Button>
              <Link to={Path.USUARIO}>Novo Usuário</Link>
            </Button>
          </Col>
          <Col span={8}>
            <Button>
              <Link to={Path.ACESSOS}>Acessos</Link>
            </Button>
          </Col>
          <Col span={8}>
            <Button>
              <Link to={Path.PACOTES}>Pacotes</Link>
            </Button>
          </Col>
        </Row>
        <Row style={{ paddingTop: '50px' }}>
          <Col span={8}>
            <Button>
              <Link to={Path.CLIENTES}>Clientes</Link>
            </Button>
          </Col>
          <Col span={8}>
            <Button>
              <Link to={Path.ESPACOS}>Espaços</Link>
            </Button>
          </Col>
          <Col span={8}>
            <Button>
              <Link to={Path.CONTRATACOES}>Contratações</Link>
            </Button>
          </Col>
        </Row>
        <Row style={{ paddingTop: '50px' }}>
          <Col span={8}>
          </Col>
          <Col span={8}>
          </Col>
          <Col span={8}>
            <Button>
              <Link to={Path.PAGAMENTOS}>Pagamentos</Link>
            </Button>
          </Col>
        </Row>
      </LayoutPadrao>
    );
  }
}
