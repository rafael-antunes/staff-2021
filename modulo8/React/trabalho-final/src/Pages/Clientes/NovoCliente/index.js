import React, { Component } from 'react';

import Layout from '../../../Components/LayoutPadrao';
import Formulario from '../../../Components/Formularios/FormularioCliente/NovoCliente'
import CoworkingAPI from '../../../Models/CoworkingAPI';

export default class NovoCliente extends Component {
  constructor ( props ) {
    super ( props );
    this.coworkingAPI = new CoworkingAPI();
  }

  novoCliente ( values ) {
    console.log(values)
    const tipoContato1 = { nome: values.Contato1.tipoContato };
    const tipoContato2 = { nome: values.Contato2.tipoContato };
    const primeiroContato = { valor: values.Contato1.valor, tipoContato: tipoContato1};
    const segundoContato = { valor: values.Contato2.valor, tipoContato: tipoContato2};
    values.contato = [ primeiroContato, segundoContato ];
    const valores = {
      nome: values.nome,
      cpf: values.cpf,
      dataDeNascimento: values.dataDeNascimento,
      contato: values.contato
    }
    console.log(valores)
    this.coworkingAPI.novoCliente( valores )
  }
  render() {
    return (
      <Layout>
        <h2>Novo Cliente</h2>
        <Formulario onFinish={ this.novoCliente.bind( this ) }></Formulario>
      </Layout>
    );
  }
}