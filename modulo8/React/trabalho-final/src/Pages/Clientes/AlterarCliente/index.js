import React, { Component } from 'react';

import Layout from '../../../Components/LayoutPadrao';
import Formulario from '../../../Components/Formularios/FormularioCliente/AlterarCliente'
import FormularioId from '../../../Components/Formularios/FormularioCliente/BuscarClienteId';
import CoworkingAPI from '../../../Models/CoworkingAPI';

export default class AlterarCliente extends Component {
  constructor ( props ) {
    super ( props );
    this.coworkingAPI = new CoworkingAPI();
    this.state = { dados: null }
  }

  componentDidUpdate() {
    console.log( this.state );
  }

  async buscarCliente( values ) {
    const response = await this.coworkingAPI.buscarClientePorId( values );
    response.data.id = values.id;
    this.setState({ dados: response.data } )
    console.log( this.state );
  }

  alterarCliente ( values ) {
    values.id = this.state.dados.id;
    this.coworkingAPI.alterarCliente( values );
  }

  render() {
    const { dados } = this.state;
    
    return (
      <Layout>
        <h2>Alterar Cliente</h2>
        <h3>Digite a identificação do cliente a ser alterado</h3>
        <FormularioId onFinish={ this.buscarCliente.bind( this ) }></FormularioId>
        {( dados !== null ) && 
          <Formulario onFinish={ this.alterarCliente.bind( this ) } dados={dados}></Formulario>
        }
        
      </Layout>
    );
  }
}