import React, { Component } from 'react';

import Layout from '../../Components/Middleware/Layout/AntdLayout';
import Content from '../../Components/Middleware/Layout/AntdContent';
import FormularioLogin from '../../Components/Formularios/FormularioLogin';
import Header from '../../Components/Middleware/Layout/AntdHeader';
import Space from '../../Components/Middleware/Layout/AntdSpace';
import CoworkingAPI from '../../Models/CoworkingAPI';

export default class Login extends Component {
  constructor ( props) {
    super( props );
    this.coworkingAPI = new CoworkingAPI();
  }

  render() {
    return(
      <Layout>
        <Header></Header>       
        <Content style={{ minHeight: '100vh', margin: '20vh auto' }} >
          <Space align='center' direction='vertical' >
            <h2>Login</h2>
            <FormularioLogin onFinish={ this.coworkingAPI.login }></FormularioLogin>
          </Space>                             
        </Content>       
      </Layout>
    )
  }
}