import React, { Component } from 'react';
import LayoutPadrao from '../../../Components/LayoutPadrao';
import AntdCol from '../../../Components/Middleware/Layout/AntdCol';
import AntdRow from '../../../Components/Middleware/Layout/AntdRow';
import CoworkingAPI from '../../../Models/CoworkingAPI';
import Formulario from '../../../Components/Formularios/FormularioPagamento';

export default class RealizarPagamento extends Component {
  constructor ( props ) {
    super ( props );
    this.coworkingAPI = new CoworkingAPI();
  }

  pagamentoContratacao( values ) {
    values.contratacaoId = values.id;
    this.coworkingAPI.novoPagamentoContratacao( values );
  }

  pagamentoPacote( values ) {
    values.pacoteId = values.id;
    this.coworkingAPI.novoPagamentoPacote( values );
  }

  render() {
    return(
      <LayoutPadrao>
        <AntdRow>
          <AntdCol span={12}>
            <h2>Pagamento de Contratacao</h2>
            <Formulario onFinish={ this.pagamentoContratacao.bind(this) } tipoPagamento={'da contratação'}></Formulario>
          </AntdCol>
          <AntdCol span={12}>
            <h2>Pagamento de Pacote</h2>
            <Formulario onFinish={ this.pagamentoPacote.bind(this) } tipoPagamento={'do pacote'}></Formulario>
          </AntdCol>
        </AntdRow>
      </LayoutPadrao>
    )
  }
}