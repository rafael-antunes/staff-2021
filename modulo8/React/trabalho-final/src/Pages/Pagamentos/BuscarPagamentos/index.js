import React, { Component } from 'react';

import Layout from '../../../Components/LayoutPadrao';
import CoworkingAPI from '../../../Models/CoworkingAPI';
import Table from '../../../Components/Middleware/AntdTable';

export default class BuscarPagamentos extends Component {
  constructor ( props ) {
    super ( props );
    this.coworkingAPI = new CoworkingAPI();
    this.state = {
      dados: []
    };
    this.coworkingAPI.buscarTodosPagamentos().then( e => this.setState({dados: e.data}) );
    
    this.columns = [
      {
        title: 'Id',
        dataIndex: 'id',
        key: 'id'
      },
      {
        title: 'Id do pacote',
        dataIndex: 'pacoteId',
        key: 'pacoteId'
      },
      {
        title: 'Id da contratação',
        dataIndex: 'contrataçãoId',
        key: 'contrataçãoId'
      },
      {
        title: 'Forma de Pagamento',
        dataIndex: 'formaPagamento',
        key: 'formaPagamento'
      }
    ]
  }

  formatarDados ( dados ) {
    dados.forEach(e => e.key = e.id );
    return dados;
  }

  render() {
    const { dados } = this.state;

    return (
      <Layout>
        <h2>Buscas</h2>
        { ( dados !== undefined ) && 
          <Table columns={this.columns} data={this.formatarDados(dados)}/>
        }
      </Layout>
    );
  }
}