import React, { Component } from 'react';

import Layout from '../../Components/LayoutPadrao';
import Formulario from '../../Components/Formularios/FormularioUsuario'
import CoworkingAPI from '../../Models/CoworkingAPI';

export default class Usuario extends Component {
  constructor ( props ) {
    super ( props );
    this.coworkingAPI = new CoworkingAPI();
  }

  render() {
    return (
      <Layout>
        <h2>Adicionar Usuário</h2>
        <Formulario onFinish={ this.coworkingAPI.cadastrarUsuario }></Formulario>
      </Layout>
    );
  }
}