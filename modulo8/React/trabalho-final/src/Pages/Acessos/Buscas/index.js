import React, { Component } from 'react';

import Layout from '../../../Components/LayoutPadrao';
import CoworkingAPI from '../../../Models/CoworkingAPI';
import Table from '../../../Components/Middleware/AntdTable';

export default class BuscarAcessos extends Component {
  constructor ( props ) {
    super ( props );
    this.coworkingAPI = new CoworkingAPI();
    this.state = {
      dados: []
    };
    this.coworkingAPI.buscarTodosAcessos().then( e => this.setState({dados: e.data}) );
    
    this.columns = [
      {
        title: 'Id',
        dataIndex: 'Id',
        key: 'Id'
      },
      {
        title: 'Id do Cliente',
        dataIndex: 'clienteId',
        key: 'clienteId'
      },
      {
        title: 'Id do espaço',
        dataIndex: 'espacoId',
        key: 'espacoId'
      },
      {
        title: 'Entrada/Saída',
        dataIndex: 'isEntrada',
        key: 'isEntrada'
      },
      {
        title: 'Data e Hora',
        dataIndex: 'data',
        key: 'data'
      },
      {
        title: 'Especial?',
        dataIndex: 'especial',
        key: 'especial'
      }
    ]
  }

  formatarDados ( dados ) {
    dados.forEach(e => e.key = e.id);
    return dados;
  }

  render() {
    const { dados } = this.state;

    return (
      <Layout>
        <h2>Buscas</h2>
        { ( dados !== undefined ) && 
          <Table columns={this.columns} data={this.formatarDados(dados)}/>
        }
      </Layout>
    );
  }
}