import React, { Component } from 'react';
import FormularioEntradaSaida from '../../../Components/Formularios/FormularioEntradaSaida';

import Layout from '../../../Components/LayoutPadrao';
import CoworkingAPI from '../../../Models/CoworkingAPI';

export default class Saidas extends Component {
  constructor ( props ) {
    super ( props );
    this.coworkingAPI = new CoworkingAPI();
  }

  realizarSaida ( values ) {
    values.data = new Date.now();
    this.coworkingAPI.realizarEntradaSaida( values );
  }

  render() {
    return (
      <Layout>
        <h2>Saídas</h2>
        <FormularioEntradaSaida onFinish={ this.realizarSaida.bind(this) }style={{ padding: 50, textAlign: 'center' }} entrada={true}></FormularioEntradaSaida>
      </Layout>
    );
  }
}