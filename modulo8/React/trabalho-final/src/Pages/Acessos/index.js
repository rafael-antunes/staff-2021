import React, { Component } from 'react';
import CoworkingAPI from '../../Models/CoworkingAPI';

import LayoutPadrao from '../../Components/LayoutPadrao';
import Row from '../../Components/Middleware/Layout/AntdRow';
import Col from '../../Components/Middleware/Layout/AntdCol';
import Button from '../../Components/Middleware/Form/AntdButton';
import { Link } from 'react-router-dom';
import Path from '../../Constants/Path';

export default class Acessos extends Component {
  constructor ( props ) {
    super ( props );
    this.coworkingAPi = new CoworkingAPI();
  }

  render() {
    return(
      <LayoutPadrao>
        <Row style={{ paddingTop: '50px' }}>
          <Col span={6}>
            <Button>
              <Link to={`${Path.ACESSOS}${Path.ENTRADAS}`}>Entradas</Link>
            </Button>
          </Col>
          <Col span={6}>
            <Button>
              <Link to={`${Path.ACESSOS}${Path.SAIDAS}`}>Saídas</Link>
            </Button>
          </Col>
          <Col span={6}>
            <Button>
              <Link to={`${Path.ACESSOS}${Path.SALDOS}`}>Saldos</Link>
            </Button>
          </Col>
          <Col span={6}>
            <Button>
              <Link to={`${Path.ACESSOS}${Path.BUSCA}`}>Buscas</Link>
            </Button>
          </Col>
        </Row>
      </LayoutPadrao>
    );
  }
}