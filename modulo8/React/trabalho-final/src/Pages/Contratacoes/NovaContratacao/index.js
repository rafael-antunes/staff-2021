import React, { Component } from 'react';

import LayoutPadrao from '../../../Components/LayoutPadrao';
import CoworkingAPI from '../../../Models/CoworkingAPI';
import FormularioNovaContratacao from '../../../Components/Formularios/FormularioContratacao/NovaContratacao'

export default class NovaContratacao extends Component {
  constructor ( props ) {
    super ( props );
    this.coworkingAPI = new CoworkingAPI();
  }

  criarContratacao ( values ) {
    values.espaco = this.coworkingAPI.buscarEspacoPorId( values.espacoId );
    values.cliente = this.coworkingAPI.buscarClientePorId( values.clienteId );
  }

  render() {
    return(
      <LayoutPadrao>
        <h2>Nova Contratação</h2>
        <FormularioNovaContratacao onFinish={ this.criarContratacao.bind( this )}></FormularioNovaContratacao>
      </LayoutPadrao>
    )
  }
}