import React, { Component } from 'react';

import Layout from '../../../Components/LayoutPadrao';
import CoworkingAPI from '../../../Models/CoworkingAPI';
import Table from '../../../Components/Middleware/AntdTable';

export default class BuscarContratacoes extends Component {
  constructor ( props ) {
    super ( props );
    this.coworkingAPI = new CoworkingAPI();
    this.state = {
      dados: []
    };
    this.coworkingAPI.buscarTodasContratacoes().then( e => this.setState({dados: e.data}) );
    
    this.columns = [
      {
        title: 'Id',
        dataIndex: 'id',
        key: 'id'
      },
      {
        title: 'Id do espaço',
        dataIndex: 'espaco',
        key: 'espaco'
      },
      {
        title: 'Id do cliente',
        dataIndex: 'cliente',
        key: 'cliente'
      },
      {
        title: 'Quantidade',
        dataIndex: 'quantidade',
        key: 'quantidade'
      },
      {
        title: 'Desconto',
        dataIndex: 'desconto',
        key: 'desconto'
      },
      {
        title: 'Prazo',
        dataIndex: 'prazo',
        key: 'prazo'
      },
      {
        title: 'Forma de Pagamento',
        dataIndex: 'formaPagamento',
        key: 'formaPagamento'
      }
    ]
  }

  componentDidUpdate() {
    console.log( this.state )
  }

  formatarDados ( dados ) {
    dados.forEach(e => {
        e.key = e.id;
        e.espaco = e.espaco.id;
        e.pacote = e.pacote.id;
      }
    );
    return dados;
  }

  render() {
    const { dados } = this.state;

    return (
      <Layout>
        <h2>Buscas</h2>
        { ( dados !== undefined ) && 
          <Table columns={this.columns} data={this.formatarDados(dados)}/>
        }
      </Layout>
    );
  }
}