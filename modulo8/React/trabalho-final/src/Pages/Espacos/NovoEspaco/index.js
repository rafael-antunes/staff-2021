import React, { Component } from 'react';

import Layout from '../../../Components/LayoutPadrao';
import Formulario from '../../../Components/Formularios/FormularioEspaco/NovoEspaco'
import CoworkingAPI from '../../../Models/CoworkingAPI';

export default class NovoEspaco extends Component {
  constructor ( props ) {
    super ( props );
    this.coworkingAPI = new CoworkingAPI();
  }

  render() {
    return (
      <Layout>
        <h2>Novo Espaço</h2>
        <Formulario onFinish={ this.coworkingAPI.novoEspaco }></Formulario>
      </Layout>
    );
  }
}