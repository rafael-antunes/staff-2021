import React, { Component } from 'react';

import Layout from '../../../Components/LayoutPadrao';
import Formulario from '../../../Components/Formularios/FormularioEspaco/AlterarEspaco'
import FormularioId from '../../../Components/Formularios/FormularioEspaco/BuscarEspacoId';
import CoworkingAPI from '../../../Models/CoworkingAPI';

export default class AlterarEspaco extends Component {
  constructor ( props ) {
    super ( props );
    this.coworkingAPI = new CoworkingAPI();
    this.state = { dados: null }
  }

  componentDidUpdate() {
    console.log( this.state );
  }

  async buscarEspaco( values ) {
    const response = await this.coworkingAPI.buscarEspacoPorId( values );
    response.data.id = values.id;
    this.setState({ dados: response.data } )
  }

  alterarEspaco ( values ) {
    values.id = this.state.dados.id;
    this.coworkingAPI.alterarEspaco( values );
  }

  render() {
    const { dados } = this.state;
    
    return (
      <Layout>
        <h2>Alterar Espaço</h2>
        <h3>Digite a identificação do espaço a ser alterado</h3>
        <FormularioId onFinish={ this.buscarEspaco.bind( this ) }></FormularioId>
        {( dados !== null ) && 
          <Formulario onFinish={ this.alterarEspaco.bind( this ) } dados={dados}></Formulario>
        }
        
      </Layout>
    );
  }
}