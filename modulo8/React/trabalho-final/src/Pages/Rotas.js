import React, { Component } from "react";
import { BrowserRouter as Router, Route } from 'react-router-dom';

import { RotasPrivadas } from "../Components/RotasPrivadas";

import App from "./App";

import Login from "./Login";

import Usuario from "./Usuario";

import Acessos from './Acessos'
import Entradas from "./Acessos/Entradas";
import Saidas from './Acessos/Saidas';
import BuscarAcessos from "./Acessos/Buscas";

import Clientes from "./Clientes";
import NovoCliente from './Clientes/NovoCliente';
import AlterarCliente from './Clientes/AlterarCliente';
import BuscarClientes from './Clientes/BuscarClientes';

import Espacos from "./Espacos";
import NovoEspaco from "./Espacos/NovoEspaco";
import AlterarEspaco from './Espacos/AlterarEspaco';
import BuscarEspacos from './Espacos/BuscarEspacos';

import Pacotes from './Pacotes';

import Path from "../Constants/Path";
import NovoPacote from "./Pacotes/NovoPacote";
import AlterarPacote from "./Pacotes/AlterarPacote";
import BuscarPacotes from "./Pacotes/BuscarPacotes";
import NovaContratacao from "./Contratacoes/NovaContratacao";
import BuscarContratacoes from "./Contratacoes/Buscas";
import RealizarPagamento from "./Pagamentos/RealizarPagamento";
import BuscarPagamentos from "./Pagamentos/BuscarPagamentos";
import BuscarSaldosPorCliente from "./Saldos";

export default class Rotas extends Component {
  render () {
    return (
      <Router>
        <RotasPrivadas path={ Path.APP } exact component={ App }/>
        <Route path={ Path.LOGIN } exact component={ Login }/>
        <RotasPrivadas path={ Path.USUARIO} exact component={ Usuario }/>
        <RotasPrivadas path={ Path.ACESSOS } exact component={ Acessos }/>
        <RotasPrivadas path={ `${Path.ACESSOS}${Path.ENTRADAS}`} exact component={ Entradas }/>
        <RotasPrivadas path={ `${Path.ACESSOS}${Path.SAIDAS}`} exact component={ Saidas }/>
        <RotasPrivadas path={ `${Path.ACESSOS}${Path.BUSCA}`} exact component={ BuscarAcessos }/>
        <RotasPrivadas path={ Path.CLIENTES } exact component={Clientes}/>
        <RotasPrivadas path={ `${Path.CLIENTES}${Path.NOVO}`} exact component={ NovoCliente }/>
        <RotasPrivadas path={ `${Path.CLIENTES}${Path.ALTERAR}`} exact component={ AlterarCliente }/>
        <RotasPrivadas path={ `${Path.CLIENTES}${Path.BUSCA}`} exact component={ BuscarClientes }/>
        <RotasPrivadas path={ Path.ESPACOS } exact component={Espacos}/>
        <RotasPrivadas path={ `${Path.ESPACOS}${Path.NOVO}` } exact component={ NovoEspaco }/>
        <RotasPrivadas path={ `${Path.ESPACOS}${Path.ALTERAR}` } exact component={ AlterarEspaco }/>
        <RotasPrivadas path={ `${Path.ESPACOS}${Path.BUSCA}` } exact component={ BuscarEspacos }/>
        <RotasPrivadas path={ `${Path.PACOTES}` } exact component={ Pacotes }/>
        <RotasPrivadas path={ `${Path.PACOTES}${Path.NOVO}` } exact component={ NovoPacote }/>
        <RotasPrivadas path={ `${Path.PACOTES}${Path.ALTERAR}` } exact component={ AlterarPacote }/>
        <RotasPrivadas path={ `${Path.PACOTES}${Path.BUSCA}` } exact component={ BuscarPacotes }/>
        <RotasPrivadas path={ `${Path.CONTRATACOES}${Path.NOVO}` } exact component={ NovaContratacao }/>
        <RotasPrivadas path={ `${Path.CONTRATACOES}${Path.BUSCA}` } exact component={ BuscarContratacoes }/>
        <RotasPrivadas path={ `${Path.PAGAMENTOS}` } exact component={ RealizarPagamento }/>
        <RotasPrivadas path={ `${Path.PAGAMENTOS}${Path.BUSCA}` } exact component={ BuscarPagamentos }/>
        <RotasPrivadas path={ `${Path.CLIENTES}${Path.SALDOS}` } exact component={ BuscarSaldosPorCliente }/>
      </Router>
    )
  }
}