import React, { Component } from 'react';

import Layout from '../../Components/LayoutPadrao';
import CoworkingAPI from '../../Models/CoworkingAPI';
import Table from '../../Components/Middleware/AntdTable';
import Formulario from '../../Components/Formularios/FormularioSaldo';

export default class BuscarSaldosPorCliente extends Component {
  constructor ( props ) {
    super ( props );
    this.coworkingAPI = new CoworkingAPI();
    this.state = {
      dados: []
    };
    this.columns = [
      {
        title: 'Id',
        dataIndex: 'Id',
        key: 'Id'
      },
      {
        title: 'Id do espaço',
        dataIndex: 'espacoId',
        key: 'espacoId'
      },
      {
        title: 'Tipo de Contratação',
        dataIndex: 'tipoContratacao',
        key: 'tipoContratacao'
      },
      {
        title: 'Quantidade',
        dataIndex: 'quantidade',
        key: 'quantidade'
      },
      {
        title: 'Vencimento',
        dataIndex: 'vencimento',
        key: 'vencimento'
      }
    ]
  }

  buscarSaldos ( values ) {
    this.coworkingAPI.buscarSaldosPorCliente( values.id ).then( e => this.setState({dados: e.data}) );
  }

  formatarDados ( dados ) {
    dados.forEach(e => e.key = e.id);
    return dados;
  }

  render() {
    const { dados } = this.state;

    return (
      <Layout>
        <h2>Buscas</h2>
        <Formulario onFinish={ this.buscarSaldos.bind( this ) }></Formulario>
        { ( dados !== undefined ) && 
          <Table columns={this.columns} data={this.formatarDados(dados)}/>
        }
      </Layout>
    );
  }
}