import React, { Component } from 'react';

import Layout from '../../../Components/LayoutPadrao';
import Formulario from '../../../Components/Formularios/FormularioPacote/AlterarPacote'
import FormularioId from '../../../Components/Formularios/FormularioPacote/BuscarPacoteId';
import CoworkingAPI from '../../../Models/CoworkingAPI';

export default class AlterarPacote extends Component {
  constructor ( props ) {
    super ( props );
    this.coworkingAPI = new CoworkingAPI();
    this.state = { dados: null }
  }

  componentDidUpdate() {
    console.log( this.state );
  }

  async buscarPacote( values ) {
    const pacote = this.coworkingAPI.buscarPacotePorId( values );
    const espacoPacotes = this.coworkingAPI.buscarEspacoPacotePorPacoteId( pacote );
    const espacoIds = [];
    espacoPacotes.forEach( e => {
      const id = e.espaco.id;
      espacoIds.add( id );
    })
    this.setState( { dados: 
      {
        espacos: espacoIds,
        quantidade: espacoPacotes.quantidade,
        valor: pacote.valor,
        prazo: espacoPacotes.prazo,
        tipoContratacao: espacoPacotes.tipoContratacao,
        id: pacote.id
      } 
    });
  }

  alterarPacote ( values ) {
    values.id = this.state.dados.id;
    this.coworkingAPI.alterarPacote( values );
  }

  render() {
    const { dados } = this.state;
    
    return (
      <Layout>
        <h2>Alterar Espaço</h2>
        <h3>Digite a identificação do espaço a ser alterado</h3>
        <FormularioId onFinish={ this.buscarPacote.bind( this ) }></FormularioId>
        {( dados !== null ) && 
          <Formulario onFinish={ this.alterarPacote.bind( this ) } dados={dados}></Formulario>
        }
        
      </Layout>
    );
  }
}