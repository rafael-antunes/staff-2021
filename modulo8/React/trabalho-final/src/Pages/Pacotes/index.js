import React, { Component } from 'react';
import CoworkingAPI from '../../Models/CoworkingAPI';

import LayoutPadrao from '../../Components/LayoutPadrao';
import Row from '../../Components/Middleware/Layout/AntdRow';
import Col from '../../Components/Middleware/Layout/AntdCol';
import Button from '../../Components/Middleware/Form/AntdButton';
import { Link } from 'react-router-dom';
import Path from '../../Constants/Path';

export default class Pacotes extends Component {
  constructor ( props ) {
    super ( props );
    this.coworkingAPi = new CoworkingAPI();
  }

  render() {
    return(
      <LayoutPadrao>
        <Row style={{ paddingTop: '50px' }}>
          <Col span={8}>
            <Button>
              <Link to={`${Path.PACOTES}${Path.NOVO}`}>Novo pacote</Link>
            </Button>
          </Col>
          <Col span={8}>
            <Button>
              <Link to={`${Path.PACOTES}${Path.ALTERAR}`}>Alterar dados de pacote</Link>
            </Button>
          </Col>
          <Col span={8}>
            <Button>
              <Link to={`${Path.PACOTES}${Path.BUSCA}`}>Buscas</Link>
            </Button>
          </Col>
        </Row>
      </LayoutPadrao>
    );
  }
}