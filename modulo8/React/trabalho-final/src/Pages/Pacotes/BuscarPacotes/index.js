import React, { Component } from 'react';

import Layout from '../../../Components/LayoutPadrao';
import CoworkingAPI from '../../../Models/CoworkingAPI';
import Table from '../../../Components/Middleware/AntdTable';

export default class BuscarPacotes extends Component {
  constructor ( props ) {
    super ( props );
    this.coworkingAPI = new CoworkingAPI();
    this.state = {
      dados: []
    };
    this.coworkingAPI.buscarTodosPacotes().then( e => this.setState({dados: e.data}) );
    
    this.columns = [
      {
        title: 'Id',
        dataIndex: 'id',
        key: 'id'
      },
      {
        title: 'Id dos Espacos',
        dataIndex: 'espacos',
        key: 'espacos'
      },
      {
        title: 'Valor',
        dataIndex: 'valor',
        key: 'valor'
      },
      {
        title: 'Prazo',
        dataIndex: 'prazo',
        key: 'prazo'
      },
      {
        title: 'Tipo de contratação',
        dataIndex: 'tipoContratacao',
        key: 'tipoContratacao'
      }
    ]
  }

  componentDidUpdate() {
    console.log( this.state )
  }

  formatarDados ( dados ) {
    dados.forEach(e => {
      e.key = e.id;
      const espacoPacotes = this.coworkingAPI.buscarEspacoPacotePorPacoteId( e );
      const espacoIds = [];
      espacoPacotes.forEach( e => {
        const id = e.espaco.id;
        espacoIds.add( id );
      })
      const espacosString = espacoIds.forEach( d => espacosString.concat(`${d},`) );
      espacosString.substr(0,espacosString.length-1);
      e.espacos = espacosString;
      });
    return dados;
  }

  render() {
    const { dados } = this.state;

    return (
      <Layout>
        <h2>Buscas</h2>
        { ( dados !== undefined ) && 
          <Table columns={this.columns} data={this.formatarDados(dados)}/>
        }
      </Layout>
    );
  }
}