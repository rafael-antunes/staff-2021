import React, { Component } from 'react';

import LayoutPadrao from '../../../Components/LayoutPadrao';
import CoworkingAPI from '../../../Models/CoworkingAPI';
import FormularioNovoPacote from '../../../Components/Formularios/FormularioPacote/NovoPacote'

export default class NovoPacote extends Component {
  constructor ( props ) {
    super ( props );
    this.coworkingAPI = new CoworkingAPI();
  }

  criarPacote ( values ) {
    const idsEspacos = values.espacos.split(',');
    const idPacote = this.coworkingAPI.novoPacote( values.valor );
    const pacote = this.coworkingAPI.buscarPacotePorId(idPacote);
    const espacos = [];
    idsEspacos.forEach( id => espacos.add(this.coworkingAPI.buscarEspacoPorId(id)) );
    espacos.forEach( e => {
      const dadosPacote = {
        pacote: pacote,
        espaco: e,
        tipoContratacao: values.tipoContratacao,
        quantidade: values.quantidade,
        prazo: values.prazo
      };
      this.coworkingAPI.novoEspacoPacote( { dadosPacote } );
    } )
  }

  render() {
    return(
      <LayoutPadrao>
        <h2>Novo Pacote</h2>
        <FormularioNovoPacote onFinish={ this.criarPacote.bind( this )}></FormularioNovoPacote>
      </LayoutPadrao>
    )
  }
}