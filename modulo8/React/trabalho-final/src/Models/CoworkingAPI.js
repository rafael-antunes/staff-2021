import axios from 'axios';

import Path from '../Constants/Path';

const endereco = 'http://localhost:8080'

axios.defaults.headers.common = {Authorization: localStorage.getItem('authorization')};

export default class CoworkingAPI {
  cadastrarUsuario( { nome, email, login, senha }) {
    return axios.post( `${endereco}${Path.API.CADASTRAR}`, { nome, email, login, senha } );
  }

  async login( { login, senha } ) {
    const response = await axios.post( `${endereco}${Path.API.LOGIN}` , { login, senha } );
    localStorage.setItem( 'authorization', response.headers.authorization );
  }

  novoCliente ( { nome, cpf, dataNascimento, contato1, contato2 } ) {
    const contato = [ contato1, contato2 ];
    return axios.post( `${endereco}${Path.API.API}${Path.API.CLIENTE}${Path.API.SALVAR}`, { nome, cpf, dataNascimento, contato });
  }

  alterarCliente ( { id, nome, cpf, dataNascimento, contato1, contato2 } ) {
    const contato = [ contato1, contato2 ];
    return axios.post( `${endereco}${Path.API.API}${Path.API.CLIENTE}${Path.API.EDITAR}/${id}`, { nome, cpf, dataNascimento, contato });
  }

  async buscarTodosClientes() {
    const response = await axios.get( `${endereco}${Path.API.API}${Path.API.CLIENTE}/`);
    console.log( response )
    return response;
  }

  async buscarClientePorId( { id } ) {
    const response = await axios.get( `${endereco}${Path.API.API}${Path.API.CLIENTE}/${id}`);
    return response;
  }

  novoEspaco ( { nome, qtdPessoas, valor } ) {
    return axios.post( `${endereco}${Path.API.API}${Path.API.ESPACO}${Path.API.SALVAR}`, { nome, qtdPessoas, valor });
  }

  alterarEspaco ( { id, nome, qtdPessoas, valor } ) {
    return axios.put( `${endereco}${Path.API.API}${Path.API.ESPACO}${Path.API.EDITAR}/${id}`, { nome, qtdPessoas, valor, id });
  }

  async buscarTodosEspacos() {
    const response = await axios.get( `${endereco}${Path.API.API}${Path.API.ESPACO}/`);
    return response;
  }

  async buscarEspacoPorId( { id } ) {
    const response = await axios.get( `${endereco}${Path.API.API}${Path.API.ESPACO}/${id}`);
    return response;
  }

  novoAcesso ( { clienteId, espacoId, isEntrada, especial, data  } ) {
    return axios.post( `${endereco}${Path.API.ACESSO}${Path.API.REGISTRAR_ACESSO}`, { clienteId, espacoId, isEntrada, especial, data  } );
  }

  async buscarTodosAcessos() {
    const response = await axios.get( `${endereco}${Path.API.API}${Path.API.ACESSO}`);
    return response;
  }

  async novoPacote ( { valor } ) {
    const response = await axios.post( `${endereco}${Path.API.API}${Path.API.PACOTE}${Path.API.SALVAR}`, { valor } ).then( e => e.id );
    return response;
  }

  async buscarTodosPacotes() {
    const response = await axios.get( `${endereco}${Path.API.API}${Path.API.PACOTE}`);
    return response;
  }

  async buscarPacotePorId( { id } ) {
    const response = await axios.get( `${endereco}${Path.API.API}${Path.API.PACOTE}/${id}`);
    return response;
  }

  novoEspacoPacote( { pacote, espaco, tipoContratacao, quantidade, prazo } ) {
    return axios.post( `${endereco}${Path.API.API}${Path.API.ESPACO_PACOTE}${Path.API.SALVAR}`, { pacote, espaco, tipoContratacao, quantidade, prazo } );
  }

  buscarEspacoPacotePorPacoteId( { id } ) {
    return axios.post( `${endereco}${Path.API.API}${Path.API.ESPACO_PACOTE}${Path.API.BUSCAR}`, { id } );
  }

  novaContratacao( { espaco, cliente, tipoContratacao, quantidade, desconto, prazo } ) {
    return axios.post( `${endereco}${Path.API.API}${Path.API.CONTRATACAO}${Path.API.SALVAR}`, { espaco, cliente, tipoContratacao, quantidade, desconto, prazo } );
  }

  async buscarTodasContratacoes() {
    const response = await axios.get( `${endereco}${Path.API.API}${Path.API.CONTRATACAO}`);
    return response;
  }

  novoPagamentoPacote ( { pacoteId, tipoPagamento } ) {
    return axios.post( `${endereco}${Path.API.API}${Path.API.PAGAMENTO}${Path.API.SALVAR}`, { pacoteId, tipoPagamento } );
  }

  novoPagamentoContratacao ( { contratacaoId, tipoPagamento } ) {
    return axios.post( `${endereco}${Path.API.API}${Path.API.PAGAMENTO}${Path.API.SALVAR}`, { contratacaoId, tipoPagamento } );
  }

  async buscarTodosPagamentos() {
    const response = await axios.get( `${endereco}${Path.API.API}${Path.API.PAGAMENTO}`);
    return response;
  }

  async buscarSaldosPorCliente( { id } ) {
    const response = await axios.get( `${endereco}${Path.API.API}${Path.API.SALDOS}${Path.API.POR_CLIENTE}/${id}`);
    return response;
  }
}