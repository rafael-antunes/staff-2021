import React, { Component } from 'react';

import EpisodioUi from '../Components/EpisodioUi';
import MensagemFlash from '../Components/MensagemFlash';
import MeuInputNumero from '../Components/MeuInputNumero';
import BotaoUi from '../Components/BotaoUi';

import ListaEpisodios from '../models/ListaEpisodios';
import EpisodioAPI from '../models/EpisodioAPI';

import Mensagem from '../Constants/mensagem';
import Cor from '../Constants/cor';
import Classe from '../Constants/classe';
import Path from '../Constants/path'

import './App.css';

export default class App extends Component {
  constructor( props ) {
    super( props );
    this.episodioAPI = new EpisodioAPI();
    this.sortear = this.sortear.bind( this );
    this.marcarComoAssistido = this.marcarComoAssistido.bind( this );
    this.state = {
      deveExibirMensagem: false,
      deveExibirErro: false,
      mensagem: ''
    }
  }

  componentDidMount() {
    this.episodioAPI.buscar()
      .then( episodio => {
        this.listaEpisodios = new ListaEpisodios( episodio );
        this.setState( state => { return { ...state, episodio: this.listaEpisodios.episodioAleatorio } } );
      })
  }

  sortear() {
    const episodio = this.listaEpisodios.episodioAleatorio;
    this.setState((state) => {
      return {
        ...state,
        episodio
      }
    });
  }  

  marcarComoAssistido() {
    const { episodio } = this.state;
    episodio.assistido();
    this.setState((state) => {
      return {
        ...state,
        episodio
      }
    });
  }

  registrarNota( { erro, nota } ) {
    this.setState((state) => {
      return {
        ...state,
        deveExibirErro: erro
      }
    });

    if( erro ) {
      return;
    }

    const { episodio } = this.state;    
    let mensagem, corMensagem;

    if ( episodio.validarNota( nota ) ) {
      episodio.avaliar( nota ).then( () => {
        corMensagem = Cor.VERDE;
        mensagem = Mensagem.SUCESSO.REGISTRO_NOTA;
        this.exibirMensagem({ corMensagem, mensagem });
      });
    }else{
      corMensagem = Cor.VERMELHO;
      mensagem = Mensagem.ERRO.NOTA_INVALIDA;
      this.exibirMensagem({ corMensagem, mensagem });
    }
  }

  exibirMensagem = ({ corMensagem, mensagem }) => {
    this.setState((state) => {
      return {
        ...state,
        deveExibirMensagem: true,
        mensagem,
        corMensagem
      }
    });
  }

  atualizarMensagem = devoExibir => {
    this.setState((state) => {
      return {
        ...state,
        deveExibirMensagem: devoExibir
      }
    });
  }
  
  render() {
    const { episodio, deveExibirMensagem, corMensagem, mensagem, deveExibirErro } = this.state;
    const { listaEpisodios } = this;

    return ( 
      !episodio ?
        ( <h3>Aguarde...</h3> ) :
        ( <div className={ Classe.APP.APP }>
          <MensagemFlash atualizar={ this.atualizarMensagem } exibir={ deveExibirMensagem } mensagem={ mensagem } cor={ corMensagem } />
          <header className={ Classe.APP.HEADER }>
            <BotaoUi classe={ Cor.PRETO } link={{ pathname:`${ Path.AVALIACOES }`, state: { listaEpisodios } }  } nome={ Mensagem.NOME_BOTAO.AVALIACOES }/>
            <BotaoUi classe={ Cor.PRETO } link={{ pathname:`${ Path.TODOS }`, state: { listaEpisodios } } } nome={ Mensagem.NOME_BOTAO.TODOS }/>
            <EpisodioUi episodio={ episodio } />
            <div className={ Classe.COMPONENTE.BOTOES }>
              <BotaoUi classe={ Cor.VERDE } metodo={ this.sortear } nome={ Mensagem.NOME_BOTAO.PROXIMO }/>
              <BotaoUi classe={ Cor.AZUL } metodo={ this.marcarComoAssistido } nome={ Mensagem.NOME_BOTAO.ASSISTI }/>
            </div>                
            <span>Nota: { episodio.nota }</span>
            <MeuInputNumero 
              placeholder={ Mensagem.PLACEHOLDER.NOTA_1_A_5 }
              mensagem={ Mensagem.DESCRICAO.INPUT_EPISODIO }
              visivel={ episodio.foiAssistido || false }
              obrigatorio
              exibirErro={ deveExibirErro }
              atualizarValor={ this.registrarNota.bind( this ) } />
          </header>
        </div> )
      );
  }
}