import React, { useState, useEffect } from 'react';
import EpisodioAPI from '../../models/EpisodioAPI';

function Formulario( props ) {
  const [ qtdVezesAssistido, setQtdVezesAssistido ] = useState(1);
  const [ nome, setNome ] = useState('Marcos');
  const [ idFilme, setidFilme ] = useState( 0 );
  const [ notas, setNotas ] = useState([]);

  useEffect( () => {
    let episodioAPI = new EpisodioAPI();
    episodioAPI.buscarNota( idFilme ).then( e => setNotas(e) );
  }, [ idFilme ] );

  return (
    <React.Fragment>
      <h1>Quantidade de Vezes Assistidas: { qtdVezesAssistido }</h1>
      <button type="button" onClick={ () => setQtdVezesAssistido( qtdVezesAssistido + 1 ) }>Já Assisti</button>
      <input type="text" onBlur={ evt => setNome(evt.target.value ) } />
      <p>{ nome }</p>
      <input type="text" onBlur={ evt => setidFilme( evt.target.value ) } />
      { notas.map( ( e, i ) => {return <div key={ i }>{ e.nota}</div> } ) }
    </React.Fragment>
  )
}

export default Formulario;