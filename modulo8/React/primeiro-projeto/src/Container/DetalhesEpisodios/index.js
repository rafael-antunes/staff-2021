import React, { Component } from 'react';
import EpisodioAPI from '../../models/EpisodioAPI';
import Classe from '../../Constants/classe';
import EpisodioUi from '../../Components/EpisodioUi';

export default class DetalhesEpisodios extends Component {
  constructor( props ) {
    super( props );
    this.episodioApi = new EpisodioAPI();
    this.state = {
      detalhes: null
    }
  }

  componentDidMount() {
    const episodioId = this.props.match.params.id;
    const requisicoes = [
      this.episodioApi.buscarDetalhes( episodioId ),
      this.episodioApi.buscarNota( episodioId )
    ];

    Promise.all( requisicoes )
      .then( respostas => {
        this.setState( state => {
          return {
            ...state,
            detalhes: respostas[0],
            objNotas: respostas[1]
          }
        });
      });
  }

  render() {
    const { episodio } = this.props.location.state;
    const { detalhes, objNotas } = this.state;
    console.log( detalhes );
    return (
      <React.Fragment>
        <header className={ Classe.APP.HEADER }>
          <EpisodioUi episodio={ episodio }/>
          {
            detalhes && (
              <React.Fragment>
                <p>{ detalhes[0].sinopse }</p>
                <span>{ new Date( detalhes[0].dataEstreia).toLocaleDateString() }</span>
                <span>IMDb: { detalhes[0].notaImdb * 0.5 }</span>
                <span>Sua nota: { objNotas ? objNotas[0].nota : 'N/D' }</span>
              </React.Fragment>
            )
          }
        </header>
      </React.Fragment>
    )
  }

}