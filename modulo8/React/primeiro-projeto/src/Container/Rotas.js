import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import App from './App';
import Avaliacoes from './Avaliacoes';
import DetalhesEpisodios from './DetalhesEpisodios';
import TodosEpisodios from './TodosEpisodios';
import Formulario from './Formulario';

import Path from '../Constants/path';

import { RotasPrivadas } from '../Components/RotasPrivadas'

export default class Rotas extends Component {
  render(){
    return(
      <Router>
        <Route path={ Path.APP } exact component={ App } />
        <Route path="/formulario" exact component={ Formulario } />
        <Route path={ Path.AVALIACOES } exact component={ Avaliacoes } />
        <Route path={`${Path.EPISODIO}/:id`} component={ DetalhesEpisodios } />
        <RotasPrivadas path={ Path.TODOS } exact component={ TodosEpisodios } />
      </Router>
    );
  }
}