import React, { Component } from 'react';
import ListaEpisodios from '../../Components/ListaEpisodios';
import Classe from '../../Constants/classe';
import EpisodioAPI from '../../models/EpisodioAPI';

import BotaoUi from '../../Components/BotaoUi'

import Cor from '../../Constants/cor'
import Mensagem from '../../Constants/mensagem';

export default class TodosEpisodios extends Component {
  constructor( props ) {
    super ( props );
    this.episodioApi = new EpisodioAPI();
    this.state = {
      ordenacao: () => {},
      tipoOrdenacaoDataEstreia: 'ASC',
      tipoOrdenacaoDuraca: 'ASC'
    }
  }

  componentDidMount() {
    const { listaEpisodios } =  this.props.location.state;
    const requisicoes = listaEpisodios.todos.map( e => this.episodioApi.buscarDetalhes( e.id ) );
    Promise.all( requisicoes ).then( resultado => {
      console.log(resultado[0])
      listaEpisodios.todos.forEach( episodio => {
        // episodio.dataEstreia = resultado.find( e => e[0].id === episodio.id ).dataEstreia;
      } )
    } )
  }

  alterarOrdenacaoParaDataEstreia = () => {
    const { tipoOrdenacaoDataEstreia } = this.state;
    this.setState( {
      ordenacao: ( a, b ) => new Date ( ( tipoOrdenacaoDataEstreia === 'ASC' ? a : b ).dataEstreia ) - new Date ( ( tipoOrdenacaoDataEstreia === 'ASC' ? b : a ).dataEstreia ),
      tipoOrdenacaoDataEstreia: tipoOrdenacaoDataEstreia === 'ASC' ? 'DESC' : 'ASC'
    } )
  }

  alterarOrdenacaoParaDuracao = () => {
    const { tipoOrdenacaoDuracao } = this.state;
    this.setState( {
      ordenacao: ( a, b ) => ( tipoOrdenacaoDuracao === 'ASC' ? a : b ).duracao - ( tipoOrdenacaoDuracao === 'ASC' ? b : a ).duracao,
      tipoOrdenacaoDuracao: tipoOrdenacaoDuracao === 'ASC' ? 'DESC' : 'ASC'
    } )
  }

  render() {
    const listaEpisodios = this.props.location.state.listaEpisodios.todos.concat([]);
    listaEpisodios.sort( this.state.ordenacao );

    return (
      <React.Fragment>
        <header className={ Classe.APP.HEADER }>
          <h1>Todos Episódios</h1>
          <BotaoUi classe={ Cor.PRETO } metodo={ this.alterarOrdenacaoParaDataEstreia.bind( this ) } nome={ `${ Mensagem.NOME_BOTAO.ORDENAR} ${ Mensagem.NOME_BOTAO.DATA }` } />
          <BotaoUi classe={ Cor.PRETO } metodo={ this.alterarOrdenacaoParaDuracao.bind( this ) } nome={ `${ Mensagem.NOME_BOTAO.ORDENAR} ${ Mensagem.NOME_BOTAO.DURACAO }` } />
          <ListaEpisodios listaEpisodios={ listaEpisodios } />
        </header>
      </React.Fragment>
    )
  }
}