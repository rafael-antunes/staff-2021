import React from 'react';
import { Link } from 'react-router-dom';

import BotaoUi from '../../Components/BotaoUi'

import './Avaliacoes.css'

import Cor from '../../Constants/cor';
import Classe from '../../Constants/classe';
import Path from '../../Constants/path'
import Mensagem from '../../Constants/mensagem'
import ListaEpisodios from '../../Components/ListaEpisodios';

const ListaAvaliacoes = props => {
  const { listaEpisodios } = props.location.state;

  return (
    <React.Fragment> 
      <div className={ Classe.AVALIACOES.AVALIACOES }>
        <div className={ Classe.AVALIACOES.HEADER }>
          <BotaoUi classe={ Cor.PRETO } link={ Path.APP } nome={Mensagem.NOME_BOTAO.PAGINA_INICIAL } />
          <ListaEpisodios listaEpisodios={ listaEpisodios.avaliados } />
        </div>        
      </div>
    </React.Fragment>
  );
}

export default ListaAvaliacoes;