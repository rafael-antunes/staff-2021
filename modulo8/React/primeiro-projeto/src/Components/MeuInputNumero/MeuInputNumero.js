import React, { Component } from 'react';

import Mensagem from '../../Constants/mensagem'

export default class MeuInputNumero extends Component {

  perderFoco = evt => {
    const { obrigatorio, atualizarValor } = this.props;
    const nota = evt.target.value;
    const erro = obrigatorio && !nota;
    atualizarValor( { erro, nota } );
  }

  render() {
    const { placeholder, visivel, mensagem, exibirErro, obrigatorio } = this.props;

    return visivel && (
      <React.Fragment>
        { mensagem && <span>{ mensagem }</span> }
        <input type="number" className={ exibirErro ? 'erro' : '' } placeholder={ placeholder } onBlur={ this.perderFoco } />
        { obrigatorio && <span className={ exibirErro ? 'mensagem-erro' : '' }>{ Mensagem.ERRO.CAMPO_OBRIGATORIO }</span> }
      </React.Fragment> );
  }
  
}