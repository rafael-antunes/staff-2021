import React from 'react';
import { Link } from 'react-router-dom';

import Path from '../../Constants/path'

const ListaEpisodios = ( { listaEpisodios } ) => 
  <React.Fragment>
    {
      listaEpisodios.avaliados && (
        listaEpisodios.avaliados.map( ( e, i ) => {
          <li key={ i }>
              <Link to={{ pathname:`${ Path.EPISODIO}/${ e.id }`, state: { episodio: e } }}>
                { `${ e.nome } - ${ e.nota }` }
              </Link>
          </li>;
        })
      )
    }
  </React.Fragment>

export default ListaEpisodios;