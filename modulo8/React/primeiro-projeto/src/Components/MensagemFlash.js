import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Cor from '../Constants/cor'
import Classe from '../Constants/classe'

export default class MensagemFlash extends Component {
  constructor( props ) {
    super( props );
    this.idsTimeouts = [];
    this.animacao = '';
  }

  fechar() {
    this.props.atualizar( false );
  }

  limparTimeouts() {
    this.idsTimeouts.forEach( clearTimeout );
  }

  componentWillUnmount() {
    this.limparTimeouts();
  }

  componentDidUpdate( prevProps ) {
    const { exibir, segundos } = this.props;

    if ( prevProps.exibir !== exibir) {
      const novoIdTimeout = setTimeout(() => {
        this.fechar();
      }, segundos * 1000);

      this.idsTimeouts.push( novoIdTimeout );
    }    
  }

  render() {
    const { cor, mensagem, exibir } = this.props;

    if ( this.animacao || exibir ) {
      this.animacao = exibir ? Classe.ANIMACAO.FADE_IN : Classe.ANIMACAO.FADE_OUT;
    }

    return <span onClick={ () => this.fechar() } className={ `${ Classe.ANIMACAO.FLASH } ${ cor } ${ this.animacao } `}>{ mensagem }</span>;
  }
}

MensagemFlash.propTypes = {
  mensagem: PropTypes.string.isRequired,
  cor: PropTypes.oneOf( [ Cor.VERDE, Cor.VERMELHO ] )
}

MensagemFlash.defaultProps = {
  cor: Cor.VERDE,
  segundos: 3
}