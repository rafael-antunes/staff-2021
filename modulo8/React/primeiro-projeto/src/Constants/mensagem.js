const Mensagem = {
  SUCESSO: {
    REGISTRO_NOTA: 'Nota registrada com sucesso!'
  },
  ERRO: {
    NOTA_INVALIDA: 'Informar uma nota válida (entre 1 e 5)!',
    CAMPO_OBRIGATORIO: '* Obrigatório'
  },
  DESCRICAO: {
    INPUT_EPISODIO: 'Qual a sua nota para esse episódio?'
  },
  NOME_BOTAO: {
    PROXIMO: 'Próximo',
    ASSISTI: 'Já Assisti',
    AVALIACOES: 'Lista de avaliações',
    PAGINA_INICIAL: 'Página Inicial',
    TODOS: 'Todos Episódios',
    DETALHES: 'Detalhes',
    ORDENAR: 'Ordenar por',
    DURACAO: 'duração',
    DATA: 'data de estréia'
  },
  PLACEHOLDER: {
    NOTA_1_A_5: 'Nota de 1 a 5'
  }
}

export default Mensagem;