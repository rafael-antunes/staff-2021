const Classe = {
  APP: {
    APP: 'App',
    HEADER: 'App-header'
  },
  AVALIACOES: {
    AVALIACOES: 'Avaliacoes',
    HEADER: 'Avaliacoes-header'
  },
  TODOS_EPISODIOS: {
    TODOS_EPISODIOS: 'TodosEpisodios',
    HEADER: 'TodosEpisodios-header'
  },
  DETALHE_EPISODIO: {
    DETALHE_EPISODIO: 'DetalheEpisodio',
    HEADER: 'DetalheEpisodio-header'
  },
  COMPONENTE: {
    BOTOES: 'botoes',
    BOTAO: 'btn'
  },
  ANIMACAO: {
    FADE_IN: 'fade-in',
    FADE_OUT: 'fade-out',
    FLASH: 'flash'
  }
}

export default Classe;