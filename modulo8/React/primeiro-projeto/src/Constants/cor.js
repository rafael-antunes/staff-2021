const Cor = {
  VERDE: 'verde',
  AZUL: 'azul',
  VERMELHO: 'vermelho',
  PRETO: 'preto'
}

export default Cor;