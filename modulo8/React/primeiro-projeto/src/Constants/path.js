const Path = {
  APP: '/',
  AVALIACOES: '/avaliacoes',
  EPISODIO: '/episodio',  
  TODOS: '/todos',
  API: {
    EPISODIOS: '/episodios',
    NOTAS: '/notas',
    DETALHES: '/detalhes'
  }
}

export default Path;