import axios from 'axios';

import Path from '../Constants/path';

const endereco = 'http://localhost:9000/api';

export default class EpisodioAPI {
  buscar() {
    return axios.get( `${ endereco }${ Path.API.EPISODIOS }` ).then( e => e.data );
  }

  registrarNota( { nota, episodioId } ) {
    return axios.post( `${ endereco }${ Path.API.NOTAS }`, { nota, episodioId } );
  }

  buscarNota( id ) {
    return axios.get( `${ endereco }${ Path.API.NOTAS}?episodioId=${ id }` ).then( e => e.data );
  }

  buscarDetalhes( id ) {
    return axios.get( `${ endereco }${ Path.API.EPISODIOS }/${ id }${ Path.API.DETALHES }`).then( e => e.data );
  }

  buscarTodasNotas() {
    return axios.get( `${ endereco }${ Path.API.NOTAS }`).then( e => e.data );
  }
}