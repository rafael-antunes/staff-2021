import Episodio from "./Episodio";

function sortear( min, max ) {
  min = Math.ceil( min );
  max = Math.floor( max );
  return Math.floor( Math.random() * ( max - min ) ) + min;
}

function sortTemporadaEp( listaEpisodios ) {
  return listaEpisodios.sort( ( a, b ) => a.temporada - b.temporada || a.ordemEpisodio - b.ordemEpisodio );
}

export default class ListaEpisodios {
  constructor( episodioAPI ) {
    this.todos = episodioAPI.map( episodio => new Episodio( episodio ) );
  }
  
  get episodioAleatorio() {
    const indice = sortear( 0, this.todos.length );
    return this.todos[ indice ];
  }
  
  get avaliados() {
    return sortTemporadaEp( this.todos.filter( e => e.nota ) );
  }

  get episodiosOrdenados() {
    return sortTemporadaEp( this.todos );
  }
  
}