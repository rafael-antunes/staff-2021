function cardapioIFood( veggie = true, comLactose = true ) {
  let cardapio = [
    'enroladinho de salsicha',
    'cuca de uva'
  ];

  if ( comLactose ) {
    cardapio.push( 'pastel de queijo' );
  }

  cardapio = [ ...cardapio,
    'pastel de carne',
    'empada de legumes marabijosa'
  ];

  if ( veggie ) {
    arr = cardapio.splice( cardapio.indexOf( 'enroladinho de salsicha' ), 1 );
    arr = cardapio.splice( cardapio.indexOf( 'pastel de carne' ), 1 );
  }
  
  const resultadoFinal = cardapio.map( alimento => alimento.toUpperCase() );  
  return resultadoFinal;
}

cardapioIFood(); // esperado: [ 'CUCA DE UVA', 'PASTEL DE QUEIJO', 'EMPADA DE LEGUMES MARABIJOSA' ]