console.log("Chegou até aqui!");

/* VAR */
console.log(nome);
var nome = "Marcos";
var nome = "Marcos H";
console.log(nome);

/* LET */

let nome1 = "Marcos";

    {
        let nome1 = "Marcos";
        nome1 = "Marcos H"; 
        console.log(nome1);
    }

console.log(nome1);

/* CONST */

//const nome2 = "Marcos - Constante";
const pessoa = {
    nome: "Marcos - Constante"
};

Object.freeze(pessoa);

pessoa.nome = "Marcos - Constante 2";
//pessoa.idade = 31;

console.log(pessoa.nome);0
//console.log(pessoa.idade);

/* ESPETACULAR */

console.log("Nossa que incrível!!!!!!")

let soma = 1 + 2;
let soma1 = "1" + 2 + 3;
let soma2 = 1 + "2" + 3;
let soma3 = 1 + 2 + "3";

console.log(soma);
console.log(soma1);
console.log(soma2);
console.log(soma3);

/* FUNCOES!!! */

let nomeFuncao = "Marcos";
let idadeFuncao = 31;
let semestre = 5;
let notas = [10.0, 3, 5, 9];

function funcaoCriarAluno(nomeFuncao, idadeFuncao, semestre, notas = []) {
    const aluno = {
        nome: nomeFuncao,
        idade: idadeFuncao,
        semestre: semestre,
        nota: notas
    };

    //Factory -> Design Pattern
    function aprovadoOuReprovado( notas ) {
        if( notas.length == 0 ) {
            return"Sem Notas";
        }

        let somatoria = 0;
        for (let i = 0; i < notas.length; i++) {
            somatoria += notas[i];
        }

        return (somatoria / notas.length) > 7.0 ? "Aprovado" : "Reprovado";
    }

    aluno.status = aprovadoOuReprovado( aluno.nota );
    console.log(aluno);
    return aluno;
}

funcaoCriarAluno(nomeFuncao, idadeFuncao, semestre, notas);
funcaoCriarAluno(nomeFuncao, idadeFuncao, semestre);


let alunoExterno = funcaoCriarAluno(nomeFuncao, idadeFuncao, semestre, notas);
console.log(alunoExterno);

/** ------------------------Template String ( Crasezinha )--------------------- */
let texto = "Texto";
let outroValor = "Texto 2";

console.log(texto + "Aqui é o meio entre dois textos " + outroValor + pessoa.nome );
console.log( `${texto} Aqui é o meio entre dois textos ${ outroValor } - ${ pessoa.nome }` );

/** ---------------------------Destruction------------------------ */

    let objeto = {
        nome: "Marcos",
        idade: 31,
        altura: 1.85
    }

    const { nome:NomeCompleto, altura } =  objeto;

    const arrayTeste = ['Gustavo', 'Kevin', 'Victor', 'Arthur'];

    let [, pos2,,pos4] = arrayTeste;

    let a = 1;
    let b = 3;

    [a,b] = [b,a];

    /** --------------------Spread Operator------------------------- */

    let arraySpread = [1, 77, 83, 42];
    console.log( ...arraySpread );
    console.log( {...arraySpread} );
    console.log( ...[1, 77, 83, 42]);
    console.log( ..."MeuNome");
    console.log( [..."MeuNome"] );