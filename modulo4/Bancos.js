bancos = [
    {
        "codigo":"011",
        "nome":"Alfa",
        "agencia":[
            ObjectId("60bfbfdea16843819ed8728d"),
            ObjectId("60bfbfdea16843819ed8728e"),
            ObjectId("60bfbfdea16843819ed8728f")
        ]
    },
    {
        "codigo":"241",
        "nome":"Beta",
        "agencia":[
            ObjectId("60bfbfdea16843819ed87290")
        ]
    },
    {
        "codigo":"307",
        "nome":"Omega",
        "agencia":[
            ObjectId("60bfbfdea16843819ed87291"),
            ObjectId("60bfbfdea16843819ed87292"),
            ObjectId("60bfbfdea16843819ed87293")
        ]
    }
]
    
db.banco.insert(bancos);
    
db.banco.find();