db.createCollection("agencia")
db.createCollection("consolidacao")
db.createCollection("saque")
db.createCollection("deposito")

depositos = []

saques = []

db.deposito.insert(depositos)
db.saque.insert(saques)

consolidacoes = [
    {
        "saldoAtual":0,
        "saques":[],
        "depositos":[],
        "numeroDeCorrentistas":0
    }
]
    
db.consolidacao.insert(consolidacoes)
    
db.consolidacao.find();
db.agencia.find();
    
agencias = [
    {
        "codigo":"0001",
        "nome":"Web",
        "endereco": ObjectId("60bfbb08a16843819ed87283"),
        "consolidacao": ObjectId("60bfbe9ba16843819ed8728c")
    },
    {
        "codigo":"0002",
        "nome":"California",
        "endereco": ObjectId("60bfbb08a16843819ed87284"),
        "consolidacao": ObjectId("60bfbe9ba16843819ed8728c")
    },
    {
        "codigo":"0003",
        "nome":"Londres",
        "endereco": ObjectId("60bfbb08a16843819ed87285"),
        "consolidacao": ObjectId("60bfbe9ba16843819ed8728c")
    },
    {
        "codigo":"0001",
        "nome":"Web",
        "endereco": ObjectId("60bfbb08a16843819ed87283"),
        "consolidacao": ObjectId("60bfbe9ba16843819ed8728c")
    },
    {
        "codigo":"0001",
        "nome":"Web",
        "endereco": ObjectId("60bfbb08a16843819ed87287"),
        "consolidacao": ObjectId("60bfbe9ba16843819ed8728c")
    },
    {
        "codigo":"8761",
        "nome":"Itu",
        "endereco": ObjectId("60bfbb08a16843819ed87288"),
        "consolidacao": ObjectId("60bfbe9ba16843819ed8728c")
    },
    {
        "codigo":"4567",
        "nome":"Hermana",
        "endereco": ObjectId("60bfbb08a16843819ed87289"),
        "consolidacao": ObjectId("60bfbe9ba16843819ed8728c")
    }
];
    
db.agencia.insert(agencias);