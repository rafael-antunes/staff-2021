db.createCollection("pessoa");

pessoas = [
    {
        "CPF":"000.000.000-01",
        "nome":"Joao",
        "dataNascimento":"01/01/2000",
        "estadoCivil":"Solteiro",
        "endereco": ObjectId("60bfb6cda16843819ed8722a")
    },
    {
        "CPF":"100.000.000-01",
        "nome":"Jose",
        "dataNascimento":"01/01/2000",
        "estadoCivil":"Solteiro",
        "tipoGerente":"GC",
        "codigoFuncionadrio": "000001",
        "endereco": ObjectId("60bfb6cda16843819ed8722a")
    }
];
    
db.pessoa.insert(pessoas);
    
db.pessoa.find();