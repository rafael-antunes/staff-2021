/*db.createCollection("movimentacao");

movimentacoes = [
    {
        "tipo": "credito",
        "valor": 1200.00
    },
    {
        "tipo": "debito",
        "valor": 1200.00
    },
    {
        "tipo": "a combinar",
        "valor": 1200.00
    }
];
    
db.movimentacao.insert(movimentacoes); */

db.createCollection("conta");

contas = [
    {
        "codigo":"0001",
        "tipoConta":"PJ",
        "saldo":0,
        "movimentacoes": [
            ObjectId("60bfb8a0a16843819ed87234"),
            ObjectId("60bfb8a0a16843819ed87235"),
            ObjectId("60bfb8a0a16843819ed87236")
        ],
        "cliente":[
            ObjectId("60bfb7cea16843819ed87232"),
            ObjectId("60bfb7cea16843819ed87232"),
            ObjectId("60bfb7cea16843819ed87232")
        ],
        "gerente":[
            ObjectId("60bfb7cea16843819ed87233")
        ]
    },
    {
        "codigo":"0002",
        "tipoConta":"Conjunta",
        "saldo":0,
        "movimentacoes": [
            ObjectId("60bfb8a0a16843819ed87234"),
            ObjectId("60bfb8a0a16843819ed87235"),
            ObjectId("60bfb8a0a16843819ed87236")
        ],
        "cliente":[
            ObjectId("60bfb7cea16843819ed87232"),
            ObjectId("60bfb7cea16843819ed87232")
        ],
        "gerente":[
            ObjectId("60bfb7cea16843819ed87233")
        ]
    }
];
  
  
db.conta.insert(contas);
    
db.conta.find();
db.movimentacao.find();